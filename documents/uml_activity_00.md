```plantuml
@startuml
!theme sunlust
skinparam defaultFontName Dejavu Serif
caption UML: Activity Diagram

|#Business|Business|
start
while (Is service needs?) is (true)
    :mindmap(SERVICE): <b>features;
    while (FEATURES.each) is (<b>feature)
        :activity(feature): <b>uses case;
    endwhile (end)
    :context(SERVICE): <b>agents;
|#Technology|Technology|
    :container(SERVICE, AGENTS): <b>containers;
    fork
    :sequense(CONTAINERS): <b>proccess;
    fork again
|#Application|Application|
    while (CONTAINERS.each) is (<b>container)
        :component(CONTAINER): <b>components;
        :sequense(COMPONENTS): <b>proccess;
    endwhile (end);
|Technology|
    end fork
    :deployment(CONTAINERS): <b>nodes;
|#Business|Business|
endwhile (false)
stop
@enduml
```
