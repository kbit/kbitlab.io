```plantuml
@startebnf
!theme sunlust
left header Extended Backus–Naur Form
caption Tense
skinparam WrapWidth 800
skinparam DefaultTextAlignment Left
!$SUBJECT = '?SUBJECT?'
!$QUEST   = '?QUESTION WORD?'
!$VERB1   = '(?VERB1?| ?VERB1 + "s / es"? (*for HE / SHE*))'
!$VERB2   = '?VERB2?'
!$VERB3   = '?VERB3? (*VERB1 + "ed"*)'
!$BE1     = '"am / are / is"'
!$BE2     = '"was / were" (*"be" VERB2*)'
!$BE3     = '"been" (*"be" VERB3*)'
!$HAVE1   = '"have / has"'
!$HAVE2   = '"had" (*"have" VERB2*)'
!$DO2     = '"did" (*"do" VERB2*)'
!$ING     = '?VERB1 + "ing"?'

(*Use **past simple** to talk about finished actions in the past*)
Past_Simple = (
    $SUBJECT, ($VERB2| $DO2, "not", ?VERB1?| $BE2, ["not"])|
    [$QUEST], ($DO2| $BE2), ["not"], $SUBJECT, ?VERB1?
), [
    "at", (?a particular time within a day or holidays or weekends?)|
    "on", (?days or dates?)|
    "in", (?months or seasons or years or centuries?)
];
(*Use **present simple** to talk about actions happening now or in general.*)
Present_Simple = (
    $SUBJECT , [("do"| "do + 'es'" (*for I / HE / SHE / IT*)), "not"]|
    [$QUEST], ("do"| "do + 'es'" (*for I / HE / SHE / IT*)), $SUBJECT
),$VERB1;
(*Use **future simple** to talk about plans that were made in the process of speaking or predictions based on our personal opinion*)
Future_Simple = (
    $SUBJECT,"will",["not"]|
    [$QUEST], ("will"| "won't"), $SUBJECT
),?VERB1?;

(*Use **past perfect simple** to talk about past situations that happened before another time in the past or states that continued up to a certain moment in the past*)
(*Use **present perfect simple** to talk about events from the past until now or past actions with results now*)
Perfect_Simple = (
    $SUBJECT, (($HAVE2| $HAVE1), ["not"]| "will", ["not"], "have")|
    [$QUEST], (($HAVE2| $HAVE1), ["not"]| ("will"| "won't"), "have"), $SUBJECT
), $VERB3;
Perfect_Continuous = (
    $SUBJECT, (($HAVE2| $HAVE1), ["not"]| "will", ["not"], "have")|
    [$QUEST], (($HAVE2| $HAVE1), ["not"]| ("will"| "won't"), "have"), $SUBJECT
), $BE3, $ING;
(*Use **past continuous** to talk about an action that was in progress at a particular time in the past or a past action which was interrupted by another past activity*)
(*Use **present continuous** to talk about actions happening now or temporary situations (these days, this week, today)*)
Continuous = (
    $SUBJECT, (($BE2| $BE1), ["not"]| "will", "be")|
    [$QUEST], (($BE2| $BE1), ["not"]| ("will"| "won't"), "be"), $SUBJECT
), ($ING| "going to", ?VERB1?);

@endebnf
```
