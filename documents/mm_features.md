```plantuml
@startmindmap
'https://technicaldocs.ru/гост{19,34}/нпа
'https://12factor.net/ru/
'https://real-world-plantuml.com
'https://elma-bpm.ru/journal/urok-1-vvod-v-notaciyu-bpmn
'https://flexberry.github.io/ru/fd_activity-diagram.html
'https://bpmn.gitbook.io/bpmn-guide/bpmn-guidelines-and-best-practices
'https://visual-paradigm.com/guide/archimate/{full-archimate-viewpoints-guide,what-is-archimate}/
'https://docs.elementary.io/hig#human-interface-guidelines
'https://plantuml-documentation.readthedocs.io/en/latest/formatting/all-skin-params.html
'https://docs.microsoft.com/ru-ru/azure/architecture
'https://talyian.github.io/ansicolors/
!theme sunlust

left header MindMap
skinparam WrapWidth 200
caption 'ITSM' system's features

*[#Application] <b>ITSM\n[GLPI]

**[#Business] <&circle-check>Helpdesk\n
***_ Customer satisfaction survey
***_ Approvals (via email)
***_ Incident management
***_ Request management
***_ Problem management
***_ Tasks for tickets
***_ Change management
***_ Recurrent tickets
***_ Automatic actions
***_ Canned responses
***_ Ticket templates
***_ Linked projects
***_ Linked tickets
***_ Knowledge Base
***_ SLA management
***_ Linked budget
***_ Linked assets
***_ Notifications
***_ Self-service
***_ Track time
***_ Dasboard
***_ Reports
***_ Forms

**[#Business] <&circle-check>Financial\nManagement
***_ Suppliers management
***_ Contract management
***_ Contact management
***_ License management
***_ Budget management
***_ Linked tickets

**[#Business] <&circle-check>User\nmanagement
***_ Access & restrictions (rules)
***_ Oauth SSO application
***_ Self-service mode
***_ Entities
***_ LDAP

left side
**[#Business] <&circle-check>CMDB\n
***_ Administrative and financial information
***_ Inventory (agentless)
***_ Warranty information
***_ Software management
***_ License management
***_ Automatic actions
***_ Native inventory
***_ Linked incidents
***_ Linked documents
***_ Impact analysis
***_ Rack management
***_ Linked problems
***_ Linked requests
***_ Asset lifecycle
***_ Linked tickets
***_ Linked changes
***_ Antiviruses
***_ Dashboard
***_ Reports
***_ Domains

**[#Business] <&circle-check>Project\nManagement
***_ Linked documents
***_ Impact analysis
***_ Team management
***_ Tasks tracking
***_ Knowledge Base
***_ Linked assets
***_ Kanban
***_ GANTT

**[#Business] <&circle-check>Customize\n
***_ Select color palette
***_ Select page layout
***_ Select language
***_ Add your logo
***_ API rest

@endmindmap
```
