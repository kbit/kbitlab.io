```plantuml
@startuml
!theme sunlust
skinparam defaultFontName Dejavu Serif
left header UML: Activity Diagram
caption 'ITSM' system's feature 'Task tracking'
|#Business|Business|
start
    while (Is new request?) is (true)
        :Generate issue after user request: <b>NEW;
        while (Have all the necessary information?) is (true)
            :Request Information: <b>HOLD;
        endwhile (false)
        :Issue analysis: <b>IN PROGRESS;
        if (Need a product change?) then (true)
            :Create Change Request: <b>NEW;
            while (Have the edits passed all\nthe checks and have they been\nagreed with the customer?) is (false)
|#Application|Application|
                :Create changes: <b>IN PROGRESS;
|#Technology|Technology|
                :Install changes in a\nDEV environment;
|Application|
                :Conduct smoke testing;
|Technology|
                :Install changes in a\nTEST environment;
|Application|
                :Conduct functional and\nregression testing: <b>TEST;
|Technology|
                :Install changes in a\nUAT environment;
|Business|
                :Conduct user testing:\n<b>AGREEMENT;
|Application|
            endwhile (true)
|Technology|
            :Install changes in a\nPROD nenvironment:\n<b>DEPLOIMENT;
|Business|
        :Update product\ndocumentation: <b>DONE;
        else (false)
        endif
        :Solution: <b>DONE;
    endwhile (false)
stop
@enduml
```
