```plantuml
@startgantt
!theme sunlust
skinparam {
    Default {
        TextAlignment left
        FontName Dejavu Serif
    }
}
printscale daily zoom 2
left header Gantt Diagram
hide footbox

Project starts the 24th of july 2023
saturday are closed
sunday are closed
[Task A]  on {Employee1} lasts 5 days
then [Task C] on {Employee1} lasts 6 days
then [Task E] on {Employee1} lasts 6 days
then [Task G] on {Employee1} lasts 6 days
then [Finish] happens at [Task G]'s end
[Task D] starts at [Task A]'s end
[Task D] on {Employeer3} lasts 6 days
then [Task F] on {Employee3} lasts 5 days
[Task B]  on {Employee2} lasts 6 days
[Task B] -> [Task E]
[Task F] -> [Finish]
@endgantt
```
