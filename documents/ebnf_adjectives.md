```plantuml
@startebnf
!theme sunlust
left header Extended Backus–Naur Form
caption Adjective
skinparam WrapWidth 600
skinparam DefaultTextAlignment Left

!$SUBJECT = '?SUBJECT?'
!$OBJECT  = '?OBJECT?'
!$VERB3   = '?VERB3? (*VERB1 + "ed"*)'
!$BE1     = '("am" (*for I*) | "are"| "is" (*for I / HE / SHE / IT*))'

(*Use **superlatives** to express the highest degree of a word*)
Adjective  = ($SUBJECT, $BE1| $BE1, $SUBJECT), (
    "as", ?ADJECTIVE?, "as", $OBJECT|
    (
        ("more"|"the most"), ?ADJECTIVE?|
        (?SHORT ADJECTIVE?, "+ 'er'"| "the", ?SHORT ADJECTIVE?, "+ 'est'")
    ), [$OBJECT, ["in" | $SUBJECT, ("have"| "has"), "ever", $VERB3]]
);
@endebnf
```
