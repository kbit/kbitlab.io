```plantuml
@startuml
!theme sunlust
!include <archimate/Archimate>
!include <awslib/AWSCommon>
!foreach $item in %splitstr('General~SecurityIdentityCompliance~NetworkingContentDelivery~DeveloperTools~Database~Storage~ApplicationIntegration~Compute~Groups~MediaServices~MachineLearning', '~')
    !include <awslib/$item/all.puml>
!endfor
hide stereotype
skinparam linetype ortho
left header AWS Diagram

AWSCloudGroup(cloud) {
    VPCGroup(vpc) {
        VPCInternetGateway(internet_gateway, "Internet gateway", "")
        AvailabilityZoneGroup(az_1, "\tAvailability Zone 1\t") {
        PublicSubnetGroup(az_1_public, "Public subnet") {
            VPCNATGateway(az_1_nat_gateway, "NAT gateway", "") #Transparent
        }
        PrivateSubnetGroup(az_1_private, "Private subnet") {
            EC2Instance(az_1_ec2_1, "Instance", "") #Transparent
        }
        Rel_Serving_Up(az_1_ec2_1, az_1_nat_gateway, '')
    }
    AvailabilityZoneGroup(az_2, "\tAvailability Zone 2\t") {
        PublicSubnetGroup(az_2_public, "Public subnet") {
            VPCNATGateway(az_2_nat_gateway, "NAT gateway", "") #Transparent
        }
        PrivateSubnetGroup(az_2_private, "Private subnet") {
            EC2Instance(az_2_ec2_1, "Instance", "") #Transparent
        }
        Rel_Serving_Up(az_2_ec2_1, az_2_nat_gateway, '')
    }
  }
}
@enduml
```
