```plantuml
@startuml
!theme sunlust
left header UML: State Diagram
skinparam WrapWidth 200
hide empty description
left to right direction

!procedure $initStep($STAGE, $STEPS)
    !foreach $STEP in %splitstr($STEPS, "~")
        !$x = $STAGE + "_check_" + $STEP
        !$y = $STAGE + "_run_" + $STEP
        $x -d[#green]-> $y : 1
        !$x = $STAGE + "_error"
        $y -d[#red]-> $x : default
    !endfor
!endprocedure

!procedure $initFlow($STAGE, $STEP0, $STEPS)
    !foreach $STEP1 in %splitstr($STEPS, "~")
        !$x = $STAGE + "_check_" + $STEP0
        !$y = $STAGE + "_check_" + $STEP1
        $x -r-> $y : default
        !$x = $STAGE + "_run_" + $STEP0
        $x -l[#green]-> $y : 0
        !$STEP0 = $STEP1
    !endfor
!endprocedure

!procedure $initStatesStop()
    state "Начальный шаг сценария (0)" as stop_init
    state "Проверка 'Уведомить администраторов кластера' (101)" as stop_check_alert
    state "Проверка 'Отключить мониторинг кластера' (102)" as stop_check_mon
    state "Проверка 'Остановить потоки YARN' (103)" as stop_check_yarn
    state "Проверка 'Заблокировать профиль CTL' (104)" as stop_check_block
    state "Проверка 'Остановить CTL' (105)" as stop_check_ctl
    state "Проверка 'Остановить InfraRS' (106)" as stop_check_infrars
    state "Проверка 'Остановить IBDM' (107)" as stop_check_ibdm
    state "Проверка 'Сохранить копию deployment.json' (108)" as stop_check_json
    state "Проверка 'Сохранить копию настроек кластера' (109)" as stop_check_backup
    state "Проверка 'Остановить BMES' (110)" as stop_check_bmes
    state "Проверка 'Остановить сервисы Hadoop' (111)" as stop_check_hadoop
    state "Оповещение администраторов кластера (201)" as stop_run_alert
    state "Отключение мониторинга кластера (202)" as stop_run_mon
    state "Остановка потоки YARN (203)" as stop_run_yarn
    state "Блокировка профиля CTL (204)" as stop_run_block
    state "Остановка CTL (205)" as stop_run_ctl
    state "Остановка InfraRS (206)" as stop_run_infrars
    state "Остановка IBDM (207)" as stop_run_ibdm
    state "Сохранение копии deployment.json (208)" as stop_run_json
    state "Сохранение копии настроек кластера (209)" as stop_run_backup
    state "Остановка BMES (210)" as stop_run_bmes
    state "Остановка сервисы Hadoop (211)" as stop_run_hadoop
    state "Проверка работы сценария (300)" as stop_check
    state "Запрос согласования работ (9999)" as stop_request
    state "Получение согласование работ (9998)" as stop_answer
    state "Обработка ошибок сценария (1000)" as stop_error
    state "Запрос отклонён (1001)" as stop_denial
    state "Вышло время на согласование (1002)" as stop_timeout
    state "Завершающий шаг сценария (10000)" as stop_final
!endprocedure

!procedure $initStatesStart()
    state "Начальный шаг сценария (0)" as start_init
    state "Проверка 'Запустить сервисы Hadoop' (101)" as start_check_hadoop
    state "Проверка 'Запустить IBDM' (102)" as start_check_ibdm
    state "Проверка 'Запустить InfraRS' (103)" as start_check_infrars
    state "Проверка 'Запустить CTL' (104)" as start_check_ctl
    state "Проверка 'Разблокировать профиль CTL' (105)" as start_check_block
    state "Проверка 'Запустить BMES' (106)" as start_check_bmes
    state "Проверка 'Включить мониторинг кластера' (107)" as start_check_mon
    state "Запуск сервисы Hadoop (201)" as start_run_hadoop
    state "Запуск IBDM (202)" as start_run_ibdm
    state "Запуск InfraRS (203)" as start_run_infrars
    state "Запуск CTL (204)" as start_run_ctl
    state "Разблокирвка профиля CTL (205)" as start_run_block
    state "Запуск BMES (206)" as start_run_bmes
    state "Включение мониторинга кластера (207)" as start_run_mon
    state "Проверка работы сценария (300)" as start_check
    state "Запрос согласования работ (9999)" as start_request
    state "Получение согласование работ (9998)" as start_answer
    state "Обработка ошибок сценария (1000)" as start_error
    state "Запрос отклонён (1001)" as start_denial
    state "Вышло время на согласование (1002)" as start_timeout
    state "Завершающий шаг сценария (10000)" as start_final
!endprocedure

[*] --> Stop
state Stop {
    $initStatesStop()
    [*]          -r-> stop_init
    stop_init    -r-> stop_request : default
    stop_request -r-> stop_answer  : default
    stop_answer  -r[#green]-> stop_check_alert : 0

    $initFlow("stop", "alert", "mon~yarn~block~ctl~infrars~ibdm~json~backup~bmes~hadoop")

    stop_check_hadoop -r->              stop_check   : default
    stop_check        -r->              stop_final   : default
    stop_answer       -d[#red]-> stop_denial  : 1
    stop_answer       -d[#red]-> stop_timeout : 2

    $initStep("stop", "alert~mon~yarn~block~ctl~infrars~ibdm~json~backup~bmes~hadoop")

    stop_run_hadoop -u[#green]-> stop_check : 0
    stop_check      -d[#red]->   stop_error : 1
    stop_denial     -d[#red]->   stop_error : default
    stop_timeout    -d[#red]->   stop_error : default
    stop_request    -d[#red]->   stop_error : default
    stop_answer     -d[#red]->   stop_error : default
    stop_error      -u->                stop_final : default
    stop_final      -r->                [*]
}
Stop  --> Start
Start --> Stop
state Start {
    $initStatesStart()
    [*]           -r-> start_init
    start_init    -r-> start_request      : default
    start_request -r-> start_answer       : default
    start_answer  -r[#green]-> start_check_hadoop : 0

    $initFlow("start", "hadoop", "ibdm~infrars~ctl~block~bmes~mon")

    start_check_mon -r->              start_check   : default
    start_check     -r->              start_final   : default
    start_answer    -d[#red]-> start_denial  : 1
    start_answer    -d[#red]-> start_timeout : 2

    $initStep("start", "hadoop~ibdm~infrars~ctl~block~bmes~mon")

    start_run_mon -u[#green]-> start_check : 0
    start_check   -d[#red]->   start_error : 1
    start_denial  -d[#red]->   start_error : default
    start_timeout -d[#red]->   start_error : default
    start_request -d[#red]->   start_error : default
    start_answer  -d[#red]->   start_error : default
    start_error   -u->                start_final : default
    start_final   -r->                [*]
}
Start --> [*]
@enduml
```
