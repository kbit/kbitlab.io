```plantuml
@startuml
!theme sunlust
skinparam {
    Default {
        TextAlignment left
        FontName Dejavu Serif
    }
    Legend {
        FontName VL Gothic
        BorderColor transparent
    }
    WrapWidth 400
    Shadowing false
}
left header Timing Diagram
right footer PlantUML %version()
scale 1 as 35 pixels
concise "Phases" as ph
concise "Mobile App" as fm
concise "API Application" as exo
concise "Single-Page App" as wm

@ph
highlight 4  to 22  #TECHNOLOGY
highlight 22 to 32  #APPLICATION
highlight 3  to 5:  Freezy
highlight 21 to 23: Freezy
highlight 31 to 33: Final

@4  <--> @22 : Development
@22 <--> @32 : Release

fm  is "old release"
exo is "old release"
wm  is "old release"

@wm
3  is 4.7.0
5  is 4.7.1
9  is 4.7.2
13 is 4.7.3
18 is 4.7.4
22 is 4.7.5
30 is 4.7.6
32 is 4.8.0

@exo
4  is 0.5.0
exo -> fm@+1 : API dependency
7  is 0.5.1
exo -> fm@+3 : API dependency
22 is 0.5.2
30 is 0.5.4
32 is 0.6.0

@fm
5  is 1.1.0
10 is 1.1.1
20 is 1.1.2
22 is 1.1.3
26 is 1.1.4
30 is 1.1.5
32 is 1.2.0
!$json = {
    "strs": [
        "<b>Аквалангист</b>",
        "",
        "Чтоб овладеть грудным регистром",
        "Я становлюсь Аквалангистом",
        "Сейчас начну я опускаться.",
        "Мой голос будет отражаться",
        "",
        "И в резонаторе грудном",
        "И непременно в головном. (идет понижение)",
        "Все ниже опускаюсь, ниже",
        "А дно морское ближе, ближе (грудной регистр)",
        "",
        "И вот уж в царстве я подводном",
        "Хоть опустился глубоко",
        "Но голосом грудным свободным",
        "Распоряжаюсь я легко",
        "",
        "Чтоб овладеть грудным регистром",
        "Полезно стать аквалангистом."
    ]
}
legend left
    !$idx = 0
    !foreach $opd in %splitstr('ТКП.КПТ.КТП.ГДБ.ДБГ.ДГБ.РЛМ.ЛМР.ЛРМ.ЖЛР.ЛРЖ.ЛЖР.МНЛ.НЛМ.НМЛ.ТЧФ.ЧФТ.ЧТФ.ЗЖДР', '.')
        !$str = '<#transparent,#transparent>|'
        !foreach $end in %splitstr('И.Э.А.О.У.Ы.Е.Я.Ё.Ю', '.')
            !$wrd = $opd + $end
            !$str = $str + $wrd + ' | '
        !endfor
        !$str = $str + $json.strs[$idx] + ' |'
        $str
        !$idx = $idx + 1
    !endfor
endlegend
@enduml
```
