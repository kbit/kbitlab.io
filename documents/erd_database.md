```plantuml
@startuml
!theme sunlust
!procedure $send($left, $right,$v='D')
    !foreach $l in %splitstr($left, "~")
        !foreach $r in %splitstr($right, "~")
            $l ||.$v.|{ $r
        !endfor
    !endfor
!endprocedure
left header [[https://c4model.com C4M: Component Diagram]] / Entity Relationship Diagram
left to right direction
caption container 'Database'
skinparam linetype polyline

folder "Database" #line.dashed {
    package App <<domain>> #line.dotted {
        map Productes {
            id      => INTEGER
            name    => STRING
            service => INTEGER
        }
        map Containers {
            product => INTEGER
            service => INTEGER
        }
        map Services {
            host => INTEGER
            name => STRING
            id   => INTEGER
        }
        $send('Productes::id', 'Containers::product' , 'U')
        $send('Services::id', 'Containers::service', 'U')
    }

    package Infra <<domain>> #line.dotted {
        map Networks {
            name    => STRING
            id      => INTEGER
            address => STRING
            color   => STRING
        }
        map OSs {
            name  => STRING
            id    => INTEGER
            pkg   => STRING
            color => STRING
        }
        map Hosts {
            id     => INTEGER
            name   => STRING
            domain => STRING
            vCPU   => INTEGER
            RAM    => INTEGER
            HDD    => INTEGER
            OS     => INTEGER
        }
        map Domains {
            id   => INTEGER
            name => STRING
        }
        map Connections {
            host    => INTEGER
            network => INTEGER
            domain => INTEGER
        }
        map Protocols {
            id    => INTEGER
            name  => STRING
            color => STRING
        }
        map Integrations {
            service  => INTEGER
            protocol => INTEGER
        }
        $send('Services::host', 'Hosts::id')
        $send('Networks::id', 'Connections::network', 'U')
        $send('Domains::id', 'Connections::domain', 'U')
        $send('Hosts::id', 'Connections::host')
        $send('OSs::id', 'Hosts::OS', 'U')
        $send('Services::id', 'Integrations::service')
        $send('Protocols::id', 'Integrations::protocol', 'U')
    }
}
@enduml
```
