```plantuml
@startuml
!theme sunlust
hide empty members
skinparam linetype ortho

left header UML: Class Diagram
'left to right direction
caption component 'Mainframe'
set separator ::
package "mainframe" <<package>> as api #line.dashed {
    abstract Hadoop {
        +Url : string
        +Name : string
        +Api : string
        +Hdfs : string
        +Jdbc : string
        +Psql : string
        +Yarn : string
        +Hive : string
        +Oozie : string
        +Zookeeper : string
        +Hosts : []string
        +Sorces : []string
        +Properties : map[string]map[string]string
        -saveConfig() : void
    }

    interface Cluster {
        +SetParams(string) : void
        -setPropers(string) : void
    }

    class HDP implements Cluster
    class HDP extends Hadoop
    class CDH implements Cluster
    class CDH extends Hadoop
}
legend left
    # TODO
endlegend
@enduml
```
