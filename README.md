# [kBit](//kbit.gitlab.io)

## PL (Program Language)

| **Name** | **Lint** | **Formatter** |
|:--------:|:--------:|:-------------:|
|[Rust](https://rust-lang.org)| [clippy](https://rust-lang.org) | [rustfmt](https://rust-lang.org) |
|[Python](https://python.org)| [pylint](https://pylint.readthedocs.io) / [bandit](https://bandit.readthedocs.io) | [black](https://black.readthedocs.io) |
