package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
	"strings"
)

// HTTP2str doc
func HTTP2str(link string) string {
	caCertPool, err := x509.SystemCertPool()
	client := &http.Client{Transport: &http.Transport{TLSClientConfig: &tls.Config{
		MinVersion:         tls.VersionTLS12,
		MaxVersion:         tls.VersionTLS12,
		RootCAs:            caCertPool,
		InsecureSkipVerify: true}}}
	req, err := http.NewRequest(http.MethodGet, link, nil)
	req.Header.Add("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(os.Getenv("CRED"))))
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("ERROR:\t%s\n", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	return string(body)
}

// Hadoop doc
type Hadoop struct {
	Url, Name, Api, Hdfs, Jdbc, Psql, Yarn, Hive, Oozie, Zookeeper string
	Hosts, Sorces                                                  []string
	Properties                                                     map[string]map[string]string
}

func (hdp *Hadoop) saveConfig() {
	res, err := json.Marshal(hdp)
	if err != nil {
		panic(err)
	}
	ioutil.WriteFile(hdp.Hdfs+".json", []byte(string(res)), 0644)
	fmt.Println(hdp.Hdfs)
}

// HDP doc
type HDP struct {
	Hadoop
}

// CDH doc
type CDH struct {
	Hadoop
}

// List doc
type List struct {
	XMLName xml.Name   `xml:"properties"`
	List    []Property `xml:"property"`
}

// Property doc
type Property struct {
	XMLName xml.Name `xml:"property"`
	Name    string   `xml:"name"`
	Value   string   `xml:"value"`
}

// Cluster doc
type Cluster interface {
	SetParams(string)
	setPropers(string) map[string]string
	saveBackup()
}

// SetParams doc
func (hdp *HDP) SetParams(link string) {
	defer hdp.saveBackup()
	defer hdp.saveConfig()
	URL, err := url.Parse(link)
	if err != nil {
		panic(err)
	}
	hdp.Url = URL.Scheme + "://" + URL.Host
	var result map[string]interface{}
	hdp.Api = "/api/v1/clusters/"
	json.Unmarshal([]byte(HTTP2str(hdp.Url+hdp.Api)), &result)
	hdp.Name = result["items"].([]interface{})[0].(map[string]interface{})["Clusters"].(map[string]interface{})["cluster_name"].(string)
	json.Unmarshal([]byte(HTTP2str(hdp.Url+hdp.Api+hdp.Name+"/hosts")), &result)
	for _, service := range result["items"].([]interface{}) {
		hdp.Hosts = append(hdp.Hosts, service.(map[string]interface{})["Hosts"].(map[string]interface{})["host_name"].(string))
	}
	json.Unmarshal([]byte(HTTP2str(hdp.Url+hdp.Api+hdp.Name+"/services")), &result)
	hdp.Properties = make(map[string]map[string]string)
	for _, service := range result["items"].([]interface{}) {
		item := service.(map[string]interface{})["ServiceInfo"].(map[string]interface{})["service_name"].(string)
		hdp.Properties[item] = hdp.setPropers(item)
	}
	URL, err = url.Parse(hdp.Properties["HDFS"]["core-site.fs.defaultFS"])
	if err != nil {
		panic(err)
	}
	hdp.Hdfs = URL.Host
	hdp.Sorces = strings.Split(hdp.Properties["HDFS"]["hdfs-site.dfs.nameservices"], ",")[1:]
	hdp.Zookeeper = hdp.Properties["HIVE"]["hive-site.hive.zookeeper.quorum"]
	hdp.Hive = hdp.Zookeeper
	hdp.Jdbc = "jdbc:hive2://" + hdp.Zookeeper + "/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2"
	hdp.Psql = hdp.Properties["HIVE"]["hive-site.javax.jdo.option.ConnectionURL"]
	hdp.Yarn = hdp.Properties["YARN"]["yarn-site.yarn.resourcemanager.address"]
	hdp.Oozie = hdp.Properties["OOZIE"]["oozie-site.oozie.base.url"]
}

func (hdp *HDP) setPropers(service string) map[string]string {
	Properties := make(map[string]string)
	var result map[string]interface{}
	json.Unmarshal([]byte(HTTP2str(hdp.Url+hdp.Api+hdp.Name+"/configurations/service_config_versions?service_name.in("+service+")&is_current=true")), &result)
	for _, item := range result["items"].([]interface{}) {
		for _, configuration := range item.(map[string]interface{})["configurations"].([]interface{}) {
			for key, value := range configuration.(map[string]interface{})["properties"].(map[string]interface{}) {
				if key != "content" && value != nil {
					Properties[configuration.(map[string]interface{})["type"].(string)+"."+key] = value.(string)
				}
			}
		}
	}
	return Properties
}

func (hdp *HDP) saveBackup() {
	var data = [][]string{
		{
			hdp.Name,
			"cluster_info.json",
			"hosts",
		},
		{
			hdp.Name + "/hosts",
			"hosts/hosts.json",
			"hosts",
		},
		{
			hdp.Name + "/configurations",
			"configurations/" + hdp.Hdfs + "_configurations.json",
			"configurations",
		},
		{
			hdp.Name + "/config_groups",
			"config_groups/" + hdp.Hdfs + "_config_groups.json",
			"config_groups",
		},
		{
			hdp.Name + "/services",
			"services.json",
			"services",
		},
	}
	for _, host := range hdp.Hosts {
		data = append(data, []string{
			hdp.Name + "/hosts/" + host,
			"hosts/" + hdp.Hdfs + "_host.json",
			"hosts",
		})
		data = append(data, []string{
			hdp.Name + "/hosts/" + host + "/host_components",
			"hosts/" + host + "_host_components.json",
			"hosts",
		})
	}
	for service := range hdp.Properties {
		data = append(data, []string{
			hdp.Name + "/configurations/service_config_versions?service_name.in(" + service + ")&is_current=true",
			service + "/" + service + "_config.json",
			service,
		})
		data = append(data, []string{
			hdp.Name + "/configurations/service_config_versions?service_name.in(" + service + ")&service_config_version=1",
			service + "/" + service + "_INITIAL.json",
			service,
		})
	}
	var xargs = [][]string{}
	for _, row := range data {
		xargs = append(xargs, []string{
			fmt.Sprintf(
				"mkdir --parent 'dc_%s'; curl --config 'cfg' --url '%s' | jq --raw-output --sort-keys 'del(getpath(path(..|select(.sensitive)?)))' > 'dc_%s'",
				path.Dir(hdp.Hdfs+"/SDP/"+row[1]),
				hdp.Url+hdp.Api+row[0],
				hdp.Hdfs+"/SDP/"+row[1],
			)},
		)
	}
	file, err := os.Create(hdp.Hdfs + ".csv")
	if err != nil {
		panic(err)
	}
	var writer = csv.NewWriter(file)
	if err = writer.WriteAll(xargs); err != nil {
		panic(err)
	}
}

// SetParams doc
func (cdh *CDH) SetParams(link string) {
	defer cdh.saveBackup()
	defer cdh.saveConfig()
	URL, err := url.Parse(link)
	if err != nil {
		panic(err)
	}
	cdh.Url = URL.Scheme + "://" + URL.Host
	var result map[string]interface{}
	cdh.Api = "/api/v19/clusters/"
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api)), &result)
	cdh.Name = strings.Replace(result["items"].([]interface{})[0].(map[string]interface{})["displayName"].(string), " ", "%20", -1)
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts")), &result)
	for _, host := range result["items"].([]interface{}) {
		cdh.Hosts = append(cdh.Hosts, host.(map[string]interface{})["hostname"].(string))
	}
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services")), &result)
	cdh.Properties = make(map[string]map[string]string)
	for _, service := range result["items"].([]interface{}) {
		item := service.(map[string]interface{})["name"].(string)
		cdh.Properties[strings.ToUpper(item)] = cdh.setPropers(item)
	}
	cdh.Hdfs = strings.Split(cdh.Properties["HDFS"]["dfs.nameservices"], ",")[0]
	cdh.Sorces = strings.Split(cdh.Properties["HDFS"]["dfs.nameservices"], ",")[1:]
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services/hive/roleConfigGroups/hive-HIVESERVER2-BASE/roles")), &result)
	cdh.Hive = result["items"].([]interface{})[0].(map[string]interface{})["hostRef"].(map[string]interface{})["hostId"].(string)
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts/"+cdh.Hive)), &result)
	cdh.Hive = result["hostname"].(string)
	cdh.Jdbc = "jdbc:hive2://" + cdh.Hive + ":10000/default;principal=hive/_HOST@DF.SBRF.RU"
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services/yarn/roleConfigGroups/yarn-RESOURCEMANAGER-BASE/roles")), &result)
	cdh.Yarn = result["items"].([]interface{})[0].(map[string]interface{})["hostRef"].(map[string]interface{})["hostId"].(string)
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts/"+cdh.Hive)), &result)
	cdh.Yarn = result["hostname"].(string)
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services/oozie/roleConfigGroups/oozie-OOZIE_SERVER-BASE/roles")), &result)
	cdh.Oozie = result["items"].([]interface{})[0].(map[string]interface{})["hostRef"].(map[string]interface{})["hostId"].(string)
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts/"+cdh.Oozie)), &result)
	cdh.Oozie = "http://" + result["hostname"].(string) + ":11000/oozie"
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services/zookeeper/roleConfigGroups/zookeeper-SERVER-BASE/roles")), &result)
	cdh.Zookeeper = result["items"].([]interface{})[0].(map[string]interface{})["hostRef"].(map[string]interface{})["hostId"].(string)
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts/"+cdh.Zookeeper)), &result)
	cdh.Zookeeper = result["hostname"].(string) + ":2181"
}

func (cdh *CDH) setPropers(service string) map[string]string {
	Properties := make(map[string]string)
	var result map[string]interface{}
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services/"+service+"/config?view=full")), &result)
	for _, configuration := range result["items"].([]interface{}) {
		if configuration.(map[string]interface{})["name"] != nil && configuration.(map[string]interface{})["value"] != nil {
			if strings.Contains(configuration.(map[string]interface{})["name"].(string), "_config_safety_valve") {
				var p List
				xml.Unmarshal([]byte("<properties>"+configuration.(map[string]interface{})["value"].(string)+"</properties>"), &p)
				for _, value := range p.List {
					Properties[value.Name] = value.Value
				}
			} else if !strings.Contains(configuration.(map[string]interface{})["name"].(string), "yarn_fs_scheduled_allocations") {
				Properties[strings.Replace(configuration.(map[string]interface{})["name"].(string), "_", ".", -1)] = configuration.(map[string]interface{})["value"].(string)
			}
		}
	}
	json.Unmarshal([]byte(HTTP2str(cdh.Url+cdh.Api+cdh.Name+"/services/"+service+"/roleConfigGroups")), &result)
	for _, item := range result["items"].([]interface{}) {
		for _, configuration := range item.(map[string]interface{})["config"].(map[string]interface{})["items"].([]interface{}) {
			if configuration.(map[string]interface{})["name"] != nil && configuration.(map[string]interface{})["value"] != nil {
				if strings.Contains(configuration.(map[string]interface{})["name"].(string), "_config_safety_valve") {
					var p List
					xml.Unmarshal([]byte("<properties>"+configuration.(map[string]interface{})["value"].(string)+"</properties>"), &p)
					for _, value := range p.List {
						Properties[value.Name] = value.Value
					}
				} else {
					Properties[strings.Replace(configuration.(map[string]interface{})["name"].(string), "_", ".", -1)] = configuration.(map[string]interface{})["value"].(string)
				}
			}
		}
	}
	return Properties
}

func (cdh *CDH) saveBackup() {
	var data = [][]string{
		{
			"clusters/" + cdh.Name + "/services",
			"services.json",
			"cm",
		},
		{
			"hosts",
			"hosts/hosts.json",
			"hosts",
		},
		{
			"cm/allHosts/config?view=full",
			"hosts/all_hosts_config.json",
			"all hosts",
		},
		{
			"cm/version",
			"cm_version.json",
			"cm",
		},
		{
			"cm/service/roles",
			"cms/cms_roles.json",
			"cm",
		},
		{
			"cm/service/config?view=full",
			"cms/cms_service_config.json",
			"cm",
		},
		{
			"cm/service/roleConfigGroups",
			"cms/cms_roleGroups/cms_roleGroups.json",
			"cm",
		},
	}
	var result map[string]interface{}
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/cm/service/roleConfigGroups")), &result)
	for _, roleConfigGroup := range result["items"].([]interface{}) {
		data = append(data, []string{
			"cm/service/roleConfigGroups/" + roleConfigGroup.(map[string]interface{})["name"].(string) + "/config?view=full",
			"cms/cms_roleGroups/cms_" + roleConfigGroup.(map[string]interface{})["name"].(string) + ".json",
			"cm",
		})
	}
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts")), &result)
	for _, host := range result["items"].([]interface{}) {
		data = append(data, []string{
			"hosts/" + host.(map[string]interface{})["hostId"].(string) + "/config?view=full",
			"hosts/host_" + host.(map[string]interface{})["hostname"].(string) + ".json",
			"cm",
		})
	}
	var res map[string]interface{}
	json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/cm/service/roles")), &result)
	for _, role := range result["items"].([]interface{}) {
		json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts/"+role.(map[string]interface{})["hostRef"].(map[string]interface{})["hostId"].(string))), &res)
		data = append(data, []string{
			"cm/service/roles/" + role.(map[string]interface{})["name"].(string) + "/config?view=full",
			"cms/cms_roles/cms_" + role.(map[string]interface{})["serviceRef"].(map[string]interface{})["serviceName"].(string) + "-" + role.(map[string]interface{})["type"].(string) + res["hostname"].(string) + ".json",
			"cm",
		})
	}
	for service := range cdh.Properties {
		service = strings.ToLower(service)
		data = append(data, []string{
			"clusters/" + cdh.Name + "/services/" + service + "/roles",
			service + "/service_" + service + "_roles.json",
			"service",
		})
		data = append(data, []string{
			"clusters/" + cdh.Name + "/services/" + service + "/config?view=full",
			service + "/service_" + service + "_roles.json",
			"service",
		})
		data = append(data, []string{
			"clusters/" + cdh.Name + "/services/" + service + "/roleConfigGroups",
			service + "/service_" + service + "_roleGroups/" + service + "_roleGroups.json",
			"service",
		})
		json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/clusters/"+cdh.Name+"/services/"+service+"/roleConfigGroups")), &result)
		for _, roleConfigGroup := range result["items"].([]interface{}) {
			data = append(data, []string{
				"clusters/" + cdh.Name + "/services/" + service + "/roleConfigGroups/" + roleConfigGroup.(map[string]interface{})["name"].(string) + "/config?view=full",
				service + "/" + service + "_roleGroups/" + service + "_" + roleConfigGroup.(map[string]interface{})["name"].(string) + "_config.json",
				"service",
			})
			json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/clusters/"+cdh.Name+"/services/"+service+"/roleConfigGroups/"+roleConfigGroup.(map[string]interface{})["name"].(string)+"/roles")), &res)
			for _, role := range result["items"].([]interface{}) {
				if role.(map[string]interface{})["hostRef"] != nil {
					var r map[string]interface{}
					json.Unmarshal([]byte(HTTP2str(cdh.Url+"/api/v19/hosts/"+role.(map[string]interface{})["hostRef"].(map[string]interface{})["hostId"].(string))), &r)
					data = append(data, []string{
						"clusters/" + cdh.Name + "/services/" + service + "/roles/" + role.(map[string]interface{})["name"].(string) + "/config?view=full",
						service + "/" + service + "_roles/" + service + "_" + role.(map[string]interface{})["type"].(string) + r["hostname"].(string) + ".json",
						"cm",
					})
				}
			}
		}
	}
	var xargs = [][]string{}
	for _, row := range data {
		xargs = append(xargs, []string{
			fmt.Sprintf(
				"mkdir --parent 'dc_%s'; curl --config 'cfg' --url '%s' | jq --raw-output --sort-keys 'del(getpath(path(..|select(.sensitive)?)))' > 'dc_%s'",
				path.Dir(cdh.Hdfs+"/CDH/"+row[1]),
				cdh.Url+"/api/v19/"+row[0],
				cdh.Hdfs+"/CDH/"+row[1],
			)},
		)
	}
	file, err := os.Create(cdh.Hdfs + ".csv")
	if err != nil {
		panic(err)
	}
	var writer = csv.NewWriter(file)
	if err = writer.WriteAll(xargs); err != nil {
		panic(err)
	}
}

func mapreduce() {
	const data = "86967897737416471853297327050364959 11861322575564723963297542624962850 70856234701860851907960690014725639 38397966707106094172783238747669219 52380795257888236525459303330302837 58495327135744041048897885734297812 69920216438980873548808413720956532 16278424637452589860345374828574668"
	children := []chan int{}
	for i, segment := range strings.Fields(data) {
		children = append(children, make(chan int))
		go f(segment, children[i])
	}
	sum := 0
	for _, thread := range children {
		sum += <-thread
	}
	log.Println(sum)
}

func f(segment string, ch chan int) {
	defer close(ch)
	sum := 0
	for _, char := range strings.SplitAfter(segment, "") {
		if i, err := strconv.Atoi(char); err == nil {
			sum += i
		}
	}
	ch <- sum
}

func main() {
	var link string
	flag.StringVar(&link, "l", "http://127.0.0.1:8080", "link to ambari")
	flag.Parse()
	if strings.Contains(link, "8080") {
		var cluster = HDP{Hadoop{}}
		cluster.SetParams(link)
	} else {
		var cluster = CDH{Hadoop{}}
		cluster.SetParams(link)
	}
}
