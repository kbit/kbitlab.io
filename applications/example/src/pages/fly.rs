use {
    gloo_net::http::Request,
    yew::{function_component, functional::UseStateHandle, html, platform, use_state, Html},
    yew_patternfly_v3::{
        components::{Table, TableData, TableRow, TableStyle},
        css::{Col, ColSize, Container, Image, IncludeCDN, Jumbotron, Row},
    },
};

#[derive(Clone, PartialEq, serde::Deserialize)]
struct Experience {
    start: String,
    stop: String,
    role: String,
    name: String,
    note: Vec<String>,
}

#[function_component]
pub fn Fly() -> Html {
    let education: UseStateHandle<Vec<Vec<String>>> = use_state(|| Vec::new());
    {
        let education = education.clone();
        platform::spawn_local(async move {
            education.set(
                Request::get("assets/education.json")
                    .send()
                    .await
                    .unwrap()
                    .json()
                    .await
                    .unwrap(),
            )
        });
    };
    let experience: UseStateHandle<Vec<Experience>> = use_state(|| Vec::new());
    {
        let experience = experience.clone();
        platform::spawn_local(async move {
            experience.set(
                Request::get("assets/experience.json")
                    .send()
                    .await
                    .unwrap()
                    .json()
                    .await
                    .unwrap(),
            );
        });
    };
    html! {
        <>
        <Jumbotron>
            <Container>
                <Table style={TableStyle::Striped}>
                    <TableRow>
                        <TableData>
                            <Image src="https://i.imgur.com/s6WgE0Ct.png"/>
                        </TableData>
                        <TableData>
                            <h2>{"Artem V. Ageev. System Administrator with over a decade of experience:"}</h2>
                            <ul>
                                <li>{"writing CICD automation scripts (Groovy for Jenkins) and cli tools (Rust and Go) with data markup languages (JSON, CSV, SQLite and KDBX)"}</li>
                                <li>{"maintenancing nodes (RedHat, Debian and FreeBSD), network (MicroTik and Cisco) and virt (Qemu and Podman) via Ansible (Bash and PowerShell)"}</li>
                                <li>{"maintenance services Prometheus (Grafana, Loki), Gitea, DroneCI, JFrog Artifactory and SonarQube"}</li>
                            </ul>
                        </TableData>
                    </TableRow>
                </Table>
            </Container>
        </Jumbotron>
        <Container>
            <h2>{"EXPERIENCE"}</h2>
            <Table><tbody> { experience.iter().map(|obj| html! {
                <TableRow>
                    <TableData>
                        <h4>{&obj.start}<br/>{&obj.stop}</h4>
                    </TableData>
                    <TableData>
                        <h4>{&obj.role}<br/>{&obj.name}</h4>
                    </TableData>
                    <TableData>
                        <ul> {obj.note.iter().map(|row| html! {
                            <li>{row}</li>
                        }).collect::<Html>()} </ul>
                    </TableData>
                </TableRow>
            }).collect::<Html>()} </tbody></Table>
        </Container>
        <Container>
            <h2>{"EDUCATION"}</h2>
            <Table><tbody> {education.iter().map(|row| html! {
                <TableRow> {
                    row.iter().map(|cell| html! {
                        <TableData>{cell}</TableData>
                    }).collect::<Html>()
                } </TableRow>
            }).collect::<Html>()} </tbody></Table>
            <Row>
                <Col size={ColSize::Three}>
                    <a href="mailto:artem.v.ageev@gmail.com">
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"/>{" email"}
                    </a>
                </Col>
                <Col size={ColSize::Three}>
                    <a href="//t.me/Artem_V_ageev">
                        <span class="glyphicon glyphicon-send" aria-hidden="true"/>{" messeger"}
                    </a>
                </Col>
                <Col size={ColSize::Three}>
                    <a href="//gitlab.com/kbit">
                        <span class="glyphicon glyphicon-file" aria-hidden="true"/>{" portfolio"}
                    </a>
                </Col>
                <Col size={ColSize::Three}>
                    <a href="//www.linkedin.com/in/artem-v-ageev">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"/>{" network"}
                    </a>
                </Col>
            </Row>
        </Container>
        <IncludeCDN/>
        </>
    }
}
