use {
    yew::{function_component, html, Html},
    yew_and_bulma::{
        columns::{Column, Columns},
        elements::{
            content::Content,
            image::{Figure, Image, Size as FS},
            table::{Table, TableData, TableRow},
            title::{Subtitle, Title},
        },
        helpers::{
            color::{BackgroundColor, Color, TextColor},
            typography::FontFamily,
        },
        layout::{
            container::{Container, Width},
            hero::{Hero, HeroBody, HeroFoot, HeroHead, Size as HS},
            section::Section,
        },
        utils::class::ClassBuilder,
    },
};

#[function_component]
pub fn CV() -> Html {
    let code = ClassBuilder::default()
        .with_font_family(Some(FontFamily::Code))
        .build();
    html! {
        <Container width={Width::FullHD}>
            <Hero class={code} size={HS::FullHeight} color={Color::White}>
                <HeroHead>
                    <About/>
                </HeroHead>
                <HeroBody>
                    <Experience/>
                </HeroBody>
                <HeroFoot>
                    <Education/>
                    <Contacts/>
                </HeroFoot>
            </Hero>
            <link
                rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css"
            />
            <link
                rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/@mdi/font@7.2.96/css/materialdesignicons.min.css"
            />
        </Container>
    }
}

#[function_component]
pub fn About() -> Html {
    let class = ClassBuilder::default()
        .with_background_color(Some(BackgroundColor::Info))
        .with_text_color(Some(TextColor::White))
        .with_font_family(Some(FontFamily::Code))
        .build();
    html! {
        <Section class={class.clone()}>
            <Columns>
                <Column narrow=true>
                    <Figure size={FS::Pixels128x128}>
                        <Image src={"https://i.imgur.com/s6WgE0C.png"}/>
                    </Figure>
                </Column>
                <Column>
                    <Content>
                        <Subtitle class={class}>
                            {"Artem V. Ageev. System Administrator with over a decade of experience:"}
                        </Subtitle>
                        <ul>
                            <li>{"writing CICD pipelines, automation scripts and CLI / GUI tools (Python and Rust) with data markup languages (JSON, CSV, SQLite and KDBX)"}</li>
                            <li>{"maintenancing nodes (RedHat, Debian and FreeBSD), network (MicroTik and Cisco) and virt (Qemu and Podman) via Ansible (Bash and PowerShell)"}</li>
                            <li>{"maintenance services Prometheus (Grafana, Loki), Gitea, DroneCI, JFrog Artifactory and SonarQube"}</li>
                        </ul>
                    </Content>
                </Column>
            </Columns>
        </Section>
    }
}

#[function_component]
pub fn Education() -> Html {
    html! {
        <Section>
            <Content>
                <Title>{"EDUCATION"}</Title>
            </Content>
            <Table full_width=true>
                <TableRow>
                    <TableData>{"2009-2013"}</TableData>
                    <TableData>{"Engineer. Informatics in management"}</TableData>
                    <TableData>{"Moscow City Pedagogical University (Moscow)"}</TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"2008-2009"}</TableData>
                    <TableData>{"Signaller, Cynologist, Parachutist"}</TableData>
                    <TableData>{"Armed forces (Russian)"}</TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"2005-2008"}</TableData>
                    <TableData>{"Technique. Automated information processing and control systems"}</TableData>
                    <TableData>{"College of Entrepreneurship 11 (Moscow)"}</TableData>
                </TableRow>
            </Table>
        </Section>
    }
}

#[function_component]
pub fn Contacts() -> Html {
    html! {
        <Section>
            <Columns>
                <Column>
                    <a href="mailto:artem.v.ageev@gmail.com"><span class="mdi mdi-email"/>{" artem.v.ageev@gmail.com"}</a>
                </Column>
                <Column>
                    <a href="//t.me/Artem_V_Ageev"><span class="mdi mdi-message"/>{" @artem_v_ageev"}</a>
                </Column>
                <Column>
                    <a href="//gitlab.com/kbit"><span class="mdi mdi-gitlab"/>{" gitlab.com/kbit"}</a>
                </Column>
                <Column>
                    <a href="//www.linkedin.com/in/artem-v-ageev"><span class="mdi mdi-linkedin"/>{" in/artem-v-ageev"}</a>
                </Column>
            </Columns>
        </Section>
    }
}

#[function_component]
pub fn Experience() -> Html {
    html! {
        <Container width={Width::FullHD}>
            <Content>
                <Title>{"EXPERIENCE"}</Title>
            </Content>
            <Table>
                <TableRow>
                    <TableData>{"DEC_2022"}<br/>{"APR_2023"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SYSTEM ADMINISTRATOR"}</Column>
                            <Column>{"EGAR TECHNOLOGY (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"writing automation scripts on Groovy (Jenkins Pipelines) and Rust (clap, gtk, yew (bulma, patternfly), teloxide, axum)"}</li>
                                <li>{"maintenance services Prometheus, Grafana, Loki, GrayLog, Gitea, DroneCI, JFrog Artifactory and SonarQube"}</li>
                                <li>{"maintenance servers (Alma Linux, Windows Core) via Ansible (Python Testinfra)"}</li>
                                <li>{"descripting via PlantUML (MindMap, C4M, UML, Network)"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"DEC_2021"}<br/>{"OCT_2022"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SYSTEM ADMINISTRATOR"}</Column>
                            <Column>{"SBER (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"maintenance servers (RedHat), virt (OpenShift, SberCloud) via Ansible and Terraform"}</li>
                                <li>{"maintenance services HortonWorks (Zookeeper, HDFS, Yarn, Hive, HBase, Ranger, Spark, Oozie, Hue) via Ambari"}</li>
                                <li>{"writing automation scripts on Groovy (Jenkins Pipelines) and Rust (clap, serde (json, yaml, xml-rs), xlsxwriter, csv, kdbx)"}</li>
                                <li>{"descripting and prototyping on PlantUML (C4M, UML) and EvolusPencil (Prototype GUI)"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"JUN_2018"}<br/>{"DEC_2021"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SYSTEM ADMINISTRATOR"}</Column>
                            <Column>{"AT CONSULTING (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"maintenance of servers (CentOS), virt (Podman, GCP) via Ansible, Vagrant and Itamae"}</li>
                                <li>{"maintenance services DBMS (Postgres, MariaDB), web (HAProxy, Nginx), authorization (FreeIPA, KeyCloack), mail (Zimbra)"}</li>
                                <li>{"writing automation scripts in Groovy (Jenkins DSL) and GoLang (std) with data markup languages (JSON, CSV, XML)"}</li>
                                <li>{"descripting and prototyping on PlantUML (UML, Archimate) and Camunda Modeler (BPMN)"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"JAN_2018"}<br/>{"JUN_2018"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SYSTEM ADMINISTRATOR"}</Column>
                            <Column>{"I-TECO (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"maintenance of servers (CentOS, FreeBSD), virt (VMware, Docker), network equipment (FreePBX, VyOS, TrueNAS) via Ansible"}</li>
                                <li>{"installation and maintenance services DBMS (Postgres, MySQL), web (Nginx, Apache) and authorization (FreeIPA, Glewlwyd)"}</li>
                                <li>{"writing automation scripts on GitLab Pipeline, Python (Fabric, WinRM, TestInfra) with data markup languages (YAML, CSV)"}</li>
                                <li>{"descripting and prototyping via ArchimatTool and Draw.io (FloorPlan)"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"OCT_2015"}<br/>{"JAN_2018"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SYSTEM ADMINISTRATOR"}</Column>
                            <Column>{"RDTEX (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"maintenance of servers (CentOS, Debian), virt (Qemu), network equipment (MikroTik, Cisco, Netgears) via Ansible"}</li>
                                <li>{"writing automation scripts on Groovy (Jenkins Scriptable Pipeline) and Python (std, xlwt)"}</li>
                                <li>{"resolving issues, documenting solutions and accounting resources in Jira and Confluence"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"DEC_2014"}<br/>{"SEP_2015"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SYSTEM ADMINISTRATOR"}</Column>
                            <Column>{"INTELLIN (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"maintenance of servers (CentOS, Windows Core) and virt (Hyper-V) via Ansible (WinRM, SSH)"}</li>
                                <li>{"writing automation scripts on Bash (Bats) and PowerShell (PSScriptAnalyzer, Pester)"}</li>
                                <li>{"resolving issues, documenting solutions and accounting resources in GLPI, OTRS, ‘OCS Inventory’, and Zabbix"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
                <TableRow>
                    <TableData>{"SEP_2012"}<br/>{"DEC_2014"}</TableData>
                    <TableData>
                        <Columns>
                            <Column>{"SUPPORT ENGINEER"}</Column>
                            <Column>{"MAGNOLIA (MOSCOW)"}</Column>
                        </Columns>
                        <Content>
                            <ul>
                                <li>{"installation and maintenance workstations of company employees (Windows) and of POS (SUSE Linux) via RDP and SSH"}</li>
                                <li>{"writing automation scripts on Bash (shellcheck, shunit2, awk, bc, jq) and PowerShell (PSScriptAnalyzer)"}</li>
                                <li>{"resolving issues, documenting solutions and accounting resources in ‘Request Tracker 4’, OpenVAS and MediaWiki"}</li>
                            </ul>
                        </Content>
                    </TableData>
                </TableRow>
            </Table>
        </Container>
    }
}
