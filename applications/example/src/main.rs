mod pages;

use {
    yew::{function_component, html, Html, Renderer},
    yew_router::prelude::{BrowserRouter, Routable, Switch},
};

#[derive(Clone, Routable, PartialEq, Eq)]
pub enum AppRoute {
    #[at("/")]
    Home,
    #[at("/cv")]
    CV,
    #[at("/fly")]
    Fly,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: AppRoute) -> Html {
    match routes {
        AppRoute::Home => html! {<pages::CV/>},
        AppRoute::CV => html! {<pages::CV/>},
        AppRoute::Fly => html! {<pages::Fly/>},
        AppRoute::NotFound => html! { "404" },
    }
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <main>
            <BrowserRouter>
                <Switch<AppRoute> render={switch} />
            </BrowserRouter>
        </main>
    }
}

fn main() {
    Renderer::<App>::new().render();
}
