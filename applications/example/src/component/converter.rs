use {
    yew::{Component, Context, html, Html},
    yew_purecss::{Form, Input},
};

pub enum Msg {
    AddOne,
    DelOne,
}

pub struct Converter {
    value: u8,
}

impl Component for Converter {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self { value: 0 }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::AddOne => {
                if self.value < 255 {
                    self.value += 1;
                }
                true
            }
            Msg::DelOne => {
                if self.value > 0 {
                    self.value -= 1;
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <Form group=true>
                <legend>{"Converter"}</legend>
                <p>{"Celsius = "}</p><Input placeholder="Celsius" rounded=true/>
                <p>{"Fahrenheit = "}</p><Input placeholder="Fahrenheit" rounded=true/>
            </Form>
        }
    }
}
