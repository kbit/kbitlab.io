use {
    yew::{function_component, html, Html},
    yew_purecss::{Menu, MenuList, MenuItem, MenuLink},
};

#[function_component]
pub fn Navbar() -> Html {
    html! {
        <Menu horizontal=true scrollable=true>
            <MenuList>
                <MenuItem>
                    <MenuLink heading=true>{"PHOTO GALLERY"}</MenuLink>
                </MenuItem>
                <MenuItem selected=true>
                    <MenuLink>{"Home"}</MenuLink>
                </MenuItem>
                <MenuItem>
                    <MenuLink>{"Blog"}</MenuLink>
                </MenuItem>
                <MenuItem>
                    <MenuLink>{"About"}</MenuLink>
                </MenuItem>
            </MenuList>
        </Menu>
    }
}
