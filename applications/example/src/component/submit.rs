use {
    yew::{function_component, html, Html},
    yew_purecss::{Button, Form},
};

#[function_component]
pub fn Submit() -> Html {
    html! {
        <Form group=true>
            <h1>{"Submit a Photo"}</h1>
            <input placeholder="Photo URL" />
            <input type="email" placeholder="Email" />
            <Button>{"SUBMIT"}</Button>
        </Form>
    }
}
