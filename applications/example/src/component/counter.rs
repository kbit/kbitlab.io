use {
    yew::{Component, Context, html, Html},
    yew_purecss::{Button, Buttons},
};

pub enum Msg {
    AddOne,
    DelOne,
}

pub struct Counter {
    value: u8,
}

impl Component for Counter {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self { value: 0 }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::AddOne => {
                if self.value < 255 {
                    self.value += 1;
                }
                true
            }
            Msg::DelOne => {
                if self.value > 0 {
                    self.value -= 1;
                }
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <>
                <h1>{self.value}</h1>
                <Buttons>
                    <Button onclick={ctx.link().callback(|_| Msg::AddOne)} primary=true>{ "+1" }</Button>
                    <Button onclick={ctx.link().callback(|_| Msg::DelOne)}>{ "-1" }</Button>
                </Buttons>
            </>
        }
    }
}
