using System.Net.Http;


class App
{
    private static readonly HttpClient client = new HttpClient();

    static async Task<int> Main(string[] args)
    {
        Console.WriteLine(args.Length);
        var responseString = await client.GetStringAsync("https://postman-echo.com/get");
        Console.WriteLine($"responseString: {responseString}");
        return 1;
    }
}
