It's [PatternFly v3](https://pf3.patternfly.org/v3), but for
[Yew](https://github.com/yewstack/yew) in [Rust](https://www.rust-lang.org/).

**Warning:** This is in early development and it is possible it won't be
finished if there is no interest. Don't use this in production! Please help or
leave a star to let me know you are interested in this project.

Roadmap
-------

-  [ ] components
    -  [ ] [Accordion (Collapse)](https://pf3.patternfly.org/v3/pattern-library/widgets/#accordion)
    -  [ ] [Alerts](https://pf3.patternfly.org/v3/pattern-library/widgets/#alerts)
    -  [ ] [Badges](https://pf3.patternfly.org/v3/pattern-library/widgets/#badges)
    -  [x] [Spinner](https://pf3.patternfly.org/v3/pattern-library/widgets/#spinner)
