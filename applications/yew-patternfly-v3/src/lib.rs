//! # Yew Bootstrap v3 - Bootstrap v3 components for Yew
//!
//! This crate provides [Bootstrap v3][bootstrap] components made to be used with the
//! [Yew][yew] framework. It aims to make an easy, as intuitive as possible
//! development experience for integrating [Bootstrap v3][bootstrap] into your [Yew][yew]
//! frontends.
//!
//! Generally speaking, it aims to provide a Rust API for ideally* all elements,
//! components, helpers etc. that you would be able to use in CSS/HTML or other
//! frontend frameworks, such as Angular or React.
//!
//! > _* It might not be possible to expose everything in the same manner as
//! with JavaScript, but wherever it is, this crate will try and implement them._
//!
//! ### Supported Targets (for Yew Client-Side Rendering only)
//! - `wasm32-unknown-unknown`
//!
//! # Examples
//!
//! Since it is in the early stages of development, no complete example is made
//! yet.
//!
//! [bootstrap]: https://getbootstrap.com/docs/3.4/
//! [yew]: https://yew.rs
#![forbid(unsafe_code)]
pub mod components;
pub mod css;
