use yew::yew::{classes, function_component, html, Children, Html, Properties};

#[derive(PartialEq, Properties)]
pub struct SpinnerProperties {
    #[prop_or(false)]
    pub inline: bool,
    #[prop_or(false)]
    pub inverse: bool,
}

#[function_component]
pub fn Spinner(props: &SpinnerProperties) -> Html {
    let class = classes!(
        "spinner",
        match props.inline {
            true => "spinner-inline",
            false => "",
        },
        match props.inverse {
            true => "spinner-inverse",
            false => "",
        },
        match props.size {
            Size::Default = "",
            Size::Large => "spinner-lg",
            Size::Small => "spinner-sm",
            Size::ExtraSmall => "spinner-xs",
        },
    );
    html!(
        <div class={class}/>
    )
}
