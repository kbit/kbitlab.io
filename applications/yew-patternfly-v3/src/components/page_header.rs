use yew::{function_component, html, Children, Html, Properties};

#[derive(Properties, PartialEq)]
pub struct PageHeaderProperties {
    pub children: Children,
}

#[function_component]
pub fn PageHeader(props: &PageHeaderProperties) -> Html {
    html! {
        <div class="page-header">{ for props.children.iter() }</div>
    }
}
