use {
    crate::css::Color,
    yew::{classes, function_component, html, Children, Html, Properties},
};

#[derive(Properties, PartialEq)]
pub struct LabelProperties {
    pub children: Children,
    #[prop_or(Color::Default)]
    pub color: Color,
}

#[function_component]
pub fn Label(props: &LabelProperties) -> Html {
    let class = classes!(
        "label",
        match props.color {
            Color::Default => "label-default",
            Color::Primary => "label-primary",
            Color::Warning => "label-warning",
            Color::Danger => "label-danger",
            Color::Info => "label-info",
            Color::Success => "label-success",
            Color::Link => "label-link",
        },
    );
    html! {
        <span class={class}>{ for props.children.iter() }</span>
    }
}
