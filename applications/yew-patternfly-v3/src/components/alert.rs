use {
    crate::css::Color,
    yew::{classes, function_component, html, Children, Html, Properties},
};

#[derive(Properties, PartialEq)]
pub struct AlertProperties {
    pub children: Children,
    #[prop_or(Color::Info)]
    pub color: Color,
}

#[function_component]
pub fn Alert(props: &AlertProperties) -> Html {
    let class = classes!(
        "alert",
        match props.color {
            Color::Default | Color::Primary | Color::Link | Color::Info => "alert-info",
            Color::Warning => "alert-warning",
            Color::Danger => "alert-danger",
            Color::Success => "alert-success",
        },
    );
    html! {
        <div class={class}  role="alert">{ for props.children.iter() }</div>
    }
}
