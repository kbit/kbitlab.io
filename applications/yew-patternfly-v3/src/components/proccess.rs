use {
    crate::css::Color,
    yew::{classes, function_component, html, virtual_dom::AttrValue, Children, Html, Properties},
};

#[derive(Properties, PartialEq)]
pub struct ProccessProperties {
    pub children: Children,
    #[prop_or(Color::Default)]
    pub color: Color,
    #[prop_or(AttrValue::from("0"))]
    pub min: AttrValue,
    #[prop_or(AttrValue::from("100"))]
    pub max: AttrValue,
    #[prop_or(AttrValue::from("50"))]
    pub now: AttrValue,
}

#[function_component]
pub fn Proccess(props: &ProccessProperties) -> Html {
    let class = classes!(
        "progress-bar",
        match props.color {
            Color::Default | Color::Primary | Color::Link | Color::Info => "progress-bar-info",
            Color::Warning => "progress-bar-warning",
            Color::Danger => "progress-bar-danger",
            Color::Success => "progress-bar-success",
        },
    );
    html! {
        <div class="progress">
            <div class={class} role="progressbar" aria-valuenow={&props.now} aria-valuemin={&props.min} aria-valuemax={&props.max} style="width: 60%;">
                { for props.children.iter() }
            </div>
        </div>
    }
}
