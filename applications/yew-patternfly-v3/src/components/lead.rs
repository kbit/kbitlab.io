use {
    crate::css::Color,
    yew::{function_component, html, Children, Html, Properties},
};

#[derive(Properties, PartialEq)]
pub struct LeadProperties {
    pub children: Children,
    #[prop_or(Color::Default)]
    pub color: Color,
}

#[function_component]
pub fn Lead(props: &LeadProperties) -> Html {
    html! {
        <p class="lead">{ for props.children.iter() }</p>
    }
}
