use yew::{classes, function_component, html, Children, ChildrenWithProps, Html, Properties};

#[derive(Properties, PartialEq)]
pub struct TableDataProperties {
    pub children: Children,
}

#[derive(Properties, PartialEq)]
pub struct TableRowProperties {
    pub children: Children,
}

#[derive(Properties, PartialEq)]
pub struct TableHeaderProperties {
    pub children: ChildrenWithProps<TableData>,
}

#[derive(PartialEq, Eq)]
pub enum TableStyle {
    Bordered,
    Horizontal,
    Striped,
}

#[derive(Properties, PartialEq)]
pub struct TableProperties {
    pub children: Children,
    #[prop_or(TableStyle::Bordered)]
    pub style: TableStyle,
}

#[function_component]
pub fn TableHeader(props: &TableHeaderProperties) -> Html {
    html! {
        <thead><tr>{ for props.children.iter() }</tr></thead>
    }
}

#[function_component]
pub fn TableData(props: &TableDataProperties) -> Html {
    html! {
        <th>{ for props.children.iter() }</th>
    }
}

#[function_component]
pub fn TableRow(props: &TableRowProperties) -> Html {
    html! {
        <tr>{ for props.children.iter() }</tr>
    }
}

#[function_component]
pub fn Table(props: &TableProperties) -> Html {
    let class = classes!(
        "table",
        match props.style {
            TableStyle::Bordered => "table-bordered",
            TableStyle::Horizontal => "table-horizontal",
            TableStyle::Striped => "table-striped",
        },
    );
    html! {
        <table class={class}>{ for props.children.iter() }</table>
    }
}
