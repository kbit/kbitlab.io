use yew::{classes, function_component, html, Children, ChildrenWithProps, Html, Properties};

#[derive(Properties, PartialEq)]
pub struct ListHeadProperties {
    pub children: Children,
}

#[function_component]
pub fn ListHead(props: &ListHeadProperties) -> Html {
    html! {
        <h4 class="list-group-item-heading">{ for props.children.iter() }</h4>
    }
}

#[derive(Properties, PartialEq)]
pub struct ListTextProperties {
    pub children: Children,
}

#[function_component]
pub fn ListText(props: &ListTextProperties) -> Html {
    html! {
        <p class="list-group-item-text">{ for props.children.iter() }</p>
    }
}

#[derive(Properties, PartialEq)]
pub struct ListItemProperties {
    pub children: Children,
    #[prop_or(false)]
    pub active: bool,
}

#[function_component]
pub fn ListItem(props: &ListItemProperties) -> Html {
    let class = classes!(
        "list-group-item",
        match props.active {
            true => "active",
            false => "",
        },
    );
    html! {
        <li class={class}>{ for props.children.iter() }</li>
    }
}

#[derive(Properties, PartialEq)]
pub struct ListGroupProperties {
    pub children: ChildrenWithProps<ListItem>,
}

#[function_component]
pub fn ListGroup(props: &ListGroupProperties) -> Html {
    html! {
        <ul class="list-group">{ for props.children.iter() }</ul>
    }
}
