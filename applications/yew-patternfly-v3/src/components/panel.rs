use {
    crate::css::Color,
    yew::{classes, function_component, html, Children, Html, Properties},
};

#[derive(Properties, PartialEq)]
pub struct PanelHeaderProperties {
    pub children: Children,
}

#[function_component]
pub fn PanelHeader(props: &PanelHeaderProperties) -> Html {
    html! {
        <div class="panel-heading">{ for props.children.iter() }</div>
    }
}
#[derive(Properties, PartialEq)]
pub struct PanelFooterProperties {
    pub children: Children,
}

#[function_component]
pub fn PanelFooter(props: &PanelFooterProperties) -> Html {
    html! {
        <div class="panel-footer">{ for props.children.iter() }</div>
    }
}
#[derive(Properties, PartialEq)]
pub struct PanelBodyProperties {
    pub children: Children,
}

#[function_component]
pub fn PanelBody(props: &PanelBodyProperties) -> Html {
    html! {
        <div class="panel-body">{ for props.children.iter() }</div>
    }
}
#[derive(Properties, PartialEq)]
pub struct PanelProperties {
    pub children: Children,
    #[prop_or(Color::Default)]
    pub color: Color,
}

#[function_component]
pub fn Panel(props: &PanelProperties) -> Html {
    let class = classes!(
        "panel",
        match props.color {
            Color::Default | Color::Link => "panel-default",
            Color::Primary => "panel-primary",
            Color::Info => "panel-info",
            Color::Warning => "panel-warning",
            Color::Danger => "panel-danger",
            Color::Success => "panel-success",
        },
    );
    html! {
        <div class={class}>{ for props.children.iter() }</div>
    }
}
