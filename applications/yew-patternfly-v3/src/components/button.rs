use {
    crate::css::{Color, Size},
    yew::{classes, function_component, html, Children, Html, Properties},
};

#[derive(Properties, PartialEq)]
pub struct ButtonProperties {
    pub children: Children,
    #[prop_or(false)]
    pub disabled: bool,
    #[prop_or(Size::Default)]
    pub size: Size,
    #[prop_or(Color::Default)]
    pub color: Color,
}

#[function_component]
pub fn Button(props: &ButtonProperties) -> Html {
    let class = classes!(
        "btn",
        match props.disabled {
            true => "btn-disabled",
            false => "btn-active",
        },
        match props.size {
            Size::Default => "",
            Size::Large => "btn-lg",
            Size::Small => "btn-sm",
            Size::ExtraSmall => "btn-xs",
        },
        match props.color {
            Color::Default => "btn-default",
            Color::Primary => "btn-primary",
            Color::Warning => "btn-warning",
            Color::Danger => "btn-danger",
            Color::Info => "btn-info",
            Color::Success => "btn-success",
            Color::Link => "btn-link",
        },
    );
    html! {
        <button type="button" class={class}>{ props.children.iter().collect::<Html>() }</button>
    }
}

#[derive(Properties, PartialEq)]
pub struct ButtonsProperties {
    pub children: Children,
}

#[function_component]
pub fn Buttons(props: &ButtonsProperties) -> Html {
    html! {
        <div class="btn-group">{ props.children.iter().collect::<Html>() }</div>
    }
}
