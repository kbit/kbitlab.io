use {
    crate::css::Container,
    yew::{
        classes, function_component, html, virtual_dom::AttrValue, Children, ChildrenWithProps,
        Html, Properties,
    },
};

#[derive(Properties, PartialEq, Eq)]
pub struct LinkProperties {
    #[prop_or(AttrValue::from("#"))]
    pub href: AttrValue,
    #[prop_or(AttrValue::from(""))]
    pub label: AttrValue,
    #[prop_or(false)]
    pub navbar_brand: bool,
}

#[function_component]
pub fn Link(props: &LinkProperties) -> Html {
    let class = match props.navbar_brand {
        true => "navbar-brand",
        false => "",
    };
    html! {
        <a class={class} href={props.href.to_string()}>{ props.label.to_string() }</a>
    }
}

#[derive(Properties, PartialEq)]
pub struct NavItemProperties {
    pub children: ChildrenWithProps<Link>,
    #[prop_or(false)]
    pub active: bool,
    #[prop_or(false)]
    pub disabled: bool,
}

#[function_component]
pub fn NavItem(props: &NavItemProperties) -> Html {
    let class = classes!(
        match props.active {
            true => "active",
            false => "",
        },
        match props.disabled {
            true => "disabled",
            false => "",
        },
    );
    html! {
        <li role="presentation" class={class}>{ for props.children.iter() }</li>
    }
}

#[derive(PartialEq, Eq)]
pub enum NavType {
    Pills,
    Tabs,
}

#[derive(Properties, PartialEq)]
pub struct NavProperties {
    pub children: Children,
    #[prop_or(NavType::Pills)]
    pub nav_type: NavType,
    #[prop_or(false)]
    pub stacked: bool,
    #[prop_or(false)]
    pub justified: bool,
    #[prop_or(false)]
    pub navbar: bool,
}

#[function_component]
pub fn Nav(props: &NavProperties) -> Html {
    let class = classes!(
        "nav",
        match props.nav_type {
            NavType::Pills => "nav-pills",
            NavType::Tabs => "nav-tabs",
        },
        match props.navbar {
            true => "navbar-nav",
            false => "",
        },
        match props.stacked {
            true => "nav-stacked",
            false => "",
        },
        match props.justified {
            true => "nav-justified",
            false => "",
        },
    );
    html! {
        <ul class={class}>{ for props.children.iter() }</ul>
    }
}

#[derive(Properties, PartialEq)]
pub struct BreadcrumbItemProperties {
    pub children: ChildrenWithProps<Link>,
    #[prop_or(false)]
    pub active: bool,
}

#[function_component]
pub fn BreadcrumbItem(props: &BreadcrumbItemProperties) -> Html {
    let class = match props.active {
        true => "active",
        false => "",
    };
    html! {
        <li class={class}>{ for props.children.iter() }</li>
    }
}

#[derive(Properties, PartialEq)]
pub struct BreadcrumbProperties {
    pub children: ChildrenWithProps<BreadcrumbItem>,
}

#[function_component]
pub fn Breadcrumb(props: &BreadcrumbProperties) -> Html {
    html! { <ol class="breadcrumb">{ for props.children.iter() }</ol> }
}

#[derive(Properties, PartialEq, Eq)]
pub struct NavbarHeaderProperties {
    #[prop_or("#".to_string())]
    pub href: String,
    #[prop_or("".to_string())]
    pub label: String,
    #[prop_or(false)]
    pub navbar_brand: bool,
}

#[function_component]
pub fn NavbarHeader(props: &NavbarHeaderProperties) -> Html {
    html! {
        <div class="navbar-header">
            <Link navbar_brand=true href={props.href.to_string()} label={props.label.to_string()}/>
        </div>
    }
}

#[derive(Properties, PartialEq)]
pub struct NavbarProperties {
    pub children: Children,
    #[prop_or(false)]
    pub inverse: bool,
    #[prop_or(false)]
    pub fluid: bool,
}

#[function_component]
pub fn Navbar(props: &NavbarProperties) -> Html {
    let class = classes!(
        "navbar",
        "navbar-default",
        match props.inverse {
            true => "navbar-inverse",
            false => "",
        },
    );
    html! {
        <nav class={class}>
            <Container fluid={props.fluid}>{ for props.children.iter() }</Container>
        </nav>
    }
}
