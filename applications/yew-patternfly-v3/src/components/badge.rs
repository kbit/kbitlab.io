use yew::{function_component, html, Children, Html, Properties};

#[derive(Properties, PartialEq)]
pub struct BadgeProperties {
    pub children: Children,
}

#[function_component]
pub fn Badge(props: &BadgeProperties) -> Html {
    html! {
        <span class="badge">{ for props.children.iter() }</span>
    }
}
