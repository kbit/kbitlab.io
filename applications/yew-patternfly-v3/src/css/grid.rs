use yew::{function_component, html, Children, ChildrenWithProps, Html, Properties};

#[derive(PartialEq, Eq)]
pub enum ColSize {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Eleven,
    Tvelve,
}

#[derive(PartialEq, Eq)]
pub enum ColDevices {
    Phones,
    Tablets,
    Desktops,
    Widescreen,
}

#[derive(Properties, PartialEq)]
pub struct ColProperties {
    pub children: Children,
    #[prop_or(ColSize::Tvelve)]
    pub size: ColSize,
    #[prop_or(ColDevices::Desktops)]
    pub devices: ColDevices,
}

#[function_component]
pub fn Col(props: &ColProperties) -> Html {
    let class = "col".to_owned()
        + match props.devices {
            ColDevices::Phones => "-xs-",
            ColDevices::Tablets => "-sm-",
            ColDevices::Desktops => "-md-",
            ColDevices::Widescreen => "-lg-",
        }
        + match props.size {
            ColSize::One => "1",
            ColSize::Two => "2",
            ColSize::Three => "3",
            ColSize::Four => "4",
            ColSize::Five => "5",
            ColSize::Six => "6",
            ColSize::Seven => "7",
            ColSize::Eight => "8",
            ColSize::Nine => "9",
            ColSize::Ten => "10",
            ColSize::Eleven => "11",
            ColSize::Tvelve => "12",
        };
    html! {
        <div class={class}>{ for props.children.iter() }</div>
    }
}

#[derive(Properties, PartialEq)]
pub struct RowProperties {
    pub children: ChildrenWithProps<Col>,
}

#[function_component]
pub fn Row(props: &RowProperties) -> Html {
    html! {
        <div class="row">{ for props.children.iter() }</div>
    }
}
