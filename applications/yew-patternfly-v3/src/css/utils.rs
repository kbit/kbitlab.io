use yew::{function_component, html, Html};

#[derive(PartialEq, Eq)]
pub enum Color {
    Default,
    Primary,
    Success,
    Info,
    Warning,
    Danger,
    Link,
}

#[derive(Clone, PartialEq, Eq)]
pub enum Size {
    Default,
    Large,
    Small,
    ExtraSmall,
}

#[function_component]
pub fn IncludeBootstrap() -> Html {
    html! {
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css"
            integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu"
            crossorigin="anonymous"
        />
    }
}

#[function_component]
pub fn IncludeCDN() -> Html {
    html! {
        <>
            <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.59.5/css/patternfly-additions.min.css"
                integrity="sha512-EYt56ZZC96V80F1+H7qwhi3NUIzR7onpd+RsVWHUxQfZSiIWmsQFD+6J/bS6OsAGKgvRftxtkxB16efK4mDFaA=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer"
            />
            <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/patternfly/3.59.5/css/patternfly.min.css"
                integrity="sha512-6UpU9AcZHviFiYbPBQSNtdLnWsTo8Y/1bnfc/OM2Hh4jEeQZEa9TfmOVGzLHE90YV44V/u5I0cnQr4uwMW2P6w=="
                crossorigin="anonymous"
                referrerpolicy="no-referrer"
            />
        </>
    }
}
