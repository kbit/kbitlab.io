use yew::{function_component, html, virtual_dom::AttrValue, Html, Properties};

#[derive(PartialEq, Eq)]
pub enum ImageStyle {
    Rounded,
    Circle,
    Thumbnail,
}

#[derive(Properties, PartialEq, Eq)]
pub struct ImageProperties {
    #[prop_or(ImageStyle::Thumbnail)]
    pub style: ImageStyle,
    #[prop_or(AttrValue::from(""))]
    pub src: AttrValue,
    #[prop_or(AttrValue::from(""))]
    pub alt: AttrValue,
}

#[function_component]
pub fn Image(props: &ImageProperties) -> Html {
    let class = match props.style {
        ImageStyle::Thumbnail => "img-thumbnail",
        ImageStyle::Rounded => "img-rounded",
        ImageStyle::Circle => "img-circle",
    };
    html! {
        <img src={&props.src} alt={&props.alt} class={class}/>
    }
}
