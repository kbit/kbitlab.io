use yew::{function_component, html, Children, Html, Properties};

#[derive(Properties, PartialEq)]
pub struct ContainerProperties {
    pub children: Children,
    #[prop_or(false)]
    pub fluid: bool,
}

#[function_component]
pub fn Container(props: &ContainerProperties) -> Html {
    let class = match props.fluid {
        true => "container-fluid",
        false => "container",
    };
    html! {
        <div class={class}>{ for props.children.iter() }</div>
    }
}

#[derive(Properties, PartialEq)]
pub struct JumbotronProperties {
    pub children: Children,
}

#[function_component]
pub fn Jumbotron(props: &JumbotronProperties) -> Html {
    html! {
        <div class="jumbotron">{ for props.children.iter() }</div>
    }
}

#[derive(Properties, PartialEq)]
pub struct WellProperties {
    pub children: Children,
}

#[function_component]
pub fn Well(props: &WellProperties) -> Html {
    html! {
        <div class="well">{ for props.children.iter() }</div>
    }
}
