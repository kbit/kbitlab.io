pub const WIDTH: i32 = 90;
pub const WIDGET_SPACE: i32 = 10;
pub const WIDGET_HEIGHT: i32 = 25;
pub const INFO: &str = "<h2>Hello world</h2>";
pub const CFG: &str = "/.config/flhudson";
pub const THEMES: [&str; 2] = ["Light", "Dark"];
#[derive(Clone, Copy)]
pub enum Message {
    Open,
    Save,
    Theme(u8),
    Wizard(i32),
    Trans,
    Info,
    Quit(bool),
}
