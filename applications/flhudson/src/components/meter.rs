use {
    crate::{
        constants::{WIDGET_SPACE, WIDTH},
        elements,
    },
    fltk::{
        frame::Frame,
        group::Flex,
        input::IntInput,
        menu::Choice,
        prelude::{GroupExt, WidgetBase, WidgetExt},
        text::TextEditor,
    },
};

pub struct Meter {
    pub proto: Choice,
    pub port: Choice,
    pub concurrent: IntInput,
    pub targets: TextEditor,
    pub endpoints: TextEditor,
}

impl Meter {
    pub fn build() -> Self {
        let mut layout = Flex::default_fill().with_label("Meter");
        layout.fixed(&Frame::default(), WIDTH);
        let mut col = Flex::default().column();
        let component = Self {
            proto: elements::choice("Proto", "https|http", 0, &mut col),
            port: elements::choice("Port", "443|80", 0, &mut col),
            concurrent: elements::int_input("concurrent", "8", &mut col),
            targets: elements::text("Targets", "127.0.0.1"),
            endpoints: elements::text("Endpoints", "/"),
        };
        col.end();
        col.set_pad(WIDGET_SPACE);
        layout.end();
        layout.set_margin(WIDGET_SPACE);
        component
    }
}
