use {
    crate::{
        commands,
        constants::{INFO, WIDGET_SPACE},
        elements,
    },
    fltk::{
        app,
        button::Button,
        dialog::{FileChooser, FileChooserType, HelpDialog},
        group::{Flex, Tabs},
        prelude::{GroupExt, WidgetBase, WidgetExt},
    },
    fltk_table::{SmartTable, TableOpts},
    std::collections::HashMap,
};

pub struct Calc {
    pub open: Button,
    pub save: Button,
    pub data: HashMap<String, Vec<Vec<String>>>,
    pub tabs: Tabs,
}

impl Calc {
    pub fn build() -> Self {
        let mut layout = Flex::default_fill().with_label("Calc").column();
        let component = Self {
            open: elements::button("@#+2fileopen", "Open", &mut layout),
            save: elements::button("@#+2filesaveas", "SaveAs", &mut layout),
            data: HashMap::new(),
            tabs: Tabs::default_fill(),
        };
        component.tabs.end();
        layout.end();
        layout.set_pad(WIDGET_SPACE);
        component
    }
    pub fn open(&mut self) {
        let mut dialog = FileChooser::new(
            &std::env::var("HOME").unwrap(),
            "*.{csv,json}",
            FileChooserType::Multi,
            "Select file:",
        );
        dialog.show();
        while dialog.shown() {
            app::wait();
        }
        if dialog.count() > 1 {
            self.data = commands::open(
                (1..=dialog.count())
                    .map(|i| dialog.value(i).unwrap())
                    .collect::<Vec<String>>(),
            );
            for label in self.data.keys() {
                let flex = Flex::default_fill().with_label(label).column();
                let mut table = SmartTable::default().with_opts(TableOpts {
                    rows: self.data[label].len() as i32,
                    cols: self.data[label][0].len() as i32,
                    editable: true,
                    ..Default::default()
                });
                for (row_ord, row) in self.data[label].iter().enumerate() {
                    for (col_ord, cell) in row.iter().enumerate() {
                        table.set_cell_value(row_ord as i32, col_ord as i32, cell);
                    }
                }
                flex.end();
                self.tabs.add(&flex);
            }
            self.tabs.auto_layout();
            app::redraw();
        }
    }
    pub fn save(&mut self) {
        let mut dialog = FileChooser::new(
            &std::env::var("HOME").unwrap(),
            "*.xlsx",
            FileChooserType::Create,
            "Select file:",
        );
        dialog.show();
        while dialog.shown() {
            app::wait();
        }
        if dialog.count() > 0 {
            commands::save(&dialog.value(1).unwrap(), &self.data);
            self.tabs.clear();
        }
    }
    pub fn info(&self) {
        let mut dialog = HelpDialog::default();
        dialog.set_value(INFO);
        dialog.show();
        while dialog.shown() {
            app::wait();
        }
    }
}
