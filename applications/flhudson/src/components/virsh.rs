use {
    crate::{
        constants::{WIDGET_SPACE, WIDTH},
        elements,
    },
    fltk::{
        frame::Frame,
        group::Flex,
        prelude::{GroupExt, WidgetBase, WidgetExt},
        text::TextEditor,
    },
};

pub struct Virsh {
    pub nodes: TextEditor,
}

impl Virsh {
    pub fn build() -> Self {
        let mut layout = Flex::default_fill().with_label("Virsh");
        layout.fixed(&Frame::default(), WIDTH);
        let mut col = Flex::default().column();
        let component = Self {
            nodes: elements::text("Targets", "127.0.0.1"),
        };
        col.end();
        col.set_margin(WIDGET_SPACE);
        layout.end();
        component
    }
}
