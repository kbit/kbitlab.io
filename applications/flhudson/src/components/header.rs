use {
    crate::{constants::WIDGET_HEIGHT, elements},
    fltk::{
        app,
        button::Button,
        frame::Frame,
        group::{Flex, Wizard},
        menu::MenuButton,
        prelude::{GroupExt, WidgetBase, WidgetExt},
    },
};

pub struct Header {
    pub run: Button,
    pub frame: Frame,
    pub menu: MenuButton,
}

impl Header {
    pub fn build(flex: &mut Flex) -> Self {
        let mut layout = Flex::default_fill();
        let component = Self {
            menu: MenuButton::default(),
            frame: Frame::default().with_label("Chooce a job..."),
            run: elements::button("@#+2->", "Run", &mut layout),
        };
        layout.end();
        layout.set_pad(0);
        layout.fixed(&component.menu, WIDGET_HEIGHT);
        flex.fixed(&layout, WIDGET_HEIGHT);
        component
    }
    pub fn switch(&mut self, wizard: &mut Wizard, number: i32) {
        wizard.set_current_widget(&wizard.child(number).unwrap());
        self.frame
            .set_label(wizard.try_current_widget().unwrap().label().as_str());
        self.run.activate();
        app::redraw();
    }
}
