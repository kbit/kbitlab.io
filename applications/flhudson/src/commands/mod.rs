use {
    csv::ReaderBuilder,
    gnuplot::{AxesCommon, Caption, Color, Figure},
    serde::Deserialize,
    std::{
        collections::HashMap,
        env,
        ffi::OsStr,
        fs,
        path::Path,
        process::Command,
        thread::{spawn, JoinHandle},
    },
    xlsxwriter::Workbook,
};

pub fn j2y() {
    let mut winrm: HashMap<&str, &str> = HashMap::new();
    let mut ssh: HashMap<&str, &str> = HashMap::new();
    let mut other: HashMap<&str, &str> = HashMap::new();
    let value: serde_json::Value = serde_json::from_str(
        fs::read_to_string(Path::new("masscan.json"))
            .unwrap()
            .as_str(),
    )
    .unwrap();
    for host in value.as_array().unwrap() {
        match host.as_object().unwrap()["ports"].as_array().unwrap()[0]
            .as_object()
            .unwrap()["port"]
            .as_u64()
            .unwrap()
        {
            5985 => winrm.entry(host["ip"].as_str().unwrap()).or_insert(""),
            22 => ssh.entry(host["ip"].as_str().unwrap()).or_insert(""),
            _ => other.entry(host["ip"].as_str().unwrap()).or_insert(""),
        };
    }
    let inventory = HashMap::from([(
        "all",
        HashMap::from([(
            "children",
            HashMap::from([
                ("winrm", HashMap::from([("hosts", winrm)])),
                ("ssh", HashMap::from([("hosts", ssh)])),
                ("other", HashMap::from([("hosts", other)])),
            ]),
        )]),
    )]);
    eprintln!(
        "{:?}",
        fs::write("inventory.yml", serde_yaml::to_string(&inventory).unwrap())
    );
}

#[derive(Deserialize)]
pub struct Hadoop {
    pub hdfs: String,
    pub properties: HashMap<String, HashMap<String, String>>,
}

pub fn from_json(dir: &str) {
    let mut clusters: Vec<Hadoop> = Vec::new();
    let mut headers: Vec<String> = vec!["properties".to_string()];
    let mut services: HashMap<String, usize> = HashMap::new();
    let workbook = Workbook::new(&format!("{dir}/report.xlsx"));
    for file in fs::read_dir(dir).unwrap() {
        let path = file.unwrap().path();
        if path.extension() == Some(OsStr::new("json")) {
            let cluster: Hadoop =
                serde_json::from_str(fs::read_to_string(path).unwrap().as_str()).unwrap();
            headers.push(cluster.hdfs.clone());
            for (key, _) in cluster.properties.iter() {
                services.insert(
                    key.to_string(),
                    if services.contains_key(key) {
                        services[key] + 1
                    } else {
                        0
                    },
                );
            }
            clusters.push(cluster)
        }
    }
    headers.push("exact".to_string());
    let f_header = workbook.add_format().set_bold();
    let f_exact = workbook.add_format().set_italic();
    for (service, count) in services.iter() {
        let mut sheet = workbook.add_worksheet(Some(service)).unwrap();
        for (index, header) in headers.iter().enumerate() {
            sheet
                .write_string(0, index as u16, &header.to_uppercase(), Some(&f_header))
                .unwrap();
        }
        if *count == clusters.len() - 1 {
            let mut properties: HashMap<String, bool> = HashMap::new();
            for cluster in clusters.iter() {
                for (property, _) in cluster.properties[service].iter() {
                    properties.insert(property.to_string(), true);
                }
            }
            for (row_num, property) in properties.keys().enumerate() {
                sheet
                    .write_string((row_num + 1) as u32, 0, property, Some(&f_header))
                    .unwrap();
                for (col_num, cluster) in clusters.iter().enumerate() {
                    sheet
                        .write_string(
                            (row_num + 1) as u32,
                            (col_num + 1) as u16,
                            match cluster.properties[service].get(property) {
                                Some(cell) => cell,
                                None => "None",
                            },
                            None,
                        )
                        .unwrap();
                }
                sheet
                    .write_formula(
                        (row_num + 1) as u32,
                        (clusters.len() + 1) as u16,
                        format!(
                            "=AND(EXACT(B{},C{}:C{}))",
                            row_num + 2,
                            row_num + 2,
                            row_num + 2
                        )
                        .as_str(),
                        Some(&f_exact),
                    )
                    .unwrap();
            }
        };
    }
    workbook.close().unwrap();
}

pub fn from_csv(dir: &str) {
    if let Ok(entries) = fs::read_dir(dir) {
        let source: Vec<String> = entries
            .map(|entry| entry.ok().unwrap().path())
            .filter(|path| path.extension() == Some(OsStr::new("csv")))
            .map(|path| format!("{:?}", path))
            .collect::<Vec<String>>();
        save(&format!("{dir}/report.xlsx"), &open(source))
    }
}

pub fn open(source: Vec<String>) -> HashMap<String, Vec<Vec<String>>> {
    let mut data: HashMap<String, Vec<Vec<String>>> = HashMap::new();
    for file in source {
        let label = Path::new(&file).file_stem().unwrap().to_str().unwrap();
        data.insert(
            label.to_string(),
            ReaderBuilder::new()
                .has_headers(false)
                .from_reader(std::fs::read_to_string(&file).unwrap().as_bytes())
                .records()
                .map(|row| {
                    row.unwrap()
                        .iter()
                        .map(str::to_string)
                        .collect::<Vec<String>>()
                })
                .collect(),
        );
    }
    data
}

pub fn save(target: &str, source: &HashMap<String, Vec<Vec<String>>>) {
    let workbook = Workbook::new(target);
    for list in source.keys() {
        let mut sheet = workbook.add_worksheet(Some(list)).unwrap();
        for (row_ord, row) in source[list].iter().enumerate() {
            for (col_ord, cell) in row.iter().enumerate() {
                sheet
                    .write_string(row_ord as u32, col_ord as u16, cell, None)
                    .unwrap();
            }
        }
    }
    workbook.close().unwrap();
}

pub fn f_comb_array(length: usize, size: usize) -> Vec<Vec<usize>> {
    let arr: Vec<usize> = (0..length).collect();
    let max: Vec<usize> = (length - size..length).rev().collect();
    let mut min: Vec<usize> = (0..size).rev().collect();
    let mut tmp: Vec<usize> = min.clone();
    let mut index: usize = 0;
    // let mut count: usize = 0;
    let mut tar: Vec<Vec<usize>> = Vec::new();
    while index < size {
        index = 0;
        for seq in 0..size {
            if tmp[seq] == max[seq] {
                index += 1;
                if index == size {
                    return tar; //count + 1;
                }
            } else {
                break;
            }
        }
        tar.push(
            (0..size)
                .rev()
                .map(|seq| arr[tmp[seq]])
                .collect::<Vec<usize>>(),
        );
        tmp[index] += 1;
        if 0 < index && index < size {
            for seq in (0..index).rev() {
                min[seq] = tmp[seq + 1] + 1;
                tmp[seq] = min[seq];
            }
        }
        // count += 1;
    }
    tar
}

#[derive(Deserialize)]
struct Result {
    name: String,
    memory: String,
    vcpu: String,
}

#[derive(Deserialize)]
pub struct Env {
    pub proto: String,
    pub port: u16,
    pub concurrent: u8,
    pub targets: Vec<String>,
    pub endpoints: Vec<String>,
}

pub fn meter(env: Env, output: &str) {
    let mut children: HashMap<String, JoinHandle<Vec<Vec<String>>>> = HashMap::new();
    for host in env.targets {
        let endpoints: Vec<String> = env.endpoints.clone();
        let proto: String = env.proto.clone();
        let port: u16 = env.port;
        let concurrent: u8 = env.concurrent;
        children.insert(
            host.clone(),
            spawn(move || -> Vec<Vec<String>> {
                let mut report: Vec<Vec<String>> = Vec::new();
                for endpoint in endpoints {
                    report.push(get_raw(
                        format!("{}://{}:{}{}", proto, host, port, endpoint),
                        concurrent,
                    ));
                }
                report
            }),
        );
    }
    let workbook = Workbook::new(output);
    let f_header = workbook.add_format().set_bold();
    for (host, thread) in children {
        let mut sheet = workbook.add_worksheet(Some(host.as_str())).unwrap();
        for (index, header) in [
            "endpoint",
            "transactions",
            "availability",
            "elapsed_time",
            "data_transferred",
            "response_time",
            "transaction_rate",
            "throughput",
            "concurrency",
            "successful_transactions",
            "failed_transactions",
            "longest_transaction",
            "shortest_transaction",
        ]
        .iter()
        .enumerate()
        {
            sheet
                .write_string(0, index as u16, &header.to_uppercase(), Some(&f_header))
                .unwrap();
        }
        for (row_num, record) in thread.join().unwrap().iter().enumerate() {
            for (coll_num, item) in record.iter().enumerate() {
                sheet
                    .write_string((row_num + 1) as u32, coll_num as u16, item, None)
                    .unwrap();
            }
        }
    }
    workbook.close().unwrap();
}

fn get_raw(connect: String, concurrent: u8) -> Vec<String> {
    let run = Command::new("bash")
        .arg("-xc")
        .arg(format!(
            "siege --json-output --concurrent {} --time 1m  '{}'",
            concurrent, &connect
        ))
        .output()
        .expect("\x1b[31mFailed to execute siege!\x1b[0m");
    let result: HashMap<String, f32> = if run.status.success() {
        eprintln!("\x1b[32m{}\x1b[0m", String::from_utf8_lossy(&run.stderr));
        serde_json::from_str(&String::from_utf8_lossy(&run.stdout)).unwrap()
    } else {
        panic!("\x1b[31m{}\x1b[0m", String::from_utf8_lossy(&run.stderr))
    };
    vec![
        connect,
        result["transactions"].to_string(),
        result["availability"].to_string(),
        result["elapsed_time"].to_string(),
        result["data_transferred"].to_string(),
        result["response_time"].to_string(),
        result["transaction_rate"].to_string(),
        result["throughput"].to_string(),
        result["concurrency"].to_string(),
        result["successful_transactions"].to_string(),
        result["failed_transactions"].to_string(),
        result["longest_transaction"].to_string(),
        result["shortest_transaction"].to_string(),
    ]
}

pub fn virsh(targets: Vec<usize>, output: &str) {
    let mut children: HashMap<usize, JoinHandle<Vec<Vec<String>>>> = HashMap::new();
    for node in targets {
        children.insert(
            node,
            spawn(move || -> Vec<Vec<String>> { get_list_raw(node) }),
        );
    }
    let workbook = Workbook::new(output);
    let f_header = workbook.add_format().set_bold();
    for (node, thread) in children {
        let mut sheet = workbook.add_worksheet(Some(&(node.to_string()))).unwrap();
        ["name", "memory", "vcpu"]
            .iter()
            .enumerate()
            .for_each(|(index, header)| {
                sheet
                    .write_string(0, index as u16, &header.to_uppercase(), Some(&f_header))
                    .unwrap();
            });
        for (row_num, record) in thread.join().unwrap().iter().enumerate() {
            for (coll_num, item) in record.iter().enumerate() {
                sheet
                    .write_string((row_num + 1) as u32, coll_num as u16, item, None)
                    .unwrap();
            }
        }
    }
    workbook.close().unwrap();
}

fn get_list_raw(node: usize) -> Vec<Vec<String>> {
    let run = Command::new("bash")
        .arg("-xc")
        .arg(format!("sshpass -e virsh --connect qemu+ssh://{}@192.168.25.{node}/system list --name --state-running", env::var("SSHUSER").unwrap()))
        .output()
        .expect("failed to execute bash");
    let mut list_raw: Vec<Vec<String>> = Vec::new();
    if run.status.success() {
        eprintln!("\x1b[32m{}\x1b[0m", String::from_utf8_lossy(&run.stderr));
        for pool in String::from_utf8_lossy(&run.stdout)
            .split_whitespace()
            .collect::<Vec<&str>>()
            .chunks(5)
        {
            let mut children: Vec<JoinHandle<Vec<String>>> = Vec::new();
            for line in pool {
                let vm: String = line.to_string();
                children.push(spawn(move || -> Vec<String> { get_raws(node, vm) }));
            }
            for thread in children {
                list_raw.push(thread.join().unwrap())
            }
        }
    } else {
        panic!("\x1b[31m{}\x1b[0m", String::from_utf8_lossy(&run.stderr))
    };
    plot(
        &(format!("{node}")),
        list_raw
            .iter()
            .map(|item| item[0].parse::<i32>().unwrap())
            .collect::<Vec<i32>>(),
        list_raw
            .iter()
            .map(|item| item[1].parse::<i32>().unwrap())
            .collect::<Vec<i32>>(),
    );
    list_raw
}

fn get_raws(node: usize, vm: String) -> Vec<String> {
    let run = Command::new("bash")
        .arg("-xc")
        .arg(format!(
            "sshpass -e virsh --connect qemu+ssh://admin@192.168.25.{}/system dumpxml {}",
            node, vm
        ))
        .output()
        .expect("failed to execute bash");
    let result: Result = if run.status.success() {
        eprintln!("\x1b[32m{}\x1b[0m", String::from_utf8_lossy(&run.stderr));
        serde_xml_rs::from_str(String::from_utf8_lossy(&run.stdout).to_string().as_str()).unwrap()
    } else {
        panic!("\x1b[31m{}\x1b[0m", String::from_utf8_lossy(&run.stderr))
    };
    vec![result.name, result.memory, result.vcpu]
}

fn plot(title: &str, x: Vec<i32>, y: Vec<i32>) {
    let mut fg = Figure::new();
    fg.set_enhanced_text(true);
    fg.set_title(title);
    let axes2d = fg.axes2d();
    axes2d.set_x_grid(true);
    axes2d.set_y_grid(true);
    axes2d.boxes(&x, &y, &[Caption("A line"), Color("red")]);
    fg.save_to_png(&format!("{title}.png"), 960, 540).unwrap();
}

pub fn nsis(url: String) {
    let name: &str = url.split('/').collect::<Vec<&str>>().last().unwrap();
    if cfg!(target_os = "windows") {
        let script: &str = &(format!("Start-BitsTransfer -Source '{name}'\nStart-Process -FilePath 'msiexec' -ArgumentList '/passive /package {url}' -Wait\nRemove-Item '{name}'"));
        let run = std::process::Command::new("powershell")
            .arg("-command")
            .arg(script)
            .output()
            .expect("failed to execute bash");
        match run.status.success() {
            true => eprintln!("\x1b[32m{script}\x1b[0m"),
            false => panic!("\x1b[31m{}\x1b[0m", String::from_utf8_lossy(&run.stderr)),
        }
    } else {
        eprintln!("\x1b[32m{name}\x1b[0m")
    }
}

pub fn once() -> bool {
    if cfg!(target_os = "linux") {
        let run = Command::new("bash")
            .arg("-xc")
            .arg(format!("lsof -t {}", env::current_exe().unwrap().display()))
            .output()
            .expect("failed to execute bash");
        match run.status.success() {
            true => {
                String::from_utf8_lossy(&run.stdout)
                    .split_whitespace()
                    .count()
                    == 1
            }
            false => panic!("\x1b[31m{}\x1b[0m", String::from_utf8_lossy(&run.stderr)),
        }
    } else {
        true
    }
}
