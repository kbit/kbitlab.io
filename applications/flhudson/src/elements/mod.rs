use {
    crate::constants::WIDGET_HEIGHT,
    fltk::{
        button::Button,
        enums::Align,
        group::Flex,
        input::IntInput,
        menu::Choice,
        prelude::{DisplayExt, InputExt, MenuExt, WidgetExt},
        text::{TextBuffer, TextEditor, WrapMode},
    },
};

pub fn button(label: &str, tooltip: &str, flex: &mut Flex) -> Button {
    let mut element = Button::default().with_label(label);
    element.set_tooltip(tooltip);
    element.deactivate();
    flex.fixed(&element, WIDGET_HEIGHT);
    element
}

pub fn choice(tooltip: &str, choice: &str, value: i32, flex: &mut Flex) -> Choice {
    let mut element = Choice::default().with_label(tooltip);
    element.set_tooltip(tooltip);
    element.add_choice(choice);
    element.set_value(value);
    flex.fixed(&element, WIDGET_HEIGHT);
    element
}

pub fn int_input(tooltip: &str, value: &str, flex: &mut Flex) -> IntInput {
    let mut element = IntInput::default().with_label(tooltip);
    element.set_tooltip(tooltip);
    element.set_value(value);
    flex.fixed(&element, WIDGET_HEIGHT);
    element
}

pub fn text(label: &str, text: &str) -> TextEditor {
    let mut element = TextEditor::default()
        .with_label(label)
        .with_align(Align::Left);
    element.set_linenumber_width(WIDGET_HEIGHT);
    element.set_buffer(TextBuffer::default());
    element.buffer().unwrap().set_text(text);
    element.wrap_mode(WrapMode::AtBounds, 0);
    element
}
