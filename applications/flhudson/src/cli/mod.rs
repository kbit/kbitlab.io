use {
    clap::{App, Arg, ArgMatches},
    std::{env, fs, path::Path},
};

pub fn app(args: Vec<String>) {
    let matches: ArgMatches = App::new(&args[0][..])
        .author("Artem V. Ageev <artem.v.ageev@gmail.com>")
        .about("TODO")
        .arg(
            Arg::with_name("config")
                .long("config")
                .takes_value(true)
                .help("CONFIGURATION, show the current config."),
        )
        .arg(
            Arg::with_name("output")
                .long("output")
                .takes_value(true)
                .help("XLSX OUTPUT, print final stats to stdout as XLSX."),
        )
        .get_matches();
    let cfg: Vec<usize> = serde_json::from_str(
        fs::read_to_string(Path::new(
            matches
                .value_of("config")
                .unwrap_or(&(env::var("HOME").unwrap() + "/config.json")),
        ))
        .expect("Not fount config")
        .as_str(),
    )
    .unwrap();
    flvirsh::run(
        cfg,
        matches
            .value_of("output")
            .unwrap_or(&(env::var("HOME").unwrap() + "/report.xlsx")),
    );
}
