#![forbid(unsafe_code)]

mod commands;
mod components;
mod constants;
mod elements;
mod gui;

fn main() {
    if commands::once() {
        gui::app()
    }
}
