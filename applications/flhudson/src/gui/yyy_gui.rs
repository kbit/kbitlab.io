use gtk::{
    glib,
    prelude::{
        ApplicationExt, ApplicationExtManual, ButtonExt, CellRendererToggleExt, GridExt,
        GtkListStoreExt, GtkListStoreExtManual, LabelExt, ToValue, TreeModelExt, TreeViewExt,
        WidgetExt,
    },
    Application, ApplicationWindow, Button, CellRendererText, CellRendererToggle, Grid, Label,
    ListStore, PolicyType, ScrolledWindow, TreeView, TreeViewColumn,
    WindowPosition::Center,
};

pub fn app() {
    let application = Application::builder()
        .application_id("io.gitlab.kbit.rpkgs")
        .build();
    application.connect_activate(window);
    application.run();
}

fn window(application: &Application) {
    let list_store = ListStore::new(&[
        glib::types::Type::BOOL,
        glib::types::Type::STRING,
        glib::types::Type::STRING,
        glib::Type::U32,
    ]);
    for url in [
        "https://www.farmanager.com/files/Far30b6060.x64.20221208.msi",
        "https://static.rust-lang.org/dist/rust-1.67.1-x86_64-pc-windows-gnu.msi",
        "https://www.libreoffice.org/donate/dl/win-x86_64/7.6.0/en-US/LibreOffice_7.6.0_Win_x86-64.msi",
    ] {
        let name: &str = url.split('/').nth_back(0).unwrap();
        list_store.set(&list_store.append(), &[(0, &true), (1, &name), (2, &url)]);
    }
    let tree_view = TreeView::with_model(&list_store);
    for (ord, name) in ["STATUS", "NAME", "URL"].into_iter().enumerate() {
        match name {
            "STATUS" => {
                let renderer = CellRendererToggle::new();
                renderer.connect_toggled(glib::clone!( @strong list_store => move |_, path| {
                    let iter = list_store.iter(&path).unwrap();
                    list_store.set_value(
                        &iter,
                        ord as u32,
                        &(!list_store.value(&iter, ord as i32).get::<bool>().unwrap()).to_value(),
                    );
                }));
                tree_view.append_column(&TreeViewColumn::with_attributes(
                    name,
                    &renderer,
                    &[("active", ord as i32)],
                ))
            }
            _ => tree_view.append_column(&TreeViewColumn::with_attributes(
                name,
                &CellRendererText::new(),
                &[("text", ord as i32)],
            )),
        };
    }
    let run = Button::with_mnemonic("Run");
    let result = Label::with_mnemonic("Result");
    let grid = Grid::builder()
        .margin(5)
        .column_homogeneous(true)
        .column_spacing(5)
        .row_homogeneous(true)
        .row_spacing(5)
        .build();
    grid.attach(
        &ScrolledWindow::builder()
            .hscrollbar_policy(PolicyType::Automatic)
            .vscrollbar_policy(PolicyType::Automatic)
            .child(&tree_view)
            .build(),
        0,
        0,
        16,
        8,
    );
    grid.attach(&run, 0, 8, 1, 1);
    grid.attach(&result, 14, 8, 2, 1);
    run.connect_clicked(glib::clone!(@strong run => move |_| {
        list_store.foreach(|store, _, iter| {
            if store.value(iter, 0).get::<bool>().unwrap() {
                gtk_nsis::run(list_store.value(iter, 2).get::<String>().unwrap());
            };
            false
        });
        let value = result.text();
        result.set_text(&(value.to_string() + " Done!"));
        run.set_sensitive(false);
    }));
    ApplicationWindow::builder()
        .application(application)
        .window_position(Center)
        .default_width(960)
        .default_height(540)
        .child(&grid)
        .build()
        .show_all();
}
