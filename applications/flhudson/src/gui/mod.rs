use {
    crate::{
        commands,
        components::{Calc, Header, Meter, Virsh},
        constants::{Message, CFG, INFO, THEMES, WIDGET_SPACE},
    },
    fltk::{
        app,
        enums::{Event, Font, Shortcut},
        group::{Flex, Wizard},
        menu::MenuFlag,
        misc::HelpView,
        prelude::{DisplayExt, GroupExt, InputExt, MenuExt, WidgetBase, WidgetExt, WindowExt},
        window::Window,
    },
    fltk_theme::{color_themes, ColorTheme},
    std::{env, fs, path::Path},
};

pub fn app() {
    let mut window = Window::default()
        .with_size(960, 540)
        .with_label("FlHudson")
        .center_screen();
    let mut flex = Flex::default_fill().column();
    let mut header = Header::build(&mut flex);
    let mut wizard = Wizard::default_fill();
    let row = Flex::default_fill();
    HelpView::default_fill().set_value(INFO);
    row.end();
    let virsh = Virsh::build();
    let meter = Meter::build();
    let mut calc = Calc::build();
    wizard.end();
    flex.set_margin(WIDGET_SPACE);
    flex.set_pad(WIDGET_SPACE);
    flex.end();
    window.end();
    window.make_resizable(true);
    window.show();
    window.set_border(true);
    run(
        &mut window,
        &mut header,
        &mut wizard,
        &mut calc,
        &meter,
        &virsh,
    );
}

fn run(
    window: &mut Window,
    header: &mut Header,
    wizard: &mut Wizard,
    calc: &mut Calc,
    meter: &Meter,
    virsh: &Virsh,
) {
    let (sender, receiver) = app::channel::<Message>();
    let file = env::var("HOME").unwrap() + CFG;
    let mut theme: u8 = match Path::new(&file).exists() {
        true => fs::read(&file).unwrap()[0],
        false => 0,
    };
    for (ord, item) in THEMES.iter().enumerate() {
        let idx = header.menu.add_emit(
            &format!("&Theme/&{}\t", item),
            Shortcut::Ctrl | item.to_lowercase().chars().next().unwrap(),
            MenuFlag::Radio,
            sender,
            Message::Theme(ord as u8),
        );
        if ord == theme as usize {
            header.menu.at(idx).unwrap().set();
        };
    }
    for (label, chr, msg) in [
        ("@#->  &Jobs/@#circle  &Virsh", 'v', Message::Wizard(1)),
        ("@#->  &Jobs/@#circle  &Meter", 'm', Message::Wizard(2)),
        ("@#->  &Jobs/@#circle  &Calc", 'c', Message::Wizard(3)),
        ("@#search  &Info", 'i', Message::Info),
        ("@#1+  &Quit", 'q', Message::Quit(true)),
    ] {
        header.menu.add_emit(
            &format!("{label}\t"),
            Shortcut::Ctrl | chr,
            MenuFlag::Normal,
            sender,
            msg,
        );
    }
    header.run.emit(sender, Message::Trans);
    calc.open.emit(sender, Message::Open);
    calc.save.emit(sender, Message::Save);
    window.emit(sender, Message::Quit(false));
    sender.send(Message::Theme(theme));
    let app = app::App::default().load_system_fonts();
    match app::fonts()
        .iter()
        .position(|item| item == " Fira Code Medium")
    {
        Some(position) => app::set_font(Font::by_index(position)),
        _ => app::set_font(Font::Courier),
    }
    while app.wait() {
        match receiver.recv() {
            Some(Message::Save) => calc.save(),
            Some(Message::Open) => calc.open(),
            Some(Message::Wizard(number)) => header.switch(wizard, number),
            Some(Message::Theme(ord)) => {
                theme = ord;
                match theme {
                    0 => {
                        ColorTheme::new(color_themes::TAN_THEME).apply();
                        app::set_scheme(app::Scheme::Base);
                    }
                    _ => {
                        ColorTheme::new(color_themes::DARK_THEME).apply();
                        app::set_scheme(app::Scheme::Plastic);
                    }
                };
            }
            Some(Message::Trans) => match header.frame.label().as_str() {
                "Virsh" => {
                    commands::virsh(
                        virsh
                            .nodes
                            .buffer()
                            .unwrap()
                            .text()
                            .split_whitespace()
                            .map(|x| x.parse::<usize>().unwrap())
                            .collect::<Vec<usize>>(),
                        &(std::env::var("HOME").unwrap() + "/report.xlsx"),
                    );
                }
                "Meter" => {
                    commands::meter(
                        commands::Env {
                            proto: meter.proto.choice().unwrap().as_str().to_string(),
                            port: meter.port.value().to_string().parse::<u16>().unwrap(),
                            concurrent: meter.concurrent.value().parse::<u8>().unwrap(),
                            targets: meter
                                .targets
                                .buffer()
                                .unwrap()
                                .text()
                                .split_whitespace()
                                .map(str::to_string)
                                .collect::<Vec<String>>(),
                            endpoints: meter
                                .endpoints
                                .buffer()
                                .unwrap()
                                .text()
                                .split_whitespace()
                                .map(str::to_string)
                                .collect::<Vec<String>>(),
                        },
                        &(std::env::var("HOME").unwrap() + "/report.xlsx"),
                    );
                }
                _ => {}
            },
            Some(Message::Info) => calc.info(),
            Some(Message::Quit(force)) => {
                if force || app::event() == Event::Close {
                    fs::write(&file, &[theme]).unwrap();
                    app::quit();
                }
            }
            None => {}
        }
    }
}
