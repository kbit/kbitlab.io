use {
    chrono::Local,
    gtk::{
        glib,
        prelude::{
            ApplicationExt, ApplicationExtManual, ButtonExt, Continue, EntryExt, GridExt, LabelExt,
            TextBufferExt, TextViewExt, WidgetExt,
        },
        Application, ApplicationWindow, Button, Entry, EntryBuffer, Grid, Label, PolicyType,
        ScrolledWindow, TextBuffer, TextView,
        WindowPosition::Center,
    },
    std::env,
};

pub fn app() {
    let application = Application::builder()
        .application_id("io.gitlab.kbit.rmeter")
        .build();
    application.connect_activate(window);
    application.run();
}

fn window(application: &Application) {
    let grid = Grid::builder()
        .margin(5)
        .column_homogeneous(true)
        .column_spacing(5)
        .row_homogeneous(true)
        .row_spacing(5)
        .build();
    let proto = Entry::with_buffer(&EntryBuffer::new(Some("https")));
    let port = Entry::with_buffer(&EntryBuffer::new(Some("443")));
    let concurrent = Entry::with_buffer(&EntryBuffer::new(Some("8")));
    let targets = TextView::with_buffer(&TextBuffer::builder().text("127.0.0.1").build());
    let endpoints = TextView::with_buffer(&TextBuffer::builder().text("/").build());
    let run = Button::with_mnemonic("Run");
    let result = Label::with_mnemonic("Result");
    let time = Label::with_mnemonic("Result");
    let right: i32 = 1;
    let left: i32 = 4;
    for (ord, item) in ["PROTO", "TARGETS", "PORT", "CONCURRENT", "ENDPOINTS"]
        .iter()
        .enumerate()
    {
        grid.attach(&Label::with_mnemonic(item), 0, ord as i32, right, 1);
    }
    grid.attach(&run, 0, 5, right, 1);
    grid.attach(&proto, 1, 0, left, 1);
    grid.attach(
        &ScrolledWindow::builder()
            .hscrollbar_policy(PolicyType::Automatic)
            .vscrollbar_policy(PolicyType::Automatic)
            .child(&targets)
            .build(),
        1,
        1,
        left,
        1,
    );
    grid.attach(&port, 1, 2, left, 1);
    grid.attach(&concurrent, 1, 3, left, 1);
    grid.attach(
        &ScrolledWindow::builder()
            .hscrollbar_policy(PolicyType::Automatic)
            .vscrollbar_policy(PolicyType::Automatic)
            .child(&endpoints)
            .build(),
        1,
        4,
        left,
        1,
    );
    grid.attach(&result, 1, 5, left, 1);
    grid.attach(&time, 0, 6, right + left, 1);
    glib::timeout_add_seconds_local(1, move || {
        time.set_text(&format!("{}", Local::now().format("%Y-%m-%d %H:%M:%S")));
        Continue(true)
    });
    run.connect_clicked(glib::clone!( @strong result => move |_| {
        let params = gtk_meter::Env {
            proto: proto.buffer().text(),
            port: port.buffer().text().parse::<u16>().unwrap(),
            concurrent: concurrent.buffer().text().parse::<u8>().unwrap(),
            targets: targets
                .buffer()
                .unwrap()
                .start_iter()
                .text(&targets.buffer().unwrap().end_iter())
                .unwrap()
                .split_whitespace()
                .map(str::to_string)
                .collect::<Vec<String>>(),
            endpoints: endpoints
                .buffer()
                .unwrap()
                .start_iter()
                .text(&endpoints.buffer().unwrap().end_iter())
                .unwrap()
                .split_whitespace()
                .map(str::to_string)
                .collect::<Vec<String>>(),
        };
        gtk_meter::run(params, &(env::var("HOME").unwrap() + "/report.xlsx"));
        let value = result.text();
        result.set_text(&(value.to_string() + " Done!"))
    }));
    ApplicationWindow::builder()
        .application(application)
        .window_position(Center)
        .default_width(960)
        .default_height(540)
        .child(
            &ScrolledWindow::builder()
                .hscrollbar_policy(PolicyType::Automatic)
                .vscrollbar_policy(PolicyType::Automatic)
                .child(&grid)
                .build(),
        )
        .build()
        .show_all();
}
