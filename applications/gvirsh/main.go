package main

import (
	"encoding/csv"
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
)

type Domain struct {
	Name   string `xml:"name"`
	Memory struct {
		Text string `xml:",chardata"`
	} `xml:"memory"`
	Vcpu struct {
		Text string `xml:",chardata"`
	} `xml:"vcpu"`
}

type Env struct {
	Usr string           `json:"usr"`
	Net string           `json:"net"`
	Bm  map[string]uint8 `json:"bm"`
}

func main() {
	var config, path string
	flag.StringVar(&path, "p", os.Getenv("HOME"), "path")
	flag.StringVar(&config, "c", path+"config.json", "config")
	flag.Parse()
	if err := exec.Command("virsh", "--version").Run(); err != nil {
		log.Fatalf("\x1b[31mVirsh not installed!\x1b[0m\n")
	}
	content, err := ioutil.ReadFile(config)
	if err != nil {
		log.Fatal(err)
	}
	var env Env
	env.Bm = make(map[string]uint8)
	if err = json.Unmarshal(content, &env); err != nil {
		log.Fatal(err)
	}
	children := make(map[string]chan [][]string)
	for name, address := range env.Bm {
		connect := "qemu+ssh://" + env.Usr + "@" + env.Net + "." + fmt.Sprint(address) + "/system"
		children[name] = make(chan [][]string)
		go getDomains(name, connect, children[name])
	}
	report := [][]string{
		{"BM", "VM", "RAM", "CPU"},
	}
	for _, child := range children {
		report = append(report, <-child...)
	}
	file, err := os.Create(path + "/report.csv")
	if err != nil {
		log.Fatal(err)
	}
	if err = csv.NewWriter(file).WriteAll(report); err != nil {
		log.Fatal(err)
	}
}

func getDomains(name string, connect string, ch chan [][]string) {
	defer close(ch)
	stdout, err := exec.Command("virsh", "--connect", connect, "list --name --state-running").Output()
	if err != nil {
		log.Fatalf("\x1b[31m%s\x1b[0m\n", connect)
	}
	const count int = 10
	children := []chan []string{}
	waitgroup := new(sync.WaitGroup)
	for i, vm := range strings.Fields(string(stdout)) {
		children = append(children, make(chan []string))
		waitgroup.Add(1)
		go getDomain(connect, name, vm, children[i], waitgroup)
		if i%count == 0 {
			waitgroup.Wait()
		}
	}
	report := [][]string{}
	for _, child := range children {
		report = append(report, <-child)
	}
	ch <- report
}

func getDomain(connect string, bm string, vm string, ch chan []string, waitgroup *sync.WaitGroup) {
	defer close(ch)
	stdout, err := exec.Command("virsh", "--connect", connect, "dumpxml "+vm).Output()
	if err != nil {
		log.Fatalf("\x1b[31m%s:%s\x1b[0m\n", connect, vm)
	}
	waitgroup.Done()
	domain := &Domain{}
	xml.Unmarshal([]byte(string(stdout)), domain)
	ch <- []string{
		strings.ToUpper(bm),
		strings.ToLower(domain.Name),
		domain.Memory.Text,
		domain.Vcpu.Text,
	}
}
