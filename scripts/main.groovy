/*
 * Description class Main
 * http://docs.groovy-lang.org/docs/latest/html/documentation
 * https://www.jdoodle.com/execute-groovy-online
 * https://codenarc.org/codenarc-rule-index.html
 * https://www.jenkins.io/doc/pipeline/steps
 */ 

node('node') {
    try {
        final String TOKEN = '/'
        final URI  SCMURI = new URI(scm.userRemoteConfigs.url[0])
        final List SCMLST = SCMURI.schemeSpecificPart.tokenize(TOKEN)
        if (SCMURI.scheme != 'https') { error('Invalid Bitbucket URL') }
        final Map BITBUCKET = [
            raw:"${SCM.scheme}://${SCM.host}/projects/${SCMLST[2]}/repos/" + SCMLST[3][0 .. -5] + '/raw',
            api:"${SCM.scheme}://${SCM.host}/rest/api/1.0", branch:'?at=refs/heads/' + scm.branches[0].name
        ]
        final String URL  = BITBUCKET.raw + '/jenkins/lib/common.groovy' + BITBUCKET.branch
        final Object HUDSON = load(
            http2file(
                URL.replace('lib/common', env.JOB_NAME.tokenize(TOKEN)[-2..-1].join(TOKEN))
            )
        )
        final Object SCRIPT = load(
            http2file(
                URL.replace('lib/hudson', env.JOB_NAME.tokenize(TOKEN)[-2..-1].join(TOKEN))
            )
        )
        HUDSON.setParams(SCRIPT.params())
        stage('Action') {
            timeout([time:1, unit:'HOURS']) { SCRIPT.action() }
        }
    }
    catch (error) {
        currentBuild.displayName += ':' + error.message
        unstable(error.message)
    }
    finally { cleanWs() }
}

END = -1
SLASH = '/'
HTTP = [
    BASE:[validResponseCodes:'200', ignoreSslErrors:true, consoleLogResponseBody:params.DEBUG],
    GIT:[authentication:scm.userRemoteConfigs.credentialsId[0]]
]

Map authBase() {
    [customHeaders:[[ maskValue:params.DEBUG, name:'Authorization', value:'Basic ' + env.CRED.bytes.encodeBase64()]]]
}

void setParams(final ArrayList param = []) {
    properties([
        [$class:'RebuildSettings', autoRebuild:false, rebuildDisabled:true],
        buildDiscarder(logRotator(daysToKeepStr:'7')),
        disableConcurrentBuilds(),
        parameters(
            [
                string(name:'JUSTIFICATION', description:"Обоснование из ${PRIVATE.task}"),
                choice(name:'CLUSTER', choices:[DEFAULT] + PRIVATE.clusters.keySet().sort())
            ] + param + [
                [$class:'WHideParameterDefinition', name:'EMAIL', defaultValue:DEFAULT],
                booleanParam(name:'DEBUG', defaultValue:false)
            ]
        )
    ])
    currentBuild.displayName = [
        currentBuild.displayName, params.JUSTIFICATION, currentUser().NAME.toUpperCase()
    ].join(' : ')
    params.keySet().each { var -> if (params[var] == '') { error(var + ' is empty') } }
    // currentBuild.rawBuild.parent.definition.scriptPath
}

String http2str(final String url) {
    try {
        return httpRequest(HTTP.BASE + [url:url] + (env.CRED ? authBase() : HTTP.GIT)).content
    }
    catch (error) {
        log(error.message)
        return ''
    }
}

String http2file(final String url) {
    final String FILE_NAME = new URI(url).path.tokenize(SLASH)[END]
    try {
        httpRequest(HTTP.BASE + [url:url, outputFile:FILE_NAME] + (env.CRED ? authBase() : HTTP.GIT))
        return FILE_NAME
    }
    catch (error) {
        log(error.message)
        return ''
    }
}

Map http2map(final String url) {
    try {
        switch (new URI(url).path.tokenize(SLASH)[END].tokenize(SLASH)[END]) {
            case 'yaml':
                return readYaml(text:http2str(url))
            case 'json':
                return readJSON(text:http2str(url))
        }
    }
    catch (error) {
        log(error.message)
        return [:]
    }
}

String str2http(final String metod, final String str, final String url) {
    try {
        final String JSON = 'APPLICATION_JSON'
        return httpRequest(HTTP.BASE + [url:url, httpMode:metod, requestBody:str,
            contentType:JSON, acceptType:JSON] +
                (env.CRED ? authBase() : HTTP.GIT)).content
    }
    catch (error) {
        log(error.message)
        return ''
    }
}

Map map2http(final String metod, final Map map, final String url) {
    try {
        return readJSON(text:str2http(metod, writeJSON(returnText: true, json: map), url))
    }
    catch (error) {
        log(error.message)
        return [:]
    }
}

Map currentUser() {
    Map user = [:]
    wrap([$class:'BuildUser']) {
        user += [
            'ID':env.BUILD_USER_ID,
            'EMAIL':env.BUILD_USER_EMAIL,
            'NAME':env.BUILD_USER,
            'GROUPS':env.BUILD_USER_GROUPS
        ]
    }
    user
}

void log2mail(final String attachmentsPattern = '', final String email = DEFAULT, final String extend = '') {
    final Map JENKINS_USER = currentUser()
    emailext([
        mimeType:'text/html',
        subject:"[${currentBuild.currentResult}][MSG] ${currentBuild.displayName}",
        body:"""
<br>Задача <a href='${env.BUILD_URL}'>${env.JOB_BASE_NAME}
</a> на основании <a href='${PRIVATE.task}/index.do?commandLine=%3D${params.JUSTIFICATION}'>${params.JUSTIFICATION}
</a> завершилась ${currentBuild.currentResult} за ${currentBuild.durationString[0 .. -14]}.<br>
<br>Инициатор jobs: <a href='${PRIVATE.user}'>${JENKINS_USER.NAME}
</a><br>${extend}<br>С уважением, команда <a href='${PRIVATE.wiki}'>${PRIVATE.team}</a><br>
""",
        attachLog:true,
        attachmentsPattern:attachmentsPattern,
        compressLog:true,
        to:(params.DEBUG || email == DEFAULT) ? JENKINS_USER.EMAIL : email + ',' + JENKINS_USER.EMAIL
    ])
}

void log(final String error) {
    ansiColor('term') { echo("\033[31m${error}\033[0m") }
}
