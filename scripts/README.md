```plantuml
@startebnf
!theme mars
caption Container 'Deploymenter'
Deploymenter = "main",(
    "clippit"|
    "core",(
        "xfce"|
        "vagrant"|
        "pgsql"|
        "mysql"|
        "eve-ng"|
        "opnsense"|
        "openmediavault"|
        "hugo","build"|
        "k8s",("ctrl"|"node")|
        "httpd","perl","otrs"|
        "nginx",(
            "site",(
                "php",("glpi"|"nextcloud"|"zabbix"),
                "perl","rt"
            )|
            "proxy",(
                "c","cockpit"|
                "go",("gitea"|"librespeed")|
                "java",(
                    "sonarqube"|
                    "tomcat",("jenkins"|"guacamole"|"artifactory")
                )
            )
        )
    )
);
@endebnf
```
