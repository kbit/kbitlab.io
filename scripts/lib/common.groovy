import org.apache.commons.codec.digest.DigestUtils

interface Pipeline {
    void action()
    List params()
}

class Ansible implements Pipeline {
    private void ansible(String host, String credId, String script) {
        ansiColor('xterm') {
            withCredentials([usernamePassword(
                credentialsId:credId,
                passwordVariable:'pass',
                usernameVariable:'user'
            )]) {
                ansiblePlaybook(
                    colorized:true
                    playbook:DigestUtils.md5Hex(env.JOB_BASE_NAME),
                    inventoryContent: writeYaml(
                        data:[
                            winrm:[
                                vars:[
                                    ansible_user: user,
                                    ansible_password: pass,
                                    ansible_connection: 'winrm',
                                    ansible_winrm_transport: 'ntlm',
                                    ansible_port: 5985,
                                ],
                                hosts: [host],
                            ]
                        ]
                    ),
                )
            }
        }
    }

    void action() {
        final List JOB_BASE_NAME = env.JOB_BASE_NAME.tokenize('_')
        switch (JOB_BASE_NAME[0]) {
            case 'cleanup':
                this.ansible(
                    JOB_BASE_NAME[-4..-1].join('.'),
                    'b8cc5d5da274bddee03c425b6269837e',
                    [[
                        hosts: 'winrm1'
                        gather_facts: false
                        tasks:[[
                            name: 'Remove a file'
                            win_shell: 'script1'
                        ]]
                    ]]
                )
                break
            case 'dataprovider':
                this.ansible(
                    JOB_BASE_NAME[-4..-1].join('.'),
                    '9354ac3f628b97c899667da482e04e9a',
                    [[
                        hosts: 'winrm'
                        gather_facts: false
                        tasks:[[
                            name: 'Remove a file, if present'
                            win_shell: 'script2'
                        ]]
                    ]]
                )
                break
        }
    }
}

class Rmeter implements Pipeline {
    List params() {
        [
            choice(name:'PROTO', choices:['http'], description:'Протокол'),
            text(name:'TARGETS', defaultValue:'127.0.0.1', description:'Цели тестирования'),
            string(name:'PORT', defaultValue:'80', trim:true, description:'Порт'),
            string(name:'CONCURRENT', defaultValue:'8', trim:true, description:'Количество параллельных потоков'),
            text(name:'ENDPOINTS', defaultValue:'/', description:'Endpoints')
        ]
    }

    void action() {
        final String TOKEN = '\n'
        writeFile(
            file:env.BUILD_TAG + '.json',
            text:writeJSON(returnText: true, json:[
                proto:params.PROTO,
                port:params.PORT.toInteger(),
                concurrent:params.CONCURRENT.toInteger(),
                targets:params.TARGETS.tokenize(TOKEN).sort(),
                endpoints:params.ENDPOINTS.tokenize(TOKEN).sort()
            ])
        )
        ansiColor('xterm') {
            sh(
                returnStdout:false,
                script:'time rmeter --config ' + env.BUILD_TAG + '.json --output ' + env.BUILD_TAG +'.xlsx'
            )
        }
        archiveArtifacts(
            artifacts:env.BUILD_TAG + '.xlsx',
            followSymlinks:false
        )
    }
}

