#!/usr/bin/env bash

function f_certbot
{
    if ! [[ -e '/etc/cron.daily/certbot_renew' ]]; then
        printf "sleep \${RANDOM} && certbot renew\n" >'/etc/cron.daily/certbot_renew'
        chmod +x '/etc/cron.daily/certbot_renew'
    fi
    certbot --nginx --rsa-key-size 4096 -d "${HOSTNAME}" # certbot certonly --standalone
}

function f_php_ini
{
    declare -r INI=${1:?}
    sed -ei '
        s|.*cgi.fix_pathinfo.*|cgi.fix_pathinfo = 0|;
        s|.*post_max_size.*|post_max_size = 100M|;
        s|.*max_execution_time.*|max_execution_time = 600|;
        s|.*max_input_time.*|max_input_time = 300|;
        s|.*memory_limit.*|memory_limit = 512M|;
        s|.*date.timezone.*|date.timezone = Europe/Moscow|;
        s|.*upload_max_filesize.*|upload_max_filesize = 100M|;
        s|.*session.auto_start.*|session.auto_start = off|;
        s|.*session.use_trans_sid.*|session.use_trans_sid = 0|;
        s|.*session.cookie_httponly.*|session.cookie_httponly = on|
            ' "${INI}"
}

function f_site
{
    declare -r SERVICE=${1:?} URL=${2:?}
    if ! [[ -e "${NGINX[CFG]}/${SERVICE}.conf" ]]; then
        f_php 'install'
        ((${#} > 1)) && f_unarch "${SERVICE}" "${NGINX[OUT]}" "${URL}"
        cat <<EOF >"${NGINX[CFG]}/${SERVICE}.conf"
server {
    server_name                       ${HOSTNAME};
    access_log                        /var/log/nginx/${SERVICE}.access.log;
    error_log                         /var/log/nginx/${SERVICE}.error.log;
    root                              ${NGINX[OUT]}/${SERVICE};
    index                             index.php index.html index.htm;
    location / {
        try_files \$uri \$uri/ =404;
    }
    location ~ [^/]\.php(/|$) {
        fastcgi_split_path_info       ^(.+\.php)(/.+)$;
        fastcgi_index                 index.php;
        fastcgi_pass                  unix:/var/run/php-fpm.sock;
        include                       fastcgi_params;
        fastcgi_param PATH_INFO       \$fastcgi_path_info;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}
EOF
        nginx -t
        nginx -s reload
    fi
}

function f_proxy
{
    declare -r PORT=${1:?}
    if ! [[ -e "${NGINX[CFG]}/proxy_${PORT}.conf" ]]; then
        f_nginx 'install'
        cat <<EOF >"${NGINX[CFG]}/proxy_${PORT}.conf"
server {
    server_name          ${HOSTNAME};
    access_log           /var/log/nginx/proxy_${PORT}.access.log;
    error_log            /var/log/nginx/proxy_${PORT}.error.log;
    location / {
        proxy_pass       http://127.0.0.1:${PORT}/;
        proxy_set_header Host            \$host;
        proxy_set_header X-Real-IP       \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        #
    }
}
EOF
        nginx -t
        nginx -s reload
    fi
}
