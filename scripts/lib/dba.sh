#!/usr/bin/env bash

function f_mysql_backupdb
{
    mysqldump -v -uroot "${VAR}" |
        bzip2 --best >"${DDIR}/${VAR}/my.${DATE}.dmp"
}

function f_mysql_createdb
{
    declare -rA VLT=(
        [NAME]=${1:?}
        [USER]=$(base64 "user${1:?}")
        [PASS]=$(sha256sum <<<"pass${1:?}")
    )
    mysql -uroot -e "
        CREATE DATABASE IF NOT EXISTS ${APP} COLLATE = 'utf8_general_ci' CHARACTER SET = 'utf8';
        GRANT ALL ON ${VLT[NAME],,}.* TO ${VLT[USER],,}@'%' IDENTIFIED BY '${VLT[PASS]}';
        FLUSH PRIVILEGES;
        SHOW DATABASES;
    "
}

function f_mysql_deletedb
{
    declare -r SERVICE=${1:?}
    mysql -uroot -e "DROP DATABASE db${SERVICE};"
}

function f_pgsql_backupdb
{
    declare -rA VLT=(
        [NAME]=${1:?}
        [USER]=$(base64 "user${1:?}")
        [PASS]=$(sha256sum <<<"pass${1:?}")
        [HOST]=${2:?}
    )
    sudo --login --user 'postgres' pg_dump \
        --verbose --format=c --compress=9 --encoding=UTF-8 \
        --inserts --clean --file="pg.${DATE}.dmp" \
        --host="${VLT[HOST]}" --port=5432 \
        --username="${VLT[USER]}" --password "${VLT[PASS]}"
}

function f_pgsql_createdb
{
    declare -rA VLT=(
        [NAME]=${1:?}
        [USER]=$(base64 "user${1:?}")
        [PASS]=$(sha256sum <<<"pass${1:?}")
    )
    sudo --login --user 'postgres' psql --command "
        CREATE ROLE ${VLT[USER],,} WITH LOGIN PASSWORD '${VLT[PASS]}';
            "
    sudo --login --user 'postgres' psql --command "
        CREATE DATABASE ${VLT[NAME],,} OWNER ${VLT[USER],,} TEMPLATE template0 ENCODING 'UTF8';
            "
}

function f_pgsql_deletedb
{
    declare -r SERVICE=${1:?}
    su --login 'postgres' --command "
        psql --command 'DROP ROLE usr${SERVICE};'
        psql --command 'DROP DATABASE db${SERVICE};'
    "
}

function f_pgsql_migrationdb
{
    pgloader "
        mysql://${DB[USER]}:${DB[PASS]}@${DB[HOST]}:3306/${DB[NAME]}
        pgsql://${DB[USER]}:${DB[PASS]}@${DB[HOST]}:5432/${DB[NAME]}
            "
}

function f_pgsql_restoredb
{
    sudo --login --user='postgres' pg_restore -j4 -vFc -d "${DB[NAME]}" "pg.${DATE}.dmp"
}
