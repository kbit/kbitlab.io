#!/usr/bin/env bash

declare -rA NGINX=(
    [OUT]='/usr/share/nginx/html'
    [CFG]='/etc/nginx/conf.d'
)

function f_artifactory
{
    declare -r APP='artifactory'
    if ! (which "${APP}"); then
        f_proxy 8081
        wget -O "/etc/yum.repos.d/jfrog-${APP}-rpms.repo" \
            "https://releases.jfrog.io/${APP}/${APP}-rpms/${APP}-rpms.repo"
        dnf install -y "jfrog-${APP}-oss"
        if (which 'postgres'); then
            f_pgsql_createdb "${APP}"
            ln -s "/opt/jfrog/${APP}/app/misc/"{db/postgresql,etc/db}.properties
        fi
        f_clear
    fi
}

function f_gitea
{
    declare -r APP='gitea' VER='1.12.3'
    if ! (which "${APP}"); then
        (which 'postgres') && f_pgsql_createdb "${APP}"
        f_proxy 3000
        wget -O "/usr/lobal/bin/${APP}" \
            "https://dl.${APP}.io/${APP}/${VER}/${APP}-${VER}-linux-amd64"
        cmod +x "/usr/lobal/bin/${APP}"
        f_clear
    fi
}

function f_core
{
    if ! (which 'nano'); then
        # localectl set-locale LANG=en_US.UTF-8
        # timedatectl set-timezone Europe/Moscow
        # hostnamectl set-hostname "${HOSTNAME}"
        printf 'install_weak_deps=False\n' >>/etc/dnf/dnf.conf
        dnf install -y epel-release {bind,yum}-utils firewalld dnf-automatic
        dnf config-manager --set-enabled crb
        mapfile -t < <(f_pkgs 'base')
        dnf install -y fuse-sshfs hdparm zip "${MAPFILE[@]}" ddrescue
        sed -ie '
            s|^apply_updates.*|apply_updates = yes|
                ' /etc/dnf/automatic.conf
        sed -ie '
            s|=permissive|=disabled|
                ' /etc/selinux/config
        setenforce 0
        systemctl enable --now dnf-automatic-notifyonly.timer
        firewall-cmd --permanent --remove-service=dhcpv6-client
        firewall-cmd --permanent --add-service=ssh
        firewall-cmd --reload
        f_swap
        f_logger 'info' "STAGE CORE DONE"
    fi
}

function f_grafana
{
    declare -r APP='grafana'
    if ! (which "${APP}-server"); then
        f_proxy 3000
        dnf install -y "${APP}" golang-github-prometheus
        if (which 'postgres'); then
            f_pgsql_createdb "${APP}"
            sed -ie "
                s|3306|5432|;
                s|^;type = sqlite3$|type = postgres|;
                s|^;user = root$|user = ${VLT[USER],,}|;
                s|^;password =$|password = ${VLT[PASS]}|
                    " "/etc/${APP}/${APP}.ini"
        fi
        systemctl enable --now "${APP}-server" prometheus
        f_clear
    fi
}

function f_clear
{
    dnf clean all
    f_logger 'info' "STAGE ${APP^^} DONE"
    f_logger 'audit' "$(ifconfig | awk '/broadcast/{print $2}')"
}

function f_clickhouse
{
    declare -r APP='clickhouse'
    if ! [[ -d "/usr/lib/systemd/system/${APP}-server.service" ]]; then
        dnf config-manager --add-repo "https://packages.${APP}.com/rpm/${APP}.repo"
        dnf install -y "${APP}-"{server,client}
        systemctl enable --now "${APP}-server"
        firewall-cmd --permanent --add-port=8123/tcp
        firewall-cmd --permanent --add-port=9000/tcp
        firewall-cmd --reload
        f_clear
    fi
}

function f_greenplum
{
    declare -r APP='greenplum' VER='6.23.0'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_core 'install'
        dnf install -y \
            "https://github.com/${APP}-db/gpdb/releases/download/${VER}\
/open-source-${APP}-db-${VER}-rhel8-x86_64.rpm"
        # shellcheck source=/dev/null
        source "/usr/local/${APP}-db-${VER}/${APP}_path.sh"
        cp "/usr/local/${APP}-db-${VER}/docs/cli_help/gpconfigs/gpinitsystem_singlenode" .
        gpinitsystem -c gpinitsystem_singlenode
        f_clear
    fi
}

function f_cockpit
{
    declare -r APP='cockpit'
    if [[ -e "/usr/lib/systemd/system/${APP}.service" ]]; then
        f_proxy 9090
        sed -ie "
            s|#|proxy_http_version 1.1;\n#|;
            s|#|proxy_buffering off;\n#|;
            s|#|proxy_set_header Upgrade \$http_upgrade;\n#|;
            s|#|proxy_set_header Connection \"upgrade\";|;
                " '/etc/nginx/conf.d/proxy_9090.conf'
        nginx -s reload
        dnf install -y "${APP}"{,-storaged}
        systemctl enable --now "${APP}"{,.socket}
        firewall-cmd --permanent --zone=public --add-service="${APP}"
        firewall-cmd --reload
    fi
}

function f_freeipa
{
    if [[ -n $(hostname -d) ]]; then
        declare -r DOMAIN='dev-pnlcalculator.ivlolatility.com'
        f_core 'install'
        yum install -y ipa-server{,-dns}
        firewall-cmd --permanent --add-service=freeipa-ldaps
        firewall-cmd --reload
        ipa-server-install -n "${DOMAIN}" -r "${DOMAIN^^}" --mkhomedir --setup-dns
        # curl -sS "idm.loc/ipa/config/ca.crt"
    fi
}

function f_oracledb
{
    if ! [[ -e '/etc/systemd/system/orcl12c.service' ]]; then
        f_core 'install'
        declare -ra PKG=(
            {make,binutils,sysstat,gcc{-c++,}}.x86_64 ksh smartmontools
            compat-lib{cap1.x86_64,stdc++-33} glibc{-devel,}.{i686,x86_64}
            lib{gcc,X{i,tst},stdc++{-devel,},aio{,-devel}}.{i686,x86_64}
        )
        yum install -y "${PKG[@]}"
        printf '%b\n' \
            'oracle\tsoft\tnproc\t131072\noracle\thard\tnproc\t131072' \
            'oracle\tsoft\tnofile\t131072\noracle\thard\tnofile\t131072' \
            'oracle\tsoft\tstack\t10240\noracle\tsoft\tcore\tunlimited' \
            'oracle\thard\tcore\tunlimited\noracle\tsoft\tmemlock\t50000000' \
            'oracle\thard\tmemlock\t50000000\noracl\tsoft\tstack\t10240' \
            >>/etc/security/limits.conf
        printf '%b\n' \
            'fs.aio-max-nr = 1048576\nfs.file-max = 6815744' \
            'kernel.shmall = 2097152\nkernel.shmmax = 4091019264' \
            'kernel.shmmni = 4096\nkernel.sem = 250 32000 100 128' \
            'net.core.rmem_default = 262144\nnet.core.rmem_max = 4194304' \
            'net.core.wmem_default = 262144\nnet.core.wmem_max = 1048586' \
            'net.ipv4.ip_local_port_range = 9000 65500' >>/etc/sysctl.conf
        sysctl -p
        mkdir -p '/stage' '/u0'{1,2}
        groupadd 'oinstall'
        groupadd 'dba'
        useradd -g 'oinstall' -G 'dba' -m 'oracle'
        chown -R 'oracle:oinstall' '/stage' '/u0'{1,2}
        chmod -R 775 /u0{1,2}
        chmod g+s /u0{1,2}
        aria2c https://download.oracle.com/otn/linux/oracle12c
        unzip -od /stage ~/*database.zip
        sudo --user oracle /stage/database/runInstaller -showProgress -silent \
            -responseFile /stage/database/response/db_install.rsp
        bash /u01/app/oraInventory/orainstRoot.sh
        bash /u01/app/oracle/home/root.sh
        sudo -login --user 'oracle' bash -c "
            /stage/database/runInstaller -executePrereqs
            /stage/database/runInstaller -silent -showProgress \
                -executeConfigTools -responseFile /u01/app/oracle/db_orcl12.rsp
            printf 'export %s\n' \
                'TMPDIR=/tmp' \
                'ORACLE_BASE=/u01/app/oracle' \
                'ORACLE_HOME=\${ORACLE_BASE}/orcl12c' \
                'ORACLE_SID=orcl' \
                'PATH=\${ORACLE_HOME}/bin:\${PATH}' \
                'LD_LIBRARY_PATH=\${ORACLE_HOME}/lib:/lib:/usr/lib:/usr/lib64'\
                'CLASSPATH=\${ORACLE_HOME}/jlib:\${ORACLE_HOME/rsqls/jlib' \ >
            .bash_profile
            bash .bash_profile
            # '${ORACLE_HOME}/bin/dbstart' '${ORACLE_HOME}'
            # '${ORACLE_HOME}/bin/dbshut' '${ORACLE_HOME}'
            "
        cat <<EOF >'/etc/systemd/system/orcl12.service'
[Unit]
Description=orcl12
After=network.target
[Service]
ExecStart=/u01/app/oracle/orcl12/bin/dbstart /u01/app/oracle/orcl12
ExecStop=/u01/app/oracle/orcl12c/bin/dbshut /u01/app/oracle/orcl12c
Type=forking
User=oracle
Group=oinstall
Restart=always
[Install]
WantedBy=multi-user.target
EOF
        systemctl 'daemon-reload'
    fi
}

function f_k8s
{
    if ! [[ -d '/etc/yum.repos.d/kubernetes.repo' ]]; then
        f_core 'install'
        printf 'br_netfilter\n' >/etc/modules-load.d/k8s.conf
        modprobe br_netfilter
        printf '%s\n' \
            'net.'{bridge.bridge-nf-call-ip{6,}tables,ipv4.ip_forward}' = 1' \
            >/etc/sysctl.d/kubernetes.conf
        sysctl --system
        cat <<EOF >/etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
        dnf install --assumeyes --disableexcludes=kubernetes \
            device-mapper-persistent-data lvm2 docker kube{let,adm,ctl}
        groupadd k8s
        useradd --create-home k8s --gid k8s
        usermod --append --groups docker k8s
        systemctl enable --now docker kubelet
    fi
}

function f_mysql
{
    declare -r APP='mariadb'
    if ! (which "${APP}"); then
        f_core 'install'
        declare -ra PKG=("${APP}"{,-server})
        dnf install -y "${PKG[@]}"
        systemctl enable --now firewalld "${APP}"
        firewall-cmd --permanent --add-service=mysql
        firewall-cmd --reload
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_nginx
{
    declare -r APP='nginx'
    if ! (which "${APP}"); then
        f_core 'install'
        dnf install -y {python3-certbot-,}"${APP}"
        sed -ie 's|_;|127.0.0.1;|' "/etc/${APP}/${APP}.conf"
        systemctl enable --now "${APP}"
        firewall-cmd --permanent --add-service={http,https}
        firewall-cmd --reload
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_pgsql
{
    declare -r APP='postgres' DIR='/var/lib/pgsql/data'
    if ! (which "${APP}"); then
        f_core 'install'
        dnf install -y postgresql-server
        postgresql-setup --initdb
        printf 'host\tall\tall\t127.0.0.1/32\tmd5\n' >>"${DIR}/pg_hba.conf"
        printf "listen_addresses = '*'\n" >>"${DIR}/postgresql.conf"
        systemctl enable --now postgresql
        firewall-cmd --permanent --add-service=postgresql
        firewall-cmd --reload
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_glpi
{
    declare -r APP='glpi' VER='10.0.5'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        (which 'mariadb') && f_mysql_createdb
        f_site "${APP}" \
            "https://github.com/glpi-project/${APP}/releases/download/${VER}/${APP}-${VER}.tgz"
        chown -R 'nginx:nginx' "${NGINX[OUT]}/${APP}"/{files,config,marketplace}
        chmod -R 775 "${NGINX[OUT]}/${APP}"/{files,config,marketplace}
        f_clear
    fi
}

function f_jenkins
{
    declare -r APP='jenkins'
    if [[ -e '/usr/local/etc/rc.d/jenkins' ]]; then
        f_proxy 8080
        wget -O {'/etc/yum.repos.d',"https://pkg.${APP}.io/redhat-stable"}/"${APP}.repo"
        rpm --import "https://pkg.${APP}.io/redhat-stable/${APP}.io.key"
        dnf install -y fontconfig java-11-openjdk "${APP}"
        systemctl enable --now "${APP}"
        f_clear
    fi
}

function f_gvm
{
    declare -r APP='gvm'
    if [[ -f '/etc/redis/redis.conf' ]]; then
        f_core 'install'
        dnf install -y \
            'http://www6.atomicorp.com/channels/atomic/centos/9/x86_64/RPMS/atomic-release-1.0-23.el9.art.noarch.rpm'
        dnf install -y redis "${APP}"
        sed -ie '
            s|^#.*unixsocket .*|unixsocket /var/run/redis/redis.sock|;
            s|^#.*unixsocketperm .*|unixsocketperm 700|
            ' /etc/redis/redis.conf
        systemctl enable --now redis
        chown gvm /var/run/redis/redis.sock
        # gvm-setup
        f_clear
    fi
}

function f_php
{
    declare -rA PHP=(
        [SCK]="/var/run/php-fpm.sock"
        [CFG]="/etc/php-fpm.d/www.conf"
        [INI]="/etc/php.ini"
        [CGI]='/etc/nginx/fastcgi.conf'
    )
    declare -ra PKG=(
        php-{fpm,cli,intl,bcmath,gd,dba,gmp,opcache,mysqlnd,pgsql,ldap,mbstring,curl,xml}
    )
    if ! (which "${PKG[0]}"); then
        f_nginx 'install'
        dnf install -y "${PKG[@]}"
        f_php_ini "${PHP[INI]}"
        sed -ie "
            s|.*listen = .*|listen = ${PHP[SCK]}|;
            s|.*listen.owner.*|listen.owner = nginx|;
            s|.*listen.group.*|listen.group = nginx|;
            s|.*listen.mode.*|listen.mode = 0660|
                " "${PHP[CFG]}"
        systemctl enable --now "${PKG[0]}"
        f_logger 'info' "STAGE ${PKG[0]^^} DONE"
    fi
}

function f_rsyslog
{
    declare -r APP='rsyslog'
    declare -r DIR="/etc/${APP}.d"
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_site 'loganalyzer'
        f_unarch 'loganalyzer' '/usr/share/nginx/html' \
            'https://download.adiscon.com/loganalyzer/loganalyzer-4.1.12.tar.gz'
        dnf install -y "${APP}"{,-mysql}
        if (which 'mysql'); then
            f_mysql_createdb "${APP}"
            cat <<EOF >"${DIR}/ommysql.conf"
module(
    load="ommysql"
)
action(
    type="ommysql"
    db="${APP}"
    user="$(f_base "user${APP}")"
    pass="$(f_hash "pass${APP}")"
    server="127.0.0.1"
)
EOF
        fi
        cat <<EOF >"${DIR}/imudp.conf"
module(
    load="imudp"
)
input(
    type="imudp"
    port="514"
)
EOF
        cat <<EOF
module(
    load="imfile"
    PollingInterval="10"
)
input(
    type="imfile"
    File="file.log"
    Tag="service"
    Severity="info"
)
*.* @127.0.0.1:514;RSYSLOG_SyslogProtocol23Format
EOF
        systemctl enable --now "${APP}"
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}
