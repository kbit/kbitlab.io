#!/usr/bin/env bash

declare -ra CURL=(
    curl --fail --insecure --location --silent --show-error --connect-timeout 0
) JQ=(
    jq --raw-output --exit-status
) RDP=(
    xfreerdp +home-drive +clipboard +fonts /cert:ignore
)

function f_push_sshkey
{
    declare -r NET=${1:?}
    while read -r; do
        sshpass -e ssh-copy-id -o 'StrictHostKeyChecking=no' -o 'ConnectTimeout=20' -f "admin@${REPLY}"
    done < <(fping --alive --generate "${NET}")
}

function f_stodav
{
    if (mount.davfs); then
        declare -rA STR=(
            [DIR]='/mnt/example_com'
            [HST]='example.com'
        )
        [[ -d ${STR[DIR]} ]] || mkdir --parent "${STR[DIR]}"
        read -resp "Enter password: "
        printf '%s\t' "${STR[DIR]}" "${USER}" "${REPLY}" >>'/etc/davfs2/secrets'
        cat <<EOF >"/etc/systemd/system/mnt_example_com"
[Unit]
Description=Mount WebDAV Service
After=network.target
Wants=network.target

[Mount]
What=https://${STD[HST]}/remote.php/webdav
Where=/mnt/${STD[DIR]}
Options=uid=${USER},file_mode=0664,dir_mode=2775,grpid
Type=davfs
TimeoutSec=15

[Install]
WantedBy=multi-user.target
EOF
        systemctl daemon-reload
        systemctl enable --now 'mnt_example_com'
    fi
}

function f_stossh
{
    if (which 'sshfs'); then
        declare -rA STR=(
            [DIR]='/mnt/sshfs'
            [HST]="${HOSTNAME}"
        )
        ssh-keygen -t ed25519 -P '' -f ~/.ssh/id_rsa
        read -resp "Enter password: "
        export SSHPASS="${REPLY}"
        ssh-pass -e ssh-copy-id "${USERNAME}@${STR[HST]}"
        export -n SSHPASS
        [[ -d ${STR[DIR]} ]] || mkdir --parent "${STR[DIR]}"
        cat <<EOF >"/etc/systemd/system/sshfs.service"
[Unit]
Description=sshfs
After=network.target
[Service]
ExecStart=/usr/bin/sshfs "${STR[HST]}:${STR[DIR]}" "${STR[DIR]}"
ExecStop=/usr/bin/fusermount -u "${STR[DIR]}"
Type=simple
User=${USER}
Group=${USER}
Restart=always
[Install]
WantedBy=multi-user.target
EOF
        systemctl daemon-reload
        systemctl enable --now sshfs
    fi
}

function f_tunany
{
    if (which 'openconnect'); then
        cat <<EOF >"/etc/systemd/system/${0##*/}.service"
[Unit]
Description=${0##*/}
After=network.target
[Service]
ExecStart=/usr/local/sbin/openconnect --background --passwd-on-stdin \
    --user "${TUN[USR]}" "${TUN[HST]}" <<< "${TUN[SEC]}"
Type=simple
User=${TUN[USR]}
Group=${TUN[USR]}
Restart=always
[Install]
WantedBy=multi-user.target
EOF
        systemctl daemon-reload
        systemctl enable --now "${0##*/}"
    fi
}

function f_tunvpn
{
    if (which 'openvpn'); then
        declare -r CFG=${1:?}
        cat <<EOF >"/etc/systemd/system/openvpn_${CFG}.service"
[Unit]
Description=openvpn
After=network.target
[Service]
ExecStart=/usr/sbin/openvpn --config /home/${USER}/.pki/${CFG}.ovpn
Type=simple
User=root
Group=root
Restart=always
[Install]
WantedBy=multi-user.target
EOF
        systemctl daemon-reload
        systemctl enable --now "openvpn_${CFG}"
    fi
}

function f_tunssh
{
    if (which 'autossh'); then
        declare -rA TUN=(
            [HST]="${HOSTNAME}"
            [PRT]=''
        )
        ssh-keygen -t ed25519 -P '' -f ~/.ssh/id_ed25519
        read -resp "Enter password: "
        export SSHPASS="${REPLY}"
        ssh-pass -e ssh-copy-id "${USERNAME}@${TUN[HST]}"
        export -n SSHPASS
        cat <<EOF >"/etc/systemd/system/${0##*/}.service"
[Unit]
Description='${0##*/}'
After=network.target
[Service]
ExecStart=/usr/bin/autossh -C -NL "127.0.0.1:${TUN[PRT]}:127.0.0.1:${TUN[PRT]}" \
    "${USER}@${TUN[HST]}:${TUN[PRT]}"
Type=simple
User=${USER}
Group=${USER}
Restart=always
[Install]
WantedBy=multi-user.target
EOF
        systemctl daemon-reload
        systemctl enable --now "${0##*/}"
    fi
}

function f_conldap
{
    # declare -A CON=()
    declare JSON=''
    for param in "${!DB[@]}"; do
        read -erp "Enter your connect ${param}:    "
        # CON[${param}]="${REPLY}"
        JSON+="${param}:${REPLY},"
    done
    "${JQ[@]}" '.' <<<"{${JSON::-1}}"
    declare -rA IPA=(
        [HOST]="ldap://$(hostname -d)"
        [BASE]="cn=users,cn=accounts,$(hostname -d | sed 's/.*/\U&/')"
        [BIND]="uid=${USER},cn=users,cn=accounts,$(hostname -d | sed 's/.*/\U&/')"
        [FLTR]="(&(objectClass=*)(uid=${USER}))"
    )
    declare -ra LDAP=(
        ldapsearch -vLLL -x -W
        -H "${IPA[HOST]}"
        -D "${IPA[BIND]}"
        -b "${IPA[BASE]}"
        "${IPA[FLTR]}"
        dn memberOf mail
    )
    bash -xc "${LDAP[*]}"
}

function f_swaks
{
    declare -r HST=${1:?} PRT=${2:?} USR=${3:?} DMN=${4:?} TO=${5:?}
    read -resp "Enter password: "
    zip --recurse-paths --move --password
    declare -ra SWAKS=(
        swaks --silent
        --server "${HST}:${PRT}"
        --h-Subject "${HOSTNAME}"
        --body '@/etc/os-release'
        --attach '@/etc/os-release'
        --from "${USR}@${DMN}"
        --to "${TO}@${DMN}"
        --auth-user "${USR}@${DMN}"
        --auth-password "${REPLY}"
    )
    bash -xc "${SWAKS[*]}"
}

function f_proxyssh
{
    if (which 'sshuttle'); then
        declare -rA TUN=(
            [HST]="${HOSTNAME}"
            [PRT]=''
        )
        ssh-keygen -t ed25519 -P '' -f ~/.ssh/id_ed25519
        read -resp "Enter password: "
        export SSHPASS="${REPLY}"
        ssh-pass -e ssh-copy-id "${USERNAME}@${TUN[HST]}"
        export -n SSHPASS
        cat <<EOF >"/etc/systemd/system/sshuttle.service"
[Unit]
Description=sshuttle
After=network.target
[Service]
ExecStart=/usr/bin/sshuttle --remote 192.168.0.1 0.0.0.0/0
Type=simple
User=admin
Group=admin
Restart=always
[Install]
WantedBy=multi-user.target
EOF
        systemctl daemon-reload
        systemctl enable --now "${0##*/}"
    fi
}

function f_env
{
    declare -r JSON=$(cat "${ID}.json")
    for param in "${!ENV[@]}"; do
        ENV[param]=$("${JQ[@]}" ".${param}" <<<"${JSON}")
    done
}

function f_swap
{
    # Create SWAP file
    declare -r SWAP='/var/swap.img'
    if ! [[ -e "${SWAP}" ]]; then
        declare -i MEM
        MEM=$(free -m | awk '/Mem/{print $2}')
        ((MEM <= 4096)) && ((MEM *= 2))
        fallocate --length "${MEM}"MB "${SWAP}"
        chmod 0600 "${SWAP}"
        mkswap "${SWAP}"
        swapon "${SWAP}"
        printf '%s\tnone\tswap\tsw\t0\t0' "${SWAP}" >>/etc/fstab
    fi
}

function f_timer
{
    if (command time); then
        command time --format='\nNAME:\t%C\nCODE:\t%X\nTIME:\t%E\nCPU:\t%P\nRAM:\t%M' "${@}"
    else
        time "${@}"
    fi
}

function f_unarch
{
    declare -rA ENV=(
        [APP]="${1:?}"
        [OUT]="${2:?}"
        [URL]="${3:?}"
        [TMP]=$(mktemp)
    )
    if ! [[ -e "${ENV[OUT]}/${ENV[APP]}" ]]; then
        if [[ "${ENV[URL]##*.}" == 'zip' ]]; then
            "${CURL[@]}" --output "${ENV[TMP]}" "${ENV[URL]}"
            unzip "${ENV[TMP]}" -d "${ENV[OUT]}"
            rm -v "${ENV[TMP]}"
        else
            "${CURL[@]}" "${ENV[URL]}" |
                tar --extract --verbose --overwrite --gzip --directory="${ENV[OUT]}"
        fi
    fi
}

function f_masscan
{
    declare -r PORTS='3389,22'
    declare ADAPTER='' RANGE=''
    RANGE=$(ip ro | awk '{if (FNR==2) print $1}')
    ADAPTER=$(ip ro | awk "/${RANGE//'/'/'\/'}/{ print \$3}")
    declare -r ADAPTER RANGE
    masscan --open-only --adapter "${ADAPTER}" --range "${RANGE}" \
        --ports "${PORTS}" -oJ 'masscan.json'
    # fping --quiet --alive --generate "${RANGE}" > 'fping.txt'
}

function f_chk_hdd
{
    declare -r DISK=${1:?}
    if ( ((${#})) && (lsblk --list | grep --word-regexp --count "${DISK}")); then
        declare -rA HCHK=(
            [DSK]="/dev/${DISK}"
            [LOG]=$(mktemp)
        )
        badblocks -s "${HCHK[DSK]}" -o "${HCHK[LOG]}"
        e2fsck -l "${HCHK[LOG]}" "${HCHK[DSK]}"
        hdparm -tT "${HCHK[DSK]}"
        sync
        dd if=/dev/zero of="${HCHK[LOG]}" bs=1MiB count=1024
        sync
        7za b "$(shuf -i1-10 -n1)"
    fi
}

function f_openssl
{
    cat <<'EOF' >req.conf
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_req
prompt = no
[req_distinguished_name]
C =  GE
ST = VA
L = Ulm
O = Company
OU = Team
CN = keycloak
[v3_req]
keyUsage = keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = keycloak
DNS.2 = keycloak.dev
DNS.3 = keycloak.dev.svc.cluster.local
EOF
    openssl req -x509 -nodes -days 3650 -newkey rsa:4096 \
        -keyout cert.pem -out cert.pem -config req.conf -extensions 'v3_req'
    openssl x509 -in cert.pem -noout -text

    openssl req -x509 -nodes -days 365 -newkey rsa:4096 \
        -keyout "/etc/pki/tls/private/${HOSTNAME}.key" \
        -out "/etc/pki/tls/certs/${HOSTNAME}.crt" \
        -subj /C=RU/ST=Moscow/L=Moscow/O=Companyname/OU=User/CN=etc/emailAddress=support@site.com
    openssl s_client -showcerts \
        -connect "${HOSTNAME}:${PRT}" 2>&1 </dev/null |
        sed -n "/BEGIN/,/END/w ${HOSTNAME}.crt"
    cp "${HOME}/${HOSTNAME}.crt" "/etc/pki/ca-trust/source/anchors/${HOSTNAME}.crt"
    openssl x509 -inform DER -in ~/pki/*.cer -out "/etc/pki/ca-trust/source/anchors/${HOSTNAME}.crt"
    update-ca-trust
}

function f_gitlab_upgrade
{
    du -sh /var/opt/gitlab/git-data/repositories
    gitlab-rake gitlab:backup:create SKIP=registry,artifacts
    ls -lh /var/opt/gitlab/backups/
    gitlab-psql -c "SELECT job_class_name, table_name, column_name, job_arguments FROM batched_background_migrations WHERE status <> 3;"
    # apt-cache madison gitlab-ce
    apt-get install gitlab-ce=16.6.6-ce.0
    gitlab-ctl status
    gitlab-rake gitlab:env:info
    apt-get install gitlab-ce=16.6.6-ce.0
    if (which gitlab-runner); then
        gitlab-runner --version
    fi
    printf 'Now I will update gitlab-runner. There may be problems with starting pipelines within 10 minutes.\n'
    bash < <(
        "${CURL[@]}" 'https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh'
    )
    apt-get install gitlab-runner=16.6.2
}

function f_inv_gen
{
    set -euo pipefail
    if [[ -d 'inventory' ]]; then
        mkdir 'inventory'
    fi
    declare -r HOSTS=$(awk '/^Host/{print $2}' ~/.ssh/config | grep -vE '(modem|\*)' | sort --uniq)
    while read -r; do
        printf '\n[%s]\n' "${REPLY}"
        grep "^${REPLY}\." <<<"${HOSTS}"
    done < <(awk -F '.' '{print $1}' <<<"${HOSTS}" | uniq) >'inventory/hosts'
}

function threads
{
    set -euo pipefail
    mapfile -t < <(
        while read -r; do
            declare -r LOG=$(mktemp)
            f_action "${REPLY}" >"${LOG}" &
            printf '%s\n' "${LOG}"
        done < <(printf '%s\n' {0..9})
    )
    wait
    for log in "${MAPFILE[@]}"; do
        cat "${log}"
        rm -v "${log}"
    done
}

function other
{
    printf '%s' "${RDP[@]}" "${ZIP[@]}"
}
