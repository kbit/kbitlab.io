#!/usr/bin/env bash

declare -ra REG=('docker.io' 'theavege')

function f_bash
{
    declare -ra APPs=(bash sh{ellcheck,fmt} wget)
    declare -r ACTION=${1:?}
    case ${ACTION} in
        build)
            declare -ra PKG=(
                "${APPs[@]}" ca-certificates
            ) RUN=(
                "which ${APPs[*]}"
            )
            declare -rA TARS=(
                [debian]='debian:stable-slim'
                [alpine]='alpine:edge'
                [fedora]='scratch'
            )
            for key in "${!TARS[@]}"; do
                f_build "${key}" "${TARS[${key}]}"
            done
            ;;
        lint)
            if (which "${APPs[@]}"); then
                while read -r; do
                    bash -xc "
                    shellcheck --check-sourced ${REPLY} 1>&2
                    shfmt -ci -fn -i 4 -d ${REPLY} 1>&2
                        "
                done < <(find . -type 'f' -name '*.sh')
            else
                f_logger error "Some not found from ${APPs[*]}"
            fi
            ;;
    esac
}

function f_build
{
    # https://github.com/containers/buildah/tree/main/docs/tutorials
    # https://docs.podman.io/en/latest/Tutorials.html
    declare -r DISTRO=${1:?} IMG=${2:?}
    if ((UID)); then
        f_logger error 'Muste be run under root, aborting'
    elif (buildah login "${REG[0]}" 1>&2); then
        declare -rA POD=(
            [ID]=$(buildah from "${IMG}")
        )
        buildah run "${POD[ID]}" set -euo pipefail
        case ${DISTRO} in
            alpine) buildah run "${POD[ID]}" apk add --no-cache "${PKG[@]}" ;;
            fedora)
                declare -ra yumOpt=(
                    '--installroot'
                    "$(buildah mount "${POD[ID]}")"
                    "--releasever=${VERSION_ID}"
                )
                dnf install -y "${yumOpt[@]}" "${PKG[@]}"
                dnf clean all "${yumOpt[@]}"
                ;;
            debian)
                buildah run "${POD[ID]}" apt-get update
                buildah run "${POD[ID]}" apt-get install --yes --no-install-recommends apt-utils "${PKG[@]}"
                buildah run "${POD[ID]}" apt-get clean
                buildah run "${POD[ID]}" find '/var/lib/apt/lists/' -depth -type f -delete
                ;;
        esac
        for cmd in "${RUN[@]}"; do
            buildah run "${POD[ID]}" bash -xc "${cmd}" 1>&2
        done
        buildah commit "${POD[ID]}" "${PKG[0]}"
        buildah push "${PKG[0]}" "${REG[0]}/${REG[1]}/${PKG[0]}:${DISTRO}"
        buildah logout "${REG[0]}"
    fi || f_logger audit 'Not build!'
    for item in unmount rm rmi; do
        buildah "${item}" --all
    done
    podman run --rm --interactive --tty --user "${UID}:$(id -g)" \
        --volume "${PWD}:/tmp" --workdir '/tmp' \
        "${REG[0]}/${REG[1]}/${PKG[0]}:${DISTRO}" bash 1>&2
    # podman run --rm --interactive --tty "${REG[0]}/${REG[1]}/${PKG[0]}:${DISTRO}" bash 1>&2
    podman system prune --all --force
    f_logger info "Build for ${PKG[*]} done!"
}

function f_clone
{
    declare -r BRANCH=${1:?} REPO=${2:?}
    git clone --depth 1 --branch "${BRANCH}" "${REPO}"
}

function f_golang
{
    declare -r ACTION=${1:?}
    case ${ACTION} in
        build)
            declare -ra PKG=(golang gcc) RUN=('go env')
            for key in 'debian' 'alpine'; do
                f_build "${key}" "${REG[0]}/${REG[1]}/bash:${key}"
            done
            ;;
        lint)
            if (which 'go'); then
                while read -r; do
                    bash -xc "go vet ${REPLY} 1>&2"
                    bash -xc "GO111MODULE='off' go build -ldflags='-s -w' ${REPLY} 1>&2"
                done < <(find . -type 'f' -name 'main.go')
            else
                f_logger error 'Go not found'
            fi
            ;;
    esac
}

function f_groovy
{
    declare -r ACTION=${1:?}
    case ${ACTION} in
        build)
            declare -ra PKG=(groovy libcodenarc-groovy-java) RUN=('which "groovy"')
            f_build 'debian' "${REG[0]}/${REG[1]}/bash:debian"
            ;;
        lint)
            if (which 'groovy'); then
                declare -ra CODENARC=(
                    java -classpath
                    /usr/share/java/groovy-all.jar:/usr/share/java/simple.jar:/usr/share/java/CodeNarc.jar
                    org.codenarc.CodeNarc -includes="**/*.groovy" -basedir='scripts'
                    -rulesetfiles='rulesets/basic.xml,rulesets/braces.xml,rulesets/concurrency.xml,rulesets/convention.xml,rulesets/dry.xml,rulesets/enhanced.xml,rulesets/exceptions.xml,rulesets/formatting.xml,rulesets/generic.xml,rulesets/grails.xml,rulesets/groovyism.xml,rulesets/imports.xml,rulesets/jdbc.xml,rulesets/junit.xml,rulesets/logging.xml,rulesets/naming.xml,rulesets/security.xml,rulesets/serialization.xml,rulesets/size.xml,rulesets/unnecessary.xml,rulesets/unused.xml'
                )
                "${CODENARC[@]}"
            else
                f_logger error 'Groovy not install'
            fi
            ;;
    esac
}

function f_powershell
{
    declare -r ACTION=${1:?}
    declare -ra APPs=(pwsh dotnet)
    case ${ACTION} in
        build)
            declare -ra PKG=(powershell,dotnet8-sdk) RUN=(
                "which ${APPs[*]}"
                'wget "https://psg-prod-eastus.azureedge.net/packages/psscriptanalyzer.1.21.0.nupkg"'
                'unzip "psscriptanalyzer.1.21.0.nupkg" -d "/usr/lib/powershell/Modules/PSScriptAnalyzer"'
                'rm -v psscriptanalyzer.1.21.0.nupkg'
            )
            f_build 'alpine' "${REG[0]}/${REG[1]}/bash:alpine"
            ;;
        lint)
            if (which "${APPs[@]}"); then
                declare -ra CMD=(
                    'Import-Module -Name PSScriptAnalyzer'
                    'Invoke-ScriptAnalyzer -EnableExit -Recurse -Path scripts'
                )
                for cmd in "${CMD[@]}"; do
                    pwsh -Command "${cmd}"
                done
            else
                f_logger audit "Nope"
            fi
            ;;
    esac
}

function f_python
{
    declare -ra APPs=(python3 {py,yaml,ansible-}lint bandit black)
    declare -r ACTION=${1:?}
    case ${ACTION} in
        build)
            declare -ra PKG=(
                "${APPs[@]}" python3-{hvac,mypy,pip-whl,kubernetes}}
            ) RUN=(
                "which ${APPs[*]}"
                "python3 -m ansible galaxy collection install community.{general,hashi_vault}"
            )
            for key in 'debian'; do
                f_build "${key}" "${REG[0]}/${REG[1]}/bash:${key}"
            done
            ;;
        lint)
            if (which "${APPs[@]}"); then
                find "${PWD}" -name '*.yml' -type 'f' |
                    xargs -I{} bash -xc "python3 -m yamllint {} 1>&2"
                while read -r; do
                    bash -xc "python3 -m pylint ${REPLY} 1>&2"
                    bash -xc "python3 -m black --check ${REPLY} 1>&2"
                done < <(find . -name '*.py')
            else
                f_logger error "Some not found from ${APPs[*]}"
            fi
            ;;
    esac
}

function f_rust
{
    declare -r ACTION=${1:?}
    case ${ACTION} in
        build)
            declare -ra PKG=(
                cargo rust{fmt,-clippy} git gcc{,-multilib} lib{std-rust-dev-wasm32,{clang,xlsxwriter,ssl,gtk-3}-dev}
                pkg-config lld {c,}make wget
            ) RUN=(
                'wget -qO- "https://github.com/thedodd/trunk/releases/download/v0.17.5/trunk-x86_64-unknown-linux-gnu.tar.gz" | tar -C "/usr/local/bin" -xzf-'
            )
            f_build 'debian' "${REG[0]}/${REG[1]}/bash:debian"
            ;;
        lint)
            if (which 'cargo'); then
                cargo clippy --ignore-rust-version && cargo fmt --all
            else
                f_logger error 'Cargo not found'
            fi
            ;;
    esac
}

function f_k8s_debug
{
    while read -r node; do
        kubectl debug -it "${node}" --image='alpine'
        # chroot /host
    done < <(kubectl get node -oname)
    # kubectl -n nodejs-example delete --cascade=orphan sts minio
}

function f_k8s_node
{
    if ! [[ -f 'kubeadm.log' ]]; then
        f_k8s 'install'
        firewall-cmd --permanent --add-port={6783,10250-10255,30000-32767}/tcp
        firewall-cmd --reload
        sudo --login --user 'k8s' "$(tail -n2 kubeadm.log)"
    fi
}

function f_k8s_ctrl
{
    if ! [[ -f 'kubeadm.log' ]]; then
        f_k8s 'install'
        firewall-cmd --permanent --add-port={6443,2379-2380,10250-10255}/tcp
        firewall-cmd --reload
        kubeadm config images pull
        # kubeadm reset --force
        kubeadm init | tee kubeadm.log
        scp -o StrictHostKeyChecking=no kubeadm.log{,node:~/}
        mkdir --parent '/home/k8s/.kube'
        cp '/etc/kubernetes/admin.conf' '/home/k8s/.kube/config'
        chown -R k8s:k8s '/home/k8s'
        su --login 'k8s' --command '
            kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
        '
    fi
}

function f_deckhouse_del
{
    declare -r KUBECONFIG=".kube/${1:?}.yaml"
    while read -r; do
        case "${REPLY}" in
            'deckhouse' | 'global') continue ;;
            *) kubectl --kubeconfig="${KUBECONFIG}" edit "${REPLY}" ;; # spec:enabled: false
        esac
    done < <(kubectl --kubeconfig="${KUBECONFIG}" --namespace d8-system get moduleconfigs)
    for namespace in d8-{monitoring,system}; do
        while read -r; do
            helm --kubeconfig="${KUBECONFIG}" --namespace "${namespace}" uninstall "${REPLY}"
        done < <(helm --kubeconfig="${KUBECONFIG}" --namespace "${namespace}" list --all -ojson | jq -M 'map(.name).[]')
    done
    while read -r; do
        if (kubectl --kubeconfig="${KUBECONFIG}" --namespace d8-system get "${REPLY}" -oyaml | grep 'heritage: deckhouse'); then
            kubectl --kubeconfig="${KUBECONFIG}" --namespace d8-system delete "${REPLY}"
        fi
    done < <(kubectl --kubeconfig="${KUBECONFIG}" --namespace d8-system get crd -oname)
}
