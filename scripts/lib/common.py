"""
https://wombat.org.ua/AByteOfPython
https://habr.com/ru/articles/736842
"""

from json import dumps, loads
from os import environ, listdir, path
from socket import gethostname
from sys import argv, stderr, exit as error
from subprocess import run
from urllib import request


def k8s():
    """docstring"""
    fstring = "\t\x1b[31m%s:\t\x1b[32m%s:\x1b[0m\n"
    stderr.write("\tList clusters:\n" + fstring % ("#", "items"))
    cluster = get_values(
        [
            path.splitext(file)[0]
            for file in listdir(f"{environ.get('HOME')}/.kube")
            if path.splitext(file)[1] == ".yaml"
        ]
    )
    config = f"--kubeconfig={environ.get('HOME')}/.kube/{cluster}.yaml"
    stderr.write(f"\tList namespace in {cluster}:\n" + fstring % ("#", "items"))
    namespace = get_values(get_names(["kubectl", config, "get", "ns"]))
    stderr.write(f"\tList pod in {namespace}:\n" + fstring % ("#", "items"))
    pod = get_values(get_names(["kubectl", config, "-n", namespace, "get", "pod"]))
    stderr.write(f"\tList commands for pod {pod}\n" + fstring % ("#", "items"))
    command = get_values(
        [
            ["/bin/sh"],
        ]
    )
    if command[0] == "/bin/sh" and len(command) == 1:
        run(
            ["kubectl", config, "-n", namespace, "exec", "-it", pod, "--"] + command,
            check=True,
        )
    else:
        run(
            ["kubectl", config, "-n", namespace, "exec", pod, "--"] + command,
            check=True,
        )


def get_names(array):
    """docstring"""
    return [
        item["metadata"]["name"]
        for item in loads(
            run(
                array + ["-ojson"],
                check=True,
                capture_output=True,
            ).stdout.decode()
        )["items"]
    ]


def get_values(array):
    """docstring"""
    ferror = "\x1b[31m%s\x1b[0m\n"
    fstring = "\t\x1b[31m%s:\t\x1b[32m%s\x1b[0m\n"
    for idx, value in enumerate(array):
        stderr.write(fstring % (idx, value))
    try:
        idx = int(input("Enter number: "))
        if idx in range(len(array)):
            stderr.write(f"\t{array[idx]}\n")
            return array[idx]
        error(ferror % ("Valid number"))
    except ValueError:
        error(ferror % ("Valid number"))


def madison(json):
    """docstring"""
    get_auth_key_from_madison()
    with open(json, encoding="utf-8") as file:
        for result in loads(file.read())["Results"]:
            for vulnerability in result["Vulnerabilities"]:
                if vulnerability["Severity"] in set(["HIGH", "CRITICAL"]):
                    stderr.write(f"\x1b[31m{vulnerability['Title']}\x1b[0m\n")
                    to_madison(vulnerability)


def to_madison(vulnerability):
    """Send a message to MADISON"""
    severeties = {
        "CRITICAL": "3",
        "HIGH": "4",
    }
    post(
        f"https://{environ.get('MADISON_HST')}/api/events/custom/{get_auth_key_from_madison()}",
        {
            "labels": {
                "trigger": "A vulnerability was found.",
                "severity_level": severeties[vulnerability["Severity"]],
                "madison_client": "TrivyScan",
                "target_project": "prod",
                "source_server": gethostname(),
            },
            "annotations": {
                "description": f"{vulnerability['Title']}\n{vulnerability['Description']}",
                "plk_incident_initial_status": "planned",
                "plk_incident_link_url__gitlab": environ.get("CI_JOB_URL"),
                "plk_incident_link_icon__gitlab": "fa-gitlab",
                "plk_incident_link_title_en__gitlab": "JOB",
                "plk_incident_link_url__yahoo": vulnerability["PrimaryURL"],
                "plk_incident_link_icon__yahoo": "fa-yahoo",
                "plk_incident_link_title_en__yahoo": vulnerability["PkgName"],
            },
        },
    )


def get_auth_key_from_madison() -> map:
    """Get auth_key from MADISON"""
    return post(
        f"https://{environ.get('MADISON_HST')}/api/{environ.get('MADISON_PRJ')}/self_setup/"
        + environ.get("MADISON_KEY"),
        {"type": "custom", "identifier": argv[0], "use_default_team": "true"},
    )["auth_key"]


def post(url, data) -> map:
    """Send report"""
    req = request.Request(url, method="POST")
    req.add_header("Content-Type", "application/json")
    with request.urlopen(req, data=dumps(data).encode()) as resp:
        return loads(resp.read())
