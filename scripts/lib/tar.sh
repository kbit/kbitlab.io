#!/usr/bin/env bash

declare -rA NGINX=(
    [OUT]='/usr/local/www'
    [CFG]='/usr/local/etc/nginx/conf.d'
)

function f_artifactory
{
    declare -r APP='artifactory'
    declare -rA VLT=(
        [USER]=$(f_base "user${APP}")
        [PASS]=$(f_hash "pass${APP}")
    )
    if ! [[ -d "/usr/local/${APP}" ]]; then
        if (which 'postgres'); then
            f_pgsql_createdb
            ln -s "/usr/local/${APP}/"{misc/db/postgresql,etc/db}.properties
        fi
        f_proxy 8081
        pkg ins -y "${APP}"
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_elk
{
    declare -r APP='elk'
    if ! [[ -d "/usr/local/${APP}" ]]; then
        f_proxy 5601
        pkg ins -y {elasticsearch,logstash,kibana}8
        sed -ie "
            s|^#xpack.security.enabled.*$|xpack.security.enabled: false|;
            s|^#xpack.ml.enabled.*$|xpack.ml.enabled: false|;
            s|^#cluster.name.*$|cluster.name: ${APP}|;
            s|^#action.*$|action.auto_create_index: false|
                " /usr/local/etc/elasticsearch/elasticsearch.yml
        for VAR in elasticsearch logstash kibana; do
            sysrc "${VAR}_enable=YES"
            service "${VAR}" start
        done
        f_clear
    fi
}

function f_core
{
    if ! [[ -e '/etc/ipfw.rules' ]]; then
        pkg upg -y
        mapfile -t < <(f_pkgs 'base')
        pkg ins -y bind-tools ca_root_nss base64 "${MAPFILE[@]}"
        cat <<'EOF' >/etc/ipfw.rules
#!/usr/bin/env sh
fw='/sbin/ipfw -q add'
srv='ssh'
ipfw -q -f flush
${fw} pass ip from any to any via lo0
${fw} pass ip from me to any
${fw} pass icmp from any to any
${fw} pass udp from any domain to me
${fw} pass tcp from any to me established
${fw} pass tcp from any to me ${srv}
EOF
        sysrc firewall_enable='YES'
        sysrc firewall_script='/etc/ipfw.rules'
        service ipfw start
        f_logger 'info' 'STAGE CORE DONE'
    fi
}

function f_gitea
{
    declare -r APP='gitea'
    if ! (which "${APP}"); then
        (which 'postgres') && f_pgsql_createdb "${APP}"
        f_proxy 3000
        pkg ins -y "${APP}"
        sed -ie "
            s|^ROOT_URL .*|ROOT_URL = http://${HOSTNAME}/|;
            s|^DB_TYPE .*|DB_TYPE = postgres|;
            s|^HOST .*|HOST = 127.0.0.1:5432|;
            s|^NAME .*|NAME = dbgitea|;
            s|^USER .*|USER = usrgitea|;
            s|^PASSWD .*|PASSWD = passgitea|
            " "/usr/local/etc/${APP}/conf/app.ini"
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_glpi
{
    declare -r APP='glpi'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        (which 'mariadb') && f_mysql_createdb "${APP}"
        f_site "${APP}"
        pkg ins -y "${APP}"{,-plugins-fusioninventory-server}
        chown -R 'www:www' "${NGINX[OUT]}/${APP}"{files,config}
        php "${NGINX[OUT]}/${APP}/bin/console" glpi:system:check_requirements
        f_clear
    fi
}

function f_gitlab
{
    # https://gitlab.fechner.net/mfechner/Gitlab-docu/-/blob/master/install/16.0-freebsd.md
    declare -r APP='gitlab'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        if (which 'postgres'); then
            psql -d template1 -U postgres -c "CREATE USER git CREATEDB SUPERUSER;"
            psql -d template1 -U postgres -c "CREATE DATABASE gitlabhq_production OWNER git;"
            psql -U git -d gitlabhq_production
            psql -U postgres -d gitlabhq_production -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"
            psql -U postgres -d gitlabhq_production -c "CREATE EXTENSION IF NOT EXISTS btree_gist;"
        fi
        f_site "${APP}"
        pkg ins -y "${APP}"-ce
        if (pushd /usr/local/www/gitlab-ce); then
            su --login git --command "
                git config --global core.autocrlf input
                git config --global gc.auto 0
                git config --global repack.writeBitmaps true
                git config --global receive.advertisePushOptions true
                git config --global core.fsync objects,derived-metadata,reference
                mkdir -p /usr/local/git/.ssh
                mkdir -p /usr/local/git/repositories
                    "
            chown git:git /usr/local/git/repositories
            chmod 2770 /usr/local/git/repositories
        fi
        f_clear
    fi
}

function f_vaultwarden
{
    declare -r APP='vaultwarden'
    if ! (which "${APP}"); then
        f_proxy 8000
        pkg ins -y "${APP}"
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_rt
{
    declare -r APP='rt'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_nginx 'install'
        pkg ins -y "${APP}"50 mysql57-server spawn-fcgi
        sysrc mysql_enable='YES'
        service mysql-server start
        f_logger 'info' "STAGE ${APP^^} DONE"
        f_logger 'audit' "$(ifconfig | awk '/broadcast/{print $2}')"
        cat <<"EOF" >"/usr/local/etc/nginx/conf.d/rt.conf"
server {
    server_name        node;
    access_log         /var/log/nginx/rt.access.log;
    error_log          /var/log/nginx/rt.error.log;
    root               /usr/local/share/rt50/html;
    location / {
        fastcgi_param  QUERY_STRING       $query_string;
        fastcgi_param  REQUEST_METHOD     $request_method;
        fastcgi_param  CONTENT_TYPE       $content_type;
        fastcgi_param  CONTENT_LENGTH     $content_length;

        fastcgi_param  SCRIPT_NAME        "";
        fastcgi_param  PATH_INFO          $uri;
        fastcgi_param  REQUEST_URI        $request_uri;
        fastcgi_param  DOCUMENT_URI       $document_uri;
        fastcgi_param  DOCUMENT_ROOT      $document_root;
        fastcgi_param  SERVER_PROTOCOL    $server_protocol;

        fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
        fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

        fastcgi_param  REMOTE_ADDR        $remote_addr;
        fastcgi_param  REMOTE_PORT        $remote_port;
        fastcgi_param  SERVER_ADDR        $server_addr;
        fastcgi_param  SERVER_PORT        $server_port;
        fastcgi_param  SERVER_NAME        $server_name;
        fastcgi_pass   unix:/var/run/spawn-fcgi.sock;
    }
}
EOF
        # EDITOR crontab -e
        # @reboot spawn-fcgi -U www -G www -s /var/run/spawn-fcgi.sock -- /usr/local/sbin/rt-server.fcgi
        # rt-setup-database --action init
        f_clear
    fi
}

function f_guacamole
{
    declare -r APP='guacd'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_tomcat 'install'
        pkg ins -y guacamole-{client,server}
        ln -s /usr/local/etc/guacamole-client/user-mapping.xml{.sample,}
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_grafana
{
    declare -r APP='grafana'
    declare -rA VLT=(
        [USER]=$(f_base "user${APP}")
        [PASS]=$(f_hash "pass${APP}")
    )
    if ! [[ -d "/usr/local/etc/rc.d/${APP}" ]]; then
        f_proxy 3000
        pkg ins -y "${APP}"{8,-loki} prometheus
        if (which 'postgres'); then
            f_pgsql_createdb
            sed -ie "
                s|3306|5432|;
                s|^;type = sqlite3$|type = postgres|;
                s|^;user = root$|user = ${VLT[USER],,}|;
                s|^;password =$|password = ${VLT[PASS]}|
                    " "/usr/local/etc/${APP}/${APP}.ini"
        fi
        for var in {"${APP}",prometheus,loki}; do
            sysrc "${var}_enable=YES"
            service "${var}" start
        done
        f_clear
    fi
}

function f_clear
{
    pkg autorem -y
    pkg clean -a -y
    f_logger 'info' "STAGE ${APP^^} DONE"
    f_logger 'audit' "$(ifconfig | awk '/broadcast/{print $2}')"
}

function f_otrs
{
    declare -r APP='otrs' DIR='/usr/local/otrs'
    if ! [[ -d "${DIR}" ]]; then
        (which 'postgres') && f_pgsql_createdb "${APP}"
        f_httpd 'install'
        pkg ins -y otrs
        chown 'www:www' "${DIR}/var/httpd"
        perl -cw "${DIR}/bin/cgi-bin/index.pl"
        perl -cw "${DIR}/bin/cgi-bin/customer.pl"
        perl -cw "${DIR}/bin/otrs.Console.pl"
        ln -s \
            /usr/local/{otrs/scripts/apache2-httpd.include,etc/apache24/Includes/otrs}.conf
        "${DIR}/bin/otrs.SetPermissions.pl" \
            --web-group=www --otrs-user=otrs --admin-group=wheel "${DIR}"
        # sudo -iu otrs /usr/local/otrs/bin/otrs.Daemon.pl start
        f_clear
    fi
}

function f_httpd
{
    declare -r APP='apache24'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_core 'install'
        pkg ins -y apache24 py39-certbot-apache ap24-mod_{fastcgi,perl2} \
            p5-{DBD-Pg,Crypt-GPG,GnuPG}
        sed -ie '
            s|.*LoadModule|LoadModule|
                ' /usr/local/etc/apache24/modules.d/260_mod_perl.conf
        sed -ie '
            s|.*LoadModule fastcgi_module|LoadModule fastcgi_module|;
                ' /usr/local/etc/apache24/httpd.conf
        sysrc apache24_enable='YES'
        service apache24 start
        sed -ie 's|ssh|ssh, http|' '/etc/ipfw.rules'
        service ipfw restart
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_tomcat
{
    declare -r APP='tomcat9'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_proxy 8080
        pkg ins -y "${APP}"
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_jenkins
{
    declare -r APP='jenkins'
    if ! [[ -e '/usr/local/etc/rc.d/jenkins' ]]; then
        f_proxy 8080
        pkg ins -y "${APP}-lts"
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_librespeed
{
    declare -r APP='librespeed-go'
    if ! [[ -e "/usr/local/bin/${APP}" ]]; then
        f_proxy 8989
        pkg ins -y "${APP}"
        ln -s \
            /usr/local/{"share/examples/${APP}/example-singleServer-full.html","www/${APP}/index.html"}
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_mysql
{
    declare -r APP='mariadb'
    if ! (which "${APP}"); then
        f_core 'install'
        pkg upg -y
        pkg ins -y "${APP}"105-server
        sysrc mysql_enable='YES'
        service mysql-server start
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_nginx
{
    declare -r APP='nginx'
    if ! ("${APP}" -v); then
        f_core 'install'
        pkg ins -y {py39-certbot-,}"${APP}"
        # chown 'www:www' '/usr/local/etc/${APP}/fastcgi_params'
        mkdir "${NGINX[CFG]}"
        declare -ra TYPES=(
            application/{{{{x-web-app-,}manifest,ld,vnd.geo}+,}json,{{rss,xhtml,atom}+,}xml,javascript,x-font-ttf,vnd.ms-fontobject}
            text/{cache-manifest,css,plain,vcard,vnd.rim.location.xloc,vtt,x-{component,cross-domain-policy}}
            image/{bmp,svg+xml,x-icon}
            font/opentype
        ) PROXIED=(
            auth expired private no{-{cache,store},_{last_modified,etag}}
        )
        cat <<EOF >"/usr/local/etc/${APP}/${APP}.conf"
user                     www;
worker_processes         auto;
events {
    worker_connections   1024;
}
http {
    include              mime.types;
    default_type         application/octet-stream;
    sendfile             on;
    client_max_body_size 20M;
    keepalive_timeout    65;
    index                index.html index.php;
    include              /usr/local/etc/nginx/conf.d/*.conf;
    gzip                 on;
    gzip_vary            on;
    gzip_comp_level      4;
    gzip_min_length      256;
    gzip_proxied         ${PROXIED[@]};
    gzip_types           ${TYPES[@]};
}
EOF
        "${APP}" -t
        sed -ie '
            s|ssh|ssh, http|
                ' /etc/ipfw.rules
        sysrc "${APP}_enable=YES"
        service ipfw restart
        service "${APP}" start
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_pgsql
{
    declare -r APP='postgresql'
    if ! (which 'postgres'); then
        f_core 'install'
        pkg ins -y "${APP}13-"{server,contrib}
        cat <<EOF >>'/etc/login.conf'
postgres::lang=en_US.UTF-8::setenv=LC_COLLATE=C::tc=default:
EOF
        cap_mkdb '/etc/login.conf'
        sysrc "${APP}_class=postgres"
        sysrc "${APP}_enable=YES"
        "/usr/local/etc/rc.d/${APP}" initdb
        sed -ie '
            s|.*127.0.0.1/32.*|host\tall\tall\t127.0.0.1/32\tmd5|
                ' /var/db/postgres/data13/pg_hba.conf
        service "${APP}" start
        f_logger 'info' "STAGE ${APP^^} DONE"
    fi
}

function f_php
{
    declare -rA PHP=(
        [SCK]='/var/run/php-fpm.sock'
        [CFG]='/usr/local/etc/php-fpm.d/www.conf'
        [INI]='/usr/local/etc/php.ini'
        [CGI]='/usr/local/etc/nginx/fastcgi_params'
    )
    if ! [[ -e "${PHP[SCK]}" ]]; then
        f_nginx 'install'
        pkg ins -y php80-{bz2,bcmath,pgsql,pdo_{mysql,pgsql,sqlite},pecl-{APCu,xmlrpc},posix,xsl,filter,gettext,snmp,sockets,xml{reader,writer},gmp,pcntl,ctype,curl,dom,exif,fileinfo,gd,iconv,imap,intl,ldap,mbstring,mysqli,opcache,session,simplexml,sodium,xml,zip,zlib}
        mv "${PHP[INI]}"{-production,}
        sed -ie "
            s|.*listen = .*|listen = ${PHP[SCK]};|;
            s|.*listen.owner.*|listen.owner = www|;
            s|.*listen.group.*|listen.group = www|;
            s|.*listen.mode.*|listen.mode = 0660|
                " "${PHP[CFG]}"
        f_php_ini "${PHP[INI]}"
        php-fpm -t
        sysrc php_fpm_enable='YES'
        service php-fpm start
        sockstat | grep php-fpm
        f_logger 'info' 'STAGE PHP DONE'
    fi
}

function f_nextcloud
{
    declare -r APP='nextcloud'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        (which 'postgres') && f_pgsql_createdb "${APP}"
        f_site "${APP}"
        chmod -R ugo+rw "${NGINX[OUT]}/${APP}"
        f_clear
    fi
}

function f_clickhouse
{
    # https://clickhouse.com/docs
    declare -r APP='clickhouse'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_core 'install'
        pkg ins -y zookeeper apache-spark "${APP}"
        mv "/usr/local/etc/${APP}-server/config.xml"{.sample,}
        mv "/usr/local/etc/${APP}-server/users.xml"{.sample,}
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_sonarqube
{
    declare -r APP='sonarqube'
    if ! [[ -e "/usr/local/etc/rc.d/${APP}" ]]; then
        (which 'postgres') && f_pgsql_createdb "${APP}"
        f_proxy 9000
        pkg ins -y "${APP}-community"
        sysrc "${APP}_supervised=NO"
        sysrc "${APP}_enable=YES"
        service "${APP}" start
        f_clear
    fi
}

function f_opnsense
{
    fetch 'https://raw.githubusercontent.com/opnsense/update/ea11531c94e1c031c87cf3c9716bb4a85613fca5/bootstrap/opnsense-bootstrap.sh.in'
    sh ./opnsense-bootstrap.sh.in -y -r 21.7
}

function f_zabbix
{
    declare -r APP='zabbix'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_site "${APP}"
        pkg ins -y zabbix6-{frontend-php80,server,agent}
        if (which 'postgres'); then
            f_pgsql_createdb "${APP}"
            for script in {schema,double,images,data,timescaledb}; do
                sudo -iu postgres psql --file \
                    "/usr/local/share/zabbix6/server/database/postgresql/${script}.sql" \
                    "dbzabbix"
            done
            sed -ie "
                s|^DBName=.*|DBName=${APP}|;
                s|^DBUser=.*|DBUser=usrzabbix|;
                s|.*DBPassword=.*|DBPassword=passzabbix|;
                s|.*DBHost=.*|DBHost=127.0.0.1|;
                    " /usr/local/etc/zabbix6/zabbix_server.conf
        fi
        sed -ie "
            s|^Server=.*|Server=127.0.0.1|;
            s|^Hostname=.*|Hostname=127.0.0.1|;
                " /usr/local/etc/zabbix6/zabbix_agentd.conf
        for VAR in zabbix_{agentd,server}; do
            sysrc "${VAR}_enable=YES"
            service "${VAR}" start
        done
        f_clear
    fi
}

function f_zoneminder
{
    declare -r APP='zoneminder'
    if ! [[ -d "${NGINX[OUT]}/${APP}" ]]; then
        f_site 'zoneminder'
        pkg ins -y "${APP}" mysql57-server fcgiwrap
        sysrc mysql_enable='YES'
        service mysql-server start
        f_logger 'info' "STAGE ${APP^^} DONE"
        f_logger 'audit' "$(ifconfig | awk '/broadcast/{print $2}')"
        cat <<"EOF" >"/usr/local/etc/nginx/conf.d/zoneminder.conf"
server {
    server_name           node;
    access_log            /var/log/nginx/zoneminder.access.log;
    error_log             /var/log/nginx/zoneminder.error.log;
    root                  /usr/local/www/zoneminder;
    index                 index.php;
    gzip                  off;
    location /cgi-bin/nph-zms {
        include           fastcgi_params;
        fastcgi_param     SCRIPT_FILENAME $request_filename;
        fastcgi_pass      unix:/var/run/fcgiwrap/fcgiwrap.sock;
    }
    location /zm/cache {
        /usr/local/www/zoneminder alias /var/cache/zoneminder;
    }
    location /zm {
        alias             /usr/local/www/zoneminder;
        location ~ \.php$ {
            if (!-f $request_filename) {
                return 404
            }
            include       fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $request_filename;
            fastcgi_index index.php;
            fastcgi_pass  unix:/var/run/php-fpm.sock;
        }
        location ~ \.(jpg|jpeg|gif|png|ico)$ {
            access_log    off;
            expires       33d;
        }
        location /zm/api/ {
            alias         /usr/local/www/zoneminder;
            rewrite       ^/zm/api(.+)$ /zm/api/app/webroot/index.php?p=$1 last;
        }
    }
}
EOF
        printf 'tmpfs\t/tmp\ttmpfs\trw,nosuid,mode=01777\t0\t0\n' >>/etc/fstab
        sysrc fcgiwrap_enable='YES'
        sysrc fcgiwrap_user='www'
        sysrc fcgiwrap_socket_owner='www'
        sysrc fcgiwrap_flags='-c 4'
        service fcgiwrap start
        sysrc zoneminder_enable='YES'
        service zoneminder start
        f_clear
    fi
}

function f_freebsd
{
    declare -r APP='xfce'
    if ! [[ -e '/usr/local/etc/xrdp/startwm.sh' ]]; then
        f_core 'install'
        declare -ar PKG=(
            xfce4-{xkb-plugin,session,settings,terminal,power-manager,wm,desktop}
            xorg-minimal lightdm xrdp firefox-esr thunderbird libreoffice
            firacode liberation-fonts-ttf crosextrafonts-carlito
            transmission-gtk thunar pavucontrol
        )
        pkg ins -y "${PKG[@]}"
        sed -ie '
            s|memorylocked=128M|memorylocked=256M|
                ' /etc/login.conf
        sed -ie '
            s|ssh|ssh, rdp|
                ' /etc/ipfw.rules
        sed -ie '
            s|^# exec x.*$||;
            s|^# exec startxfce4.*$|exec startxfce4|
                ' /usr/local/etc/xrdp/startwm.sh
        ln -s /usr/local/etc/xrdp/startwm.sh /home/vagrant/
        chown vagrant:vagrant /home/vagrant/startwm.sh
        chmod +x /home/vagrant/startwm.sh
        cap_mkdb /etc/login.conf
        service ipfw restart
        sysrc xrdp_sesman_enable="YES"
        for VAR in moused dbus hald lightdm xrdp{,sesman}; do
            sysrc "${VAR}_enable=YES"
        done
        f_clear
    fi
}

function f_brew
{
    bash -x < <(curl -fsSL 'https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh')
    brew install sh{ellcheck,fmt} cargo golang
}

function f_werf
{
    werf helm secret generate-secret-key >.werf_secret_key
}
