#!/usr/bin/env bash

function f_omv
{
    # https://docs.openmediavault.org/en/stable/installation/on_debian.html
    if ! [[ -f '/etc/apt/trusted.gpg.d/openmediavault-archive-keyring.asc' ]]; then
        wget -O '/etc/apt/trusted.gpg.d/openmediavault-archive-keyring.asc' https://packages.openmediavault.org/public/archive.key
        apt-key add '/etc/apt/trusted.gpg.d/openmediavault-archive-keyring.asc'
        cat <<EOF >>/etc/apt/sources.list.d/openmediavault.list
deb https://packages.openmediavault.org/public shaitan main
EOF
        export LANG='C.UTF-8'
        export DEBIAN_FRONTEND='noninteractive'
        export APT_LISTCHANGES_FRONTEND='none'
        apt-get update
        apt-get --yes --auto-remove --show-upgraded \
            --allow{-downgrades,-change-held-packages} \
            --no-install-recommends \
            --option DPkg::Options::="--force-confdef" \
            --option DPkg::Options::="--force-confold" \
            install openmediavault{,-keyring}
        f_apt 'testdisk'
        omv-confdbadm populate
    fi
}

function f_eve_ng
{
    declare -r APP='eve-ng'
    if ! [[ -f '/opt/unetlab/wrappers/unl_wrapper' ]]; then
        wget -O - 'http://www.eve-ng.net/focal/eczema@ecze.com.gpg.key' |
            apt-key add -
        apt-get -y install software-properties-common
        printf 'deb [arch=amd64] http://www.eve-ng.net/focal focal main\n' \
            >/etc/apt/sources.list.d/eve-ng.list
        apt-get update || true
        DEBIAN_FRONTEND='noninteractive' apt-get -y install "${APP}" || true
        /etc/init.d/mysql restart
        declare -r ADD=/opt/unetlab/addons/qemu
        declare -rA IMGS=(
            [linux - centos]='http://centos-mirror.rbc.ru/pub/centos/7.9.2009/isos/x86_64/CentOS-7-x86_64-NetInstall-2009.iso'
            [linux - debian]='https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso'
            [linux - ipfire]='https://downloads.ipfire.org/releases/ipfire-2.x/2.27-core172/ipfire-2.27-core172-x86_64.iso'
            [freebsd - stable]='https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/12.4/FreeBSD-12.4-RELEASE-amd64-bootonly.iso'
            [vyos - rolling]='https://s3-us.vyos.io/rolling/current/vyos-rolling-latest.iso'
        )
        for key in "${!IMGS[@]}"; do
            mkdir "${ADD}/${key}"
            wget --output-document="${ADD}/${key}/cdrom.iso" "${IMGS[${key}]}"
            qemu-img create -f qcow2 "${ADD}/${key}/virtioa.qcow2" 10G
        done
        /opt/unetlab/wrappers/unl_wrapper -a fixpermissions
        f_apt aria2 || true
    fi
}
