#!/usr/bin/env bash

declare -ra BEELINE=(
    beeline --outputformat=tsv2 --showHeader=false
)

function f_keytool
{
    keytool -genkey \
        -alias "${HOSTNAME}" -keyalg RSA -keysize 2048 -keystore "${HOSTNAME}.jks" -validity 365 \
        -dname "c=RU, s=Moscow, l=Moscow, o=HOME, ou=HR, cn=${HOSTNAME}"
    keytool -genkeypair -storepass 'changeit' -keypass 'itchange' \
        -alias "${HOSTNAME}" -keyalg RSA -keysize 2048 -keystore "${HOSTNAME}.jks" -validity 365 \
        -dname "c=RU, s=Moscow, l=Moscow, o=HOME, ou=HR, cn=${HOSTNAME}"
    keytool -importkeystore -srckeystore "${HOSTNAME}.jks" \
        -destkeystore "${HOSTNAME}.jks" -deststoretype 'pkcs12'
    keytool -certreq -alias "${HOSTNAME}" -file "${HOSTNAME}.csr" -keystore "${HOSTNAME}.jks"
    yes | keytool -import -alias "${HOSTNAME}" \
        -keystore "$(find /etc -name cacerts | awk '/java/{if (FNR == 1) print $0}')" \
        -storepass 'changeit' -file "ca${HOSTNAME}.crt"
}

function f_java_apps
{
    declare -rA APP=(
        [ApacheDirectoryStudio]='dlcdn.apache.org/directory/studio/2.0.0.v20210717-M17/ApacheDirectoryStudio-2.0.0.v20210717-M17-linux.gtk.x86_64.tar.gz'
        [DBeaver]='dbeaver.io/files/dbeaver-ce-latest-linux.gtk.x86_64.tar.gz'
        [ProjectLibre]='sourceforge.net/projects/projectlibre/files/ProjectLibre/1.93/projectlibre-1.9.3.tar.gz'
        [KSE]='github.com/kaikramer/keystore-explorer/releases/download/v5.5.1/kse-551.zip'
        [Archi]='www.archimatetool.com/downloads/4.4.0/Archi-Linux64-4.4.0.tgz'
    )
    for app in "${!APP[@]}"; do
        f_unarch "${app}" '/opt' "${APP[${app}]}"
    done
}

function f_script
{
    cat <<EOF >"ddl_${SOURCE[NAME]^^}_${DATABASE,,}.sh"
#!/usr/bin/env bash
set -euo pipefail
shellcheck "\${0}"
declare -r TABLE="\${1:?}"
declare -r DDL="${HOME}/ddl_${SOURCE[NAME]^^}_${DATABASE,,}_\${TABLE^^}.hql"
# source.getDDL(Table)
(
    printf '%s\n' "CREATE DATABASE IF NOT EXISTS ${DATABASE}" "USE ${DATABASE}" "ALTER TABLE IF EXISTS \${TABLE} SET TBLPROPERTIES('EXTERNAL'='TRUE')" "DROP TABLE IF EXISTS \${TABLE}'
    ${BEELINE[@]} -u ${SOURCE[JDBC]} -e "USE ${DATABASE}; SHOW CREATE TABLE \${TABLE};" 2> /dev/null |
        sed "s/CREATE TABLE/CREATE EXTERNAL TABLE IF NOT EXISTS/g"
    printf ';\nmsck repair table %s;\n' "\${TABLE}"
) > "\${DDL}"
# source.checkNameSpace(DDL)
if [[ ${SOURCE[NAME]} == \$(awk -F '/' '/hdfs:/{if (FNR==1) print \$3}' "\${DDL}") ]]
then
    # destiny.execDDL(DDL)
    if ! (${GREP[@]} 'VIEW' "\${DDL}")
    then
        if (${BEELINE[@]} -u ${DESTINY[JDBC]} -f "\${DDL}" &> "\${DDL//hql/log}")
        then fLog 'INFO' "\${DDL} append on ${DESTINY[NAME]^^}"
        else fLog 'ERROR' "\${DDL} not append on ${DESTINY[NAME]^^}"
        fi
    else fLog 'AUDIT' "\${DDL}: \${TABLE} is VIEW ${DESTINY[NAME]^^}"
    fi
else fLog 'AUDIT' "\${DDL} other namespace"
fi
EOF
    printf '%s' "ddl_${SOURCE[NAME]^^}_${DATABASE,,}.sh"
}

function f_link
{
    ((${#})) || f_clippit
    f_chk_env 'ipa' 'beeline'
    while getopts ':s:d:b:' Option; do
        case ${Option} in
            s) declare -rA SOURCE=(
                [NAME]=${OPTARG}
                [JDBC]=$(fJDBC)
            ) ;;
            d) declare -rA DESTINY=(
                [NAME]=${OPTARG}
                [JDBC]=$(fJDBC)
            ) ;;
            b) declare -r DATABASE=${OPTARG} ;;
            *) fClippit ;;
        esac
    done
    shift $((OPTIND - 1))
    if [[ ${SOURCE[JDBC]:?} == "${DESTINY[JDBC]:?}" ]]; then
        f_log 'ERROR' 'SOURCE HOST == DESTINY HOST'
    else
        # source.checkDB(DB)
        if (
            "${BEELINE[@]}" -u "${SOURCE[JDBC]}" -e 'SHOW DATABASES;' 2>/dev/null |
                "${GREP[@]}" "${DATABASE:?}"
        ); then
            export -f f_log
            # source.getTables(DB)
            "${BEELINE[@]}" -u "${SOURCE[JDBC]}" -e "show tables from ${DATABASE};" 2>/dev/null |
                timeout --preserve-status 3000 "${TIME[@]}" xargs --max-procs=15 --replace='{}' bash "$(f_script)"
            zip -m "${HOME}/ddl_${SOURCE[NAME]^^}_${DATABASE,,}."{zip,*}
        else
            fLog 'ERROR' "database ${DATABASE} on ${SOURCE[NAME]^^} not found"
        fi
        fLog 'INFO' 'done'
    fi
}
