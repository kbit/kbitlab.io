#!/usr/bin/env python3
"""
https://wombat.org.ua/AByteOfPython
https://habr.com/ru/articles/736842
"""

from sys import stderr, argv
from lib.common import madison, k8s


def main():
    """Check JSON"""
    try:
        if len(argv) == 2:
            if argv[1] == "k8s":
                k8s()
            else:
                madison(argv[1])
        else:
            stderr.write("Nothing")
        stderr.write("\x1b[32mDone!\x1b[0m\n")
    except KeyboardInterrupt:
        stderr.write("\x1b[33m\tEscape!\x1b[0m\n")


if __name__ == "__main__":
    main()
