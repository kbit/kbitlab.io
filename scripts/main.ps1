#!/usr/bin/env pwsh

function f_clipper {
    $ENV = @{
        vagrant  = 'it-gro/win10-ltsc-eval'
        download = 'https://microsoft.com/en-us/evalcenter'
        package  = 'https://learn.microsoft.com/en-us/mem/configmgr/develop/apps/how-to-create-the-windows-installer-file-msi'
        shell    = 'https://learn.microsoft.com/ru-ru/powershell'
    }
    Write-Output $ENV
}

function f_install {
    $List = @(
        'https://www.7-zip.org/a/7z2401-x64.msi'
        'https://www.farmanager.com/files/Far30b6060.x64.20221208.msi'
        'https://download-installer.cdn.mozilla.net/pub/firefox/releases/115.7.0esr/win64/en-US/Firefox%20Setup%20115.7.0esr.msi'
    )
    foreach ($Uri in $list) {
        $params = @{
            Uri = $Uri
            OutFile = Split-Path $Uri -Leaf
        }
        Write-Output "Invoke-WebRequest $($params.Uri)"
        $ProgressPreference = 'SilentlyContinue'
        Invoke-WebRequest @params
        $ProgressPreference = 'Continue'
        # Start-Process -Wait -FilePath 'msiexec' -ArgumentList "/passive /package $($params.OutFile)"
        Write-Output "Remove-Item $($params.OutFile)"
        Remove-Item $params.OutFile
    }
}

function f_get {
    $params = @{
        Uri = 'https://postman-echo.com/get'
    }
    $ProgressPreference = 'SilentlyContinue'
    $data = Invoke-WebRequest @params | ConvertFrom-Json
    $ProgressPreference = 'Continue'
    Write-Output $data.headers
}

function f_post {
    $data = @{
        One = '1'
        Two = '2'
    }
    $params = @{
        Method = 'POST'
        Headers = @{ContentType = 'application/json'}
        Uri = 'https://postman-echo.com/post'
        Body = $data | ConvertTo-Json
    }
    $ProgressPreference = 'SilentlyContinue'
    $data = Invoke-WebRequest @params | ConvertFrom-Json
    $ProgressPreference = 'Continue'
    Write-Output $data.headers
}

switch ($args.count) {
    0 {f_install}
    1 {
        switch ($args[0]) {
            'get' {f_get}
            'post' {f_post}
            default {f_clipper}
        }
    }
    default {f_clipper}
}
