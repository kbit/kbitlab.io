#!/usr/bin/env bash
# declare -rA ENV=(
#     [vagrant]='generic/rhel9'
#     [download]='https://developers.redhat.com'
#     [package]='https://rpm-packaging-guide.github.io'
#     [shell]='https://www.opennet.ru/docs/RUS/bash_scripting_guide'
# )

########################################################################################################################
function f_clippit
{
    cat <<EOF
https://guide.bash.academy
https://kvz.io/bash-best-practices.html
https://google.github.io/styleguide/shellguide.html
https://opennet.ru/docs/RUS/bash_scripting_guide
https://lug.fh-swf.de/vim/vim-bash/StyleGuideShell.en.pdf
https://redhatofficial.github.io/#!/main
https://itman.in/kvm-manage-virsh/#i
https://interface31.ru/tech_it/2017/04/ssh-tunneli-na-sluzhbe-sistemnogo-administratora.html
https://easy-network.ru
https://tldr.sh
https://ovios.org/use-cases
https://devhints.io/bash

Usage: bash ${0} [OPTIONS]
Options:
EOF
    for key in "${!SCOPE[@]}"; do
        for value in ${SCOPE[${key}]//|/' '}; do
            printf '\t-o %s\t-a %s\n' "${key}" "${value}"
        done
    done
    exit 0
}

########################################################################################################################
function f_main
{
    set -euo pipefail
    # shellcheck source=/dev/null
    if [[ -f /etc/os-release ]]; then
        source '/etc/os-release'
    elif [[ -f /System/Library/CoreServices/SystemVersion.plist ]]; then
        declare -r ID='macos'
    else
        f_logger error 'This OS not support'
    fi
    # shellcheck source=/dev/null
    if ((${#})); then
        for lib in common dba web pod; do
            # shellcheck source=/dev/null
            source "${0//main/lib/${lib}}"
        done
        case ${ID} in
            debian | ubuntu) source "${0//main/lib/deb}" ;;
            fedora | rhel) source "${0//main/lib/rpm}" ;;
            alpine | macos | freebsd) source "${0//main/lib/tar}" ;;
            *) f_clippit ;;
        esac
        declare -rA TRAIT=(
            [serv]='install|remove'
            [lang]='build|lint'
        )
        declare -A SCOPE
        for lang in bash python powershell groovy golang rust; do
            SCOPE[${lang}]=${TRAIT[lang]}
        done
        for serv in freeipa postgresql; do
            SCOPE[${serv}]=${TRAIT[serv]}
        done
        declare -rA SCOPE
        while getopts 'o:m:i' Option; do
            case ${Option} in
                o) declare -r OBJECT="${OPTARG}" ;;
                m) declare -r METHOD="${OPTARG}" ;;
                i)
                    PS3="Select object: "
                    select object in "${!SCOPE[@]}"; do
                        declare -r OBJECT=${object}
                        PS3="Select method: "
                        select method in ${SCOPE[${object}]//|/' '}; do
                            declare -r METHOD=${method}
                            break
                        done
                        break
                    done
                    ;;
                *) f_clippit ;;
            esac
        done
        shift $((OPTIND - 1))
        if [[ -n ${SCOPE[${OBJECT}]} ]]; then
            for method in ${SCOPE[${OBJECT}]//|/' '}; do
                if [[ ${method} == "${METHOD}" ]]; then
                    "f_${OBJECT}" "${METHOD}"
                    return 0
                fi
            done
            f_clippit
        else
            f_logger error 'This Object not support'
        fi
    else
        case ${ID} in
            debian | ubuntu) f_deb ;;
            rhel | fedora) f_rpm ;;
            freebsd) f_tar ;;
            *) f_clippit ;;
        esac
    fi
}

########################################################################################################################
function f_logger
{
    declare -r TAG=${1:?}
    declare -r MSG=${2:?}
    declare -irA TAGs=(
        [error]=31
        [info]=32
        [audit]=33
    )
    printf '%(%y-%m-%d_%T)T\x1b[%dm\t%s:\t%b\x1b[0m\n' -1 "${TAGs[${TAG}]}" "${TAG^^}" "${MSG}" 1>&2
    if [[ ${TAG} == 'error' ]]; then
        return 1
    fi
}

########################################################################################################################
function f_deb
{
    if ((UID)); then
        f_logger error 'Muste be run under root, aborting'
    elif ! [[ -e '/usr/sbin/lightdm' ]]; then
        declare -ra DIFF=(
            clonezilla testdisk inxi deb{ootstrap,orphan} wavemon unattended-upgrades rfkill
            aptitude acpi-support jq n{cdu,wipe} hdparm aria2 swaks gnupg speedtest-cli httrack git curl nmap autossh
            nano pmount bash shellcheck {htt,o}ping dnsutils arp{on,-scan} proxytunnel gddrescue parted cdw rkhunter

            x{org,clip} desktop-base {exo-,more}utils plymouth-themes
            light{-locker,dm} fonts-{crosextra-carlito,firacode,oldstandard}
            firefox-esr webext-ublock-origin-firefox laptop-mode-tools gvfs-backends
            libreoffice-{gtk3,writer,calc,impress} network-manager-{gnome,openvpn,openconnect}
        )
        apt-get autopurge -y reportbug rsyslog vim-common avahi-autoipd
        mapfile -t < <(f_pkgs base xfce)
        f_apt "${MAPFILE[@]}" "${DIFF[@]}"
        plymouth-set-default-theme -r
        sed -ie '
            s|T=5|T=0|; s|quiet|quiet splash|g
                ' /etc/default/grub
        update-grub2
        systemctl mask {suspend,hibernate,{hybrid-,}sleep}.target
        if [[ -f /etc/NetworkManager/conf.d/wifi.conf ]]; then
            printf '[device]\nwifi.scan-rand-mac-address=no\n' >/etc/NetworkManager/conf.d/wifi.conf
            systemctl restart NetworkManager
        fi
        # dpkg-reconfigure unattended-upgrades locales
        reboot
    fi
}

########################################################################################################################
function f_tar
{
    if ((UID)); then
        f_logger error 'Muste be run under root, aborting'
    elif ! [[ -e /usr/local/etc/lightdm ]]; then
        declare -ar DIFF=(
            xorg-minimal bind-tools ca_root_nss linkchecker siege httrack
        )
        mapfile -t < <(f_pkgs 'base' 'xfce')
        pkg upg -y
        pkg ins -y "${MAPFILE[@]}" "${DIFF[@]}"
        pkg autorem -y
        pkg clean -a -y
        sed -ie '
            s|memorylocked=128M|memorylocked=256M|
                ' /etc/login.conf
        sed -ie '
            s|^# exec x.*$||;
            s|^# exec startxfce4.*$|exec startxfce4|
                ' /usr/local/etc/xrdp/startwm.sh
        ln -s {/usr/local/etc/xrdp,"${HOME}"}/startwm.sh
        chmod +x "${HOME}/startwm.sh"
        pw usermod "${USER}" -L english
        cap_mkdb /etc/login.conf
        sysrc "hostname=${HOSTNAME}"
        for VAR in moused dbus hald lightdm xrdp{,sesman}; do
            sysrc "${VAR}_enable=YES"
        done
        reboot
    fi
}

########################################################################################################################
function f_rpm
{
    if ((UID)); then
        f_logger error 'Muste be run under root, aborting'
    elif ! [[ -e '/sbin/lightdm' ]]; then
        declare -ar DIFF=(
            dnf-automatic @base-x inxi xfwm4 xfdesktop pipewire-pulseaudio rpmorphan
            libreoffice-{gtk3,writer,calc} {google-carlito,mozilla-fira-mono,oldstandard-sfd}-fonts
            {transmission,redshift}-gtk firefox mozilla-ublock-origin gstreamer1-plugin-libav
        )
        mapfile -t < <(f_pkgs 'base' 'xfce')
        printf 'install_weak_deps=False\n' >>/etc/dnf/dnf.conf
        # dnf install -y 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm'
        dnf install -y "${MAPFILE[@]}" "${DIFF[@]}"
        dnf clean all
        sed -ie '
            s|^apply_updates.*|apply_updates = yes|
                ' /etc/dnf/automatic.conf
        sed -ie '
            s|=permissive|=disabled|
                ' /etc/selinux/config
        setenforce 0
        cat <<EOF >>/etc/xrdp/xrdp.ini
[Xorg]
name=Xorg
lib=libxup.so
username=ask
password=ask
ip=127.0.0.1
port=-1
code=20
EOF
        firewall-cmd --permanent --remove-service=dhcpv6-client
        firewall-cmd --permanent --add-service={ssh,rdp}
        firewall-cmd --reload
        systemctl enable lightdm xrdp dnf-automatic-notifyonly.timer
        systemctl set-default graphical
        systemctl mask {suspend,hibernate,{hybrid-,}sleep}.target
        reboot
    fi
}

########################################################################################################################
function f_pkgs
{
    declare -a PKG
    for CASE in "${@}"; do
        case ${CASE} in
            base) PKG=(
                sudo unzip bash curl autossh n{load,map} iperf3 testdisk rkhunter n{map,load} nethogs iperf3
            ) ;;
            xfce) PKG=(
                lightdm x{rdp,fce4-{session,terminal,panel,xkb-plugin,{task,power-}manager,notifyd},archiver}
                th{unar,underbird} geany transmission-gtk parole pavucontrol ristretto
            ) ;;
            virt) PKG=(
                lynis qemu-{system,utils} ovmf libguestfs-tools libvirt-{clients,daemon-system} vagrant-libvirt podman
                buildah
            ) ;;
            tex) PKG=(
                texlive-latex-{recommended,extra} tesseract-ocr{-rus,} fail2ban tor{socks,-geoipdb,}
            ) ;;
        esac
        printf '%s\n' "${PKG[@]}"
    done
}

########################################################################################################################
function f_apt
{
    apt-get update
    apt-get install --yes --no-install-recommends "${@}"
    apt-get full-upgrade -y
    mapfile -t < <(deborphan --guess-all)
    while ((${#MAPFILE[@]})); do
        apt-get autopurge -y "${MAPFILE[@]}"
        mapfile -t < <(deborphan --guess-all)
    done
    apt-get clean
    find /var/lib/apt/lists/ -depth -type f -delete
}

########################################################################################################################
time f_main "${@}" >/dev/null
